package net.cocooncreations.template.network;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;

/**
 * Created by zeniosagapiou on 22/02/17.
 */

public interface CocoonNetworkResponse {
    <T> void onResponse(T response, String requestcode, NetworkResponse networkResponse);
    void onError(VolleyError volleyError);
}
