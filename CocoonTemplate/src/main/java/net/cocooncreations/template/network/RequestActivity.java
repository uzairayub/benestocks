package net.cocooncreations.template.network;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by zeniosagapiou on 22/02/17.
 */

public abstract class RequestActivity extends AppCompatActivity implements CocoonNetworkResponse {


    @Override
    protected void onStop() {
        super.onStop();

        VolleySingleton.getInstance(this).getRequestQueue().cancelAll(this.getClass().getSimpleName());
    }

}
