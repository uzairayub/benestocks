package net.cocooncreations.template.network;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by zeniosagapiou on 22/02/17.
 */

public class CocoonJsonRequest extends JsonObjectRequest {


    private Builder builder;

    public static Builder with(String url, CocoonNetworkResponse requester) {
        return new Builder(url, requester);
    }

    private CocoonJsonRequest(Builder builder) {
        super(builder.method, builder.url, builder.jsonObject, builder.listener, builder.errorListener);
        this.builder=builder;
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        builder.networkResponse = response;
        return  super.parseNetworkResponse(response);
    }

    @Override
    public HashMap<String, String> getHeaders() {
        return builder.headers == null ? new HashMap<String, String>() : builder.headers;
    }

    public static class Builder {

        private String url;
        private CocoonNetworkResponse requester;

        private int method = Method.GET;
        private JSONObject jsonObject = new JSONObject();
        private HashMap<String, String> headers = new HashMap<>();
        private Response.ErrorListener errorListener;
        private Response.Listener<JSONObject> listener;
        private NetworkResponse networkResponse;

        public Builder(String url, CocoonNetworkResponse requestActivity) {
            this.url = url;
            this.requester = requestActivity;
            this.errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Builder.this.requester.onError(error);
                }
            };
            this.listener = new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Builder.this.requester.onResponse(response, method + Builder.this.url, networkResponse);
                }
            };

        }

        public CocoonJsonRequest callAsync() {
            CocoonJsonRequest cocoonJsonRequest = new CocoonJsonRequest(this);
            cocoonJsonRequest.setTag(requester.getClass().getSimpleName());

            Log.d("simplename", requester.getClass().getSimpleName());

            if (requester instanceof RequestFragment) {
                RequestFragment requestFragment = (RequestFragment) requester;
                VolleySingleton.getInstance(requestFragment.getContext()).addRequest(cocoonJsonRequest);
            } else if (requester instanceof RequestActivity) {
                RequestActivity requestActivity = (RequestActivity) requester;
                VolleySingleton.getInstance(requestActivity).addRequest(cocoonJsonRequest);
            }


            return cocoonJsonRequest;
        }

        public Builder method(int method) {
            this.method = method;
            return this;
        }

        public Builder jsonObject(JSONObject jsonObject) {
            this.jsonObject = jsonObject;
            return this;
        }

        public Builder headers(HashMap headers) {
            this.headers = headers;
            return this;
        }

    }

//    public CocoonJsonRequest(int method, String url, JSONObject jsonRequest, HashMap<String, String> headers, RequestActivity requestActivity) {
//        super(method, url, jsonRequest, requestActivity, requestActivity);
//        setTag(requestActivity.getClass().getSimpleName());
//    }
//
//    public CocoonJsonRequest(int method, String url, RequestActivity requestActivity) {
//        super(url, new JSONObject(), requestActivity, requestActivity);
//        setTag(requestActivity.getClass().getSimpleName());
//    }
//
//    public CocoonJsonRequest(String url, JSONObject jsonRequest, RequestActivity requestActivity) {
//        super(url, jsonRequest, requestActivity, requestActivity);
//        setTag(requestActivity.getClass().getSimpleName());
//    }
//
//    public CocoonJsonRequest(String url, RequestActivity requestActivity) {
//        super(url, new JSONObject(), requestActivity, requestActivity);
//        setTag(requestActivity.getClass().getSimpleName());
//    }


}
