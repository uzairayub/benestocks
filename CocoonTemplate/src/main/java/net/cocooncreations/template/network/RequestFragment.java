package net.cocooncreations.template.network;

import android.support.v4.app.Fragment;

/**
 * Created by zeniosagapiou on 28/02/17.
 */

public abstract class RequestFragment extends Fragment implements CocoonNetworkResponse {

    @Override
    public void onStop() {
        super.onStop();
        VolleySingleton.getInstance(getContext()).getRequestQueue().cancelAll(this.getClass().getSimpleName());
    }

}
