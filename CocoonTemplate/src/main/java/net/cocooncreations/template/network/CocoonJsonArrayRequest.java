package net.cocooncreations.template.network;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import java.util.HashMap;

/**
 * Created by zeniosagapiou on 22/02/17.
 */

public class CocoonJsonArrayRequest extends JsonArrayRequest {


    public final String requestCode;

    public static CocoonJsonArrayRequest.Builder with(String url, CocoonNetworkResponse requester) {
        return new CocoonJsonArrayRequest.Builder(url, requester);
    }

    private final HashMap<String, String> headers;

    private CocoonJsonArrayRequest(CocoonJsonArrayRequest.Builder builder ) {
        super(builder.method, builder.url, builder.jsonArray, builder.listener, builder.errorListener);
        headers = builder.headers;
        requestCode = builder.method + builder.url;
    }


    @Override
    public HashMap<String, String> getHeaders() {
        return headers;
    }

    public static class Builder {

        private String url;
        private final CocoonNetworkResponse requester;

        private int method = Method.GET;
        private JSONArray jsonArray = new JSONArray();
        private HashMap<String, String> headers = new HashMap<>();
        private final Response.ErrorListener errorListener;
        private final Response.Listener<JSONArray> listener;

        public Builder(String url, CocoonNetworkResponse requester) {
            this.url = url;
            this.requester = requester;
            this.listener = new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    Builder.this.requester.onResponse(response, method + Builder.this.url, null);

                }
            };
            this.errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Builder.this.requester.onError(error);
                }
            };
        }

        public CocoonJsonArrayRequest callAsync() {
            CocoonJsonArrayRequest cocoonJsonArrayRequest = new CocoonJsonArrayRequest(this);
            cocoonJsonArrayRequest.setTag(requester.getClass().getSimpleName());

            if (requester instanceof RequestFragment) {
                RequestFragment requestFragment = (RequestFragment) requester;
                VolleySingleton.getInstance(requestFragment.getContext()).addRequest(cocoonJsonArrayRequest);
            } else if (requester instanceof RequestActivity) {
                RequestActivity requestActivity = (RequestActivity) requester;
                VolleySingleton.getInstance(requestActivity).addRequest(cocoonJsonArrayRequest);
            }
            return cocoonJsonArrayRequest;
        }

        public CocoonJsonArrayRequest.Builder method(int method) {
            this.method = method;
            return this;
        }

        public CocoonJsonArrayRequest.Builder jsonArray(JSONArray jsonArray) {
            this.jsonArray = jsonArray;
            return this;
        }

        public CocoonJsonArrayRequest.Builder headers(HashMap headers) {
            this.headers = headers;
            return this;
        }

    }
}
