package net.cocooncreations.template.network;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zeniosagapiou on 22/02/17.
 */

public class CocoonStringRequest extends StringRequest {


    public static Builder with(String url, CocoonNetworkResponse requester) {
        return new Builder(url, requester);
    }

    private Builder builder;

    private CocoonStringRequest(Builder builder) {
        super(builder.method, builder.url, builder.listener, builder.errorListener);
        this.builder = builder;
    }

    @Override
    public HashMap<String, String> getHeaders() {
        return builder.headers == null ? new HashMap<String, String>() : builder.headers;
    }


    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        builder.networkResponse = response;
        return super.parseNetworkResponse(response);
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return builder.params == null ? new HashMap<String, String>() : builder.params;
    }

    @Override
    public String getBodyContentType() {
        if (builder.bodyContentType != null)
            return builder.bodyContentType;

        return super.getBodyContentType();
    }

    public static class Builder {

        private final Response.ErrorListener errorListener;
        private final Response.Listener<String> listener;
        private final CocoonNetworkResponse requester;
        private final String url;
        private int method = Request.Method.GET;
        private HashMap<String, String> headers = new HashMap<>();
        private HashMap<String, String> params;
        private String bodyContentType;
        public NetworkResponse networkResponse;

        public Builder(String url, CocoonNetworkResponse requester) {
            this.url = url;
            this.requester = requester;
            this.errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Builder.this.requester.onError(error);
                }
            };
            this.listener = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Builder.this.requester.onResponse(response, method + Builder.this.url, networkResponse);
                }
            };
        }

        public CocoonStringRequest callAsync() {
            CocoonStringRequest cocoonStringRequest = new CocoonStringRequest(this);
            cocoonStringRequest.setTag(requester.getClass().getSimpleName());

            if (requester instanceof RequestFragment) {
                RequestFragment requestFragment = (RequestFragment) requester;
                VolleySingleton.getInstance(requestFragment.getContext()).addRequest(cocoonStringRequest);
            } else if (requester instanceof RequestActivity) {
                RequestActivity requestActivity = (RequestActivity) requester;
                VolleySingleton.getInstance(requestActivity).addRequest(cocoonStringRequest);
            }

            return cocoonStringRequest;
        }

        public CocoonStringRequest.Builder method(int method) {
            this.method = method;
            return this;
        }

        public CocoonStringRequest.Builder headers(HashMap headers) {
            this.headers = headers;
            return this;
        }


        public CocoonStringRequest.Builder params(HashMap<String, String> params) {
            this.params = params;
            return this;
        }

        public Builder bodyContentType(String bodyContentType) {
            this.bodyContentType = bodyContentType;
            return this;
        }
    }
}
