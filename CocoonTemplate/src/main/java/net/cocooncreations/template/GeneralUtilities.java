package net.cocooncreations.template;

import java.lang.reflect.Field;

/**
 * Created by aloupas on 3/6/2017.
 */

public class GeneralUtilities {

    public  static <T> String toString(Object object){

        String returnedString = "";
        String comma = "";
        String new_line = "";

        for (Field field : object.getClass().getDeclaredFields()) {
            try {
                returnedString += comma + new_line +  field.getName() + ": " + field.get(object).toString();
                comma = ",";
                new_line = "\n";
            } catch (IllegalAccessException | NullPointerException e) {
                e.printStackTrace();
            }
        }

        return returnedString;
    }

}
