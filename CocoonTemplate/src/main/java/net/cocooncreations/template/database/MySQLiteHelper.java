package net.cocooncreations.template.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import net.cocooncreations.template.database.library.NotInitialisedException;


public class MySQLiteHelper extends SQLiteOpenHelper {

    public interface DatabaseListener{
        void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion);
        void onCreate(SQLiteDatabase db);
    }

    private static MySQLiteHelper instance;

    private SQLiteDatabase myWritableDb;
    private static Context mContext;

    private static String databaseName;
    private static int databaseVersion;
    private static DatabaseListener databaseListener;

    public MySQLiteHelper(Context context, String databaseName, int databaseVersion) {
        super(context, databaseName, null, databaseVersion);
    }

    public static void init(Context context, String databaseNameIn, int databaseVersionIn, DatabaseListener databaseListenerIn) {
        mContext = context.getApplicationContext();
        databaseName = databaseNameIn;
        databaseVersion = databaseVersionIn;
        databaseListener = databaseListenerIn;
    }

    public static synchronized MySQLiteHelper getInstance() {

        if (mContext == null)
            throw new NotInitialisedException("Not Initialized. Intiialize DatabaseUtils inside Application class.");

        if (instance == null)
            instance = new MySQLiteHelper(mContext, databaseName, databaseVersion);

        return instance;
    }

    public SQLiteDatabase getMyWritableDatabase() {
        if ((myWritableDb == null) || (!myWritableDb.isOpen())) {
            myWritableDb = this.getWritableDatabase();
        }
        return myWritableDb;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        databaseListener.onCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        databaseListener.onUpgrade(db, oldVersion, newVersion);

    }


}
