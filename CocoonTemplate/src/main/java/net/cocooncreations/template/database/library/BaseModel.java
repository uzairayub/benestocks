package net.cocooncreations.template.database.library;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import net.cocooncreations.template.database.MySQLiteHelper;

import org.json.JSONObject;
import org.w3c.dom.Document;

import java.lang.reflect.Field;
import java.util.Date;

import static net.cocooncreations.template.database.library.Datatype.BOOLEAN;
import static net.cocooncreations.template.database.library.Datatype.BYTES;
import static net.cocooncreations.template.database.library.Datatype.DATE;
import static net.cocooncreations.template.database.library.Datatype.FOREIGN_KEY;
import static net.cocooncreations.template.database.library.Datatype.INTEGER;
import static net.cocooncreations.template.database.library.Datatype.LONG;
import static net.cocooncreations.template.database.library.Datatype.REAL;
import static net.cocooncreations.template.database.library.Datatype.TEXT;

/**
 * Created by aloupas on 2/5/2017.
 */

public abstract class BaseModel<T extends BaseModel> {

    private static String TAG = "BaseModel";

    public Class<T> modelClass;
    public BaseContentValues baseContentValues;

    public BaseModel(Class<T>  tClass){
        this.modelClass = tClass;
        this.baseContentValues = new BaseContentValues(this);
    }

    protected abstract void assignValues(JSONObject jsonObject);

    protected abstract void assignValues(Document document);

    protected abstract void assignValues(String[] stringArray);

    protected void loadCursor(Cursor cursor) {

        for (Field field : getClass().getDeclaredFields()) {

            if (field.isAnnotationPresent(Database.class)) {

                Database annotation = field.getAnnotation(Database.class);

                try {
                    if (annotation.dataType() == TEXT) {
                        if (cursor.getColumnIndex(field.getName()) != -1)
                            field.set(this, cursor.getString(cursor.getColumnIndex(field.getName())));
                    } else if (annotation.dataType() == INTEGER) {
                        if (cursor.getColumnIndex(field.getName()) != -1)
                            field.set(this, cursor.getInt(cursor.getColumnIndex(field.getName())));
                    } else if (annotation.dataType() == BYTES) {
                        if (cursor.getColumnIndex(field.getName()) != -1)
                            field.set(this, cursor.getBlob(cursor.getColumnIndex(field.getName())));
                    } else if (annotation.dataType() == REAL) {
                        if (cursor.getColumnIndex(field.getName()) != -1)
                            field.set(this, cursor.getFloat(cursor.getColumnIndex(field.getName())));
                    } else if (annotation.dataType() == LONG) {
                        if (cursor.getColumnIndex(field.getName()) != -1)
                            field.set(this, cursor.getLong(cursor.getColumnIndex(field.getName())));
                    } else if (annotation.dataType() == FOREIGN_KEY) {
                        if (cursor.getColumnIndex(field.getName()) != -1)
                            field.set(this, cursor.getInt(cursor.getColumnIndex(field.getName())));
                    } else if (annotation.dataType() == DATE) {
                        if (cursor.getColumnIndex(field.getName()) != -1)
                            field.set(this, new Date(cursor.getLong(cursor.getColumnIndex(field.getName()))));
                    } else if (annotation.dataType() == BOOLEAN) {
                        if (cursor.getColumnIndex(field.getName()) != -1)
                            field.set(this, cursor.getInt(cursor.getColumnIndex(field.getName())) > 0);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }
    }


    public void save() {
        String table = DatabaseUtils.getTableName(modelClass);
        String nullColumnHack = "";
        ContentValues contentValues = baseContentValues.get();
        int conflictAlgorithm = SQLiteDatabase.CONFLICT_REPLACE;
        save(table, nullColumnHack, contentValues, conflictAlgorithm);

    }

    public void save(int condflictAlgorithm) {
        String table = DatabaseUtils.getTableName(modelClass);
        String nullColumnHack = "";
        ContentValues contentValues = baseContentValues.get();
        save(table, nullColumnHack, contentValues, condflictAlgorithm);
    }

    private void save(String table, String nullColumnHack, ContentValues contentValues, int conflictAlgorithm) {
        MySQLiteHelper.getInstance().getMyWritableDatabase().insertWithOnConflict(table, nullColumnHack, contentValues, conflictAlgorithm);
    }

    public void delete() {

        String table = DatabaseUtils.getTableName(modelClass);
        String whereClause = "";
        String[] whereArgs = null;

        MySQLiteHelper.getInstance().getMyWritableDatabase().delete(table, whereClause, whereArgs);
    }

    public void updateInclude(String... fields) {
        String table = DatabaseUtils.getTableName(modelClass);
        String nullColumnHack = "";
        ContentValues contentValues = baseContentValues.include(fields).get();
        int conflictAlgorithm = SQLiteDatabase.CONFLICT_REPLACE;

        save(table, nullColumnHack, contentValues, conflictAlgorithm);
    }

    public void updateExclude(String... fields) {
        String table = DatabaseUtils.getTableName(modelClass);
        String nullColumnHack = "";
        ContentValues contentValues = baseContentValues.exclude(fields).get();
        int conflictAlgorithm = SQLiteDatabase.CONFLICT_REPLACE;
        save(table, nullColumnHack, contentValues, conflictAlgorithm);

    }

    @Override
    public String toString() {

        String gay = "";
        String comma = "";

        for (Field field : getClass().getDeclaredFields()) {
            try {
                gay += comma + field.get(this).toString();
                comma = ",";
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return gay;
    }

}
