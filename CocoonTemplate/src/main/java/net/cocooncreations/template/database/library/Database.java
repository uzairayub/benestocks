package net.cocooncreations.template.database.library;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by aloupas on 2/5/2017.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Database {

    boolean primaryKey() default  false;

    boolean notNull() default false;

    String defaultValue() default "null";

    Datatype dataType();


}
