package net.cocooncreations.template.database.library;

import android.content.ContentValues;
import android.util.Log;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by zeniosagapiou on 21/02/17.
 */

public class BaseContentValues {

    private BaseModel baseModel;
    protected ContentValuesGetter contentValuesGetter;
    private List<String> includeValuesList;
    private List<String> excludeValuesList;

    public BaseContentValues(BaseModel baseModel) {
        this.baseModel = baseModel;
        this.contentValuesGetter = new ContentValuesGetter();
        includeValuesList = new ArrayList<>();
        excludeValuesList = new ArrayList<>();
    }

    public ContentValuesGetter exclude(String... excludeValues) {
        includeValuesList.clear();
        excludeValuesList.clear();
        for (String value : excludeValues) {
            excludeValuesList.add(value);
        }
        return contentValuesGetter;
    }

    public ContentValuesGetter include(String... includeValues) {
        includeValuesList.clear();
        excludeValuesList.clear();
        for (String value : includeValues) {
            includeValuesList.add(value);
        }
        return contentValuesGetter;
    }

    public ContentValues get() {
        includeValuesList.clear();
        excludeValuesList.clear();
        return contentValuesGetter.get();
    }

    public class ContentValuesGetter {

        private static final String TAG = "ContentValuesGetter";

        public ContentValues get() {

            ContentValues contentValues = new ContentValues();

            for (Field field : baseModel.getClass().getDeclaredFields()) {

                if (field.isAnnotationPresent(Database.class)) {

                    if ((includeValuesList.isEmpty() || includeValuesList.contains(field.getName()))
                            && !excludeValuesList.contains(field.getName())) {

                        Database annotation = field.getAnnotation(Database.class);

                        try {
                            if (annotation.dataType() == Datatype.TEXT) {
                                contentValues.put(field.getName(), (String) field.get(baseModel));
                            } else if (annotation.dataType() == Datatype.INTEGER) {
                                contentValues.put(field.getName(), (int) field.get(baseModel));
                            } else if (annotation.dataType() == Datatype.BOOLEAN) {
                                contentValues.put(field.getName(), (boolean) field.get(baseModel) ? 1 : 0);
                            } else if (annotation.dataType() == Datatype.BYTES) {
                                contentValues.put(field.getName(), (byte[]) field.get(baseModel));
                            } else if (annotation.dataType() == Datatype.REAL) {
                                contentValues.put(field.getName(), (float) field.get(baseModel));
                            } else if (annotation.dataType() == Datatype.LONG) {
                                contentValues.put(field.getName(), (long) field.get(baseModel));
                            } else if (annotation.dataType() == Datatype.FOREIGN_KEY) {
                                contentValues.put(field.getName(), (int) field.get(baseModel));
                            } else if (annotation.dataType() == Datatype.DATE) {
                                Date date = (Date) field.get(baseModel);
                                if (date != null)
                                    contentValues.put(field.getName(), date.getTime());
                            }
                        } catch (IllegalAccessException ex) {
                            ex.printStackTrace();
                        } catch (ClassCastException ex) {
                            ex.printStackTrace();
                        } catch (NullPointerException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }

            Log.d(TAG, contentValues.toString());

            return contentValues;

        }

    }

}
