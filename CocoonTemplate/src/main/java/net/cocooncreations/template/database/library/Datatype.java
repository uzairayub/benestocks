package net.cocooncreations.template.database.library;

public enum Datatype {
    TEXT("TEXT"), INTEGER("INTEGER"), BYTES("BLOB"), REAL("REAL"), NULL("TEXT"), LONG("INTEGER"), FOREIGN_KEY("INTEGER"), DATE("INTEGER"), BOOLEAN("INTEGER");

    public final String value;

    Datatype(final String value) {
        this.value = value;
    }
}