package net.cocooncreations.template.database.library;

/**
 * Created by zeniosagapiou on 21/02/17.
 */

public class IllegalTypeException extends RuntimeException{
    public IllegalTypeException(String message){
        super(message);
    }
}
