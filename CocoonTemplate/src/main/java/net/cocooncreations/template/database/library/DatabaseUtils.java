package net.cocooncreations.template.database.library;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import net.cocooncreations.template.database.MySQLiteHelper;

import org.json.JSONObject;
import org.w3c.dom.Document;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by aloupas on 2/6/2017.
 */

public class DatabaseUtils {

    public static <T extends BaseModel> String getCreateTableString(Class<T> t) {

        String createTable = "CREATE TABLE IF NOT EXISTS %s (%s);";
        String comma = "";
        String columns = "";

        for (Field field : t.getDeclaredFields()) {

            if (field.isAnnotationPresent(Database.class)) {
                Database annotation = field.getAnnotation(Database.class);

                columns += comma + field.getName() + " " + annotation.dataType().value + (annotation.primaryKey() ? " PRIMARY KEY" : "") + (annotation.notNull() ? " NOT NULL" : "");
                comma = ", ";
            }
        }
        return String.format(createTable, t.getSimpleName(), columns);

    }

    public static <T extends BaseModel> String getTableName(Class<T> modelClass) {
        return modelClass.getSimpleName();
    }

    public static <T extends BaseModel> T createModel(JSONObject jsonObject, Class<T> modelClass) throws IllegalAccessException, InstantiationException {

        T baseModel = modelClass.newInstance();
        baseModel.modelClass = modelClass;
        baseModel.assignValues(jsonObject);
        return baseModel;
    }

    public static <T extends BaseModel> T createModel(Document document, Class<T> modelClass) {

        try {
            T baseModel = modelClass.newInstance();
            baseModel.modelClass = modelClass;
            baseModel.assignValues(document);
            return baseModel;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static <T extends BaseModel> T createModel(String[] stringArray, Class<T> modelClass) {

        try {
            T baseModel = modelClass.newInstance();
            baseModel.modelClass = modelClass;
            baseModel.assignValues(stringArray);
            return baseModel;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    public static <T extends BaseModel> T createModel(Cursor cursor, Class<T> modelClass) {

        if (cursor != null) {
            try {
                T baseModel = modelClass.newInstance();
                baseModel.modelClass = modelClass;
                baseModel.loadCursor(cursor);
                return baseModel;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    private static <T extends BaseModel> void saveModels(List<T> models, String nullColumnHack, int conflictAlgorithm) {

        try {
            MySQLiteHelper.getInstance().getMyWritableDatabase().beginTransaction();
            for (T model : models) {
                MySQLiteHelper.getInstance().getMyWritableDatabase().insertWithOnConflict(model.getClass().getSimpleName(), nullColumnHack, model.baseContentValues.get(), conflictAlgorithm);
            }
            MySQLiteHelper.getInstance().getMyWritableDatabase().setTransactionSuccessful();
        } finally {
            MySQLiteHelper.getInstance().getMyWritableDatabase().endTransaction();
        }
    }

    public static <T extends BaseModel> void updateModelsExclude(List<T> models, String nullColumnHack, int conflictAlgorithm, String... excludeValues) {

        try {
            MySQLiteHelper.getInstance().getMyWritableDatabase().beginTransaction();
            for (T model : models) {
                MySQLiteHelper.getInstance().getMyWritableDatabase().insertWithOnConflict(model.getClass().getSimpleName(), nullColumnHack, model.baseContentValues.exclude(excludeValues).get(), conflictAlgorithm);
            }
            MySQLiteHelper.getInstance().getMyWritableDatabase().setTransactionSuccessful();
        } finally {
            MySQLiteHelper.getInstance().getMyWritableDatabase().endTransaction();
        }
    }

    public static <T extends BaseModel> void updateModelsInclude(List<T> models, String nullColumnHack, int conflictAlgorithm, String includeValues) {

        try {
            MySQLiteHelper.getInstance().getMyWritableDatabase().beginTransaction();
            for (T model : models) {
                MySQLiteHelper.getInstance().getMyWritableDatabase().insertWithOnConflict(model.getClass().getSimpleName(), nullColumnHack, model.baseContentValues.include(includeValues).get(), conflictAlgorithm);
            }
            MySQLiteHelper.getInstance().getMyWritableDatabase().setTransactionSuccessful();
        } finally {
            MySQLiteHelper.getInstance().getMyWritableDatabase().endTransaction();
        }
    }

    public static <T extends BaseModel> void saveModels(List<T> models, int condflictAlgorithm) {
        String nullColumnHack = "";
        saveModels(models, nullColumnHack, condflictAlgorithm);
    }

    public static <T extends BaseModel> void saveModels(List<T> models) {
        String nullColumnHack = "";
        saveModels(models, nullColumnHack, SQLiteDatabase.CONFLICT_REPLACE);
    }


    public static <T extends BaseModel> void delete(Class<T> modelClass, String whereClause, String[] whereArgs) {
        MySQLiteHelper.getInstance().getMyWritableDatabase().delete(modelClass.getSimpleName(), whereClause, whereArgs);

    }


    public static <T extends BaseModel> List<T> getModelList(Class<T> modelClass, String[] columns, String selection, String[] selectionArgs, String orderBy, String limit) {

        Cursor cursor = MySQLiteHelper.getInstance().getMyWritableDatabase().query(modelClass.getSimpleName(), columns, selection, selectionArgs, null, null, orderBy, limit);

        List<T> list = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                list.add(DatabaseUtils.createModel(cursor, modelClass));
            } while (cursor.moveToNext());
        }

        cursor.close();

        return list;
    }

    public static <T extends BaseModel> List<T> getModelList(Class<T> modelClass, String[] columns, String selection, String[] selectionArgs) {
        return getModelList(modelClass, columns, selection, selectionArgs, null, null);
    }

    public static <T extends BaseModel> List<T> getModelList(Class<T> modelClass, String selection, String[] selectionArgs) {
        return getModelList(modelClass, null, selection, selectionArgs);
    }

    public static <T extends BaseModel> List<T> getModelList(Class<T> modelClass) {
        return getModelList(modelClass, null, null);
    }


}
