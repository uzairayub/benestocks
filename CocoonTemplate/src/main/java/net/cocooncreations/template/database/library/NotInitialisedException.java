package net.cocooncreations.template.database.library;

public class NotInitialisedException extends RuntimeException {

  public NotInitialisedException(String message){
     super(message);
  }

}