package com.fxvalley.exchanger.benestocks.utilities;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fxvalley.exchanger.benestocks.R;
import com.fxvalley.exchanger.benestocks.models.Account;

import java.text.DecimalFormat;

public class TopHederViewAdapter extends RecyclerView.Adapter<TopHeaderViewholder> {
    private static final String TAG = "Adapter" ;

    private static final int C = 0;
    private static final int UM = 1;
    private static final int P_AND_L = 2;
    private static final int SL = 3;

    static final int ITEM_COUNT = 4;
//    private final int width;

    private String c, um, pAndL, sl;
    DecimalFormat myFormat = new DecimalFormat("#.##");
    private Context context;

    public TopHederViewAdapter(Context context){
        this.context = context;
//        DisplayMetrics displaymetrics = new DisplayMetrics();
//        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
//        width = displaymetrics.widthPixels;
    }
    @Override
    public TopHeaderViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TopHeaderViewholder(LayoutInflater.from(parent.getContext()).inflate(R.layout.textview, parent, false));
    }

    @Override
    public void onBindViewHolder(final TopHeaderViewholder holder, int position) {

        ViewGroup.LayoutParams layoutParams = holder.itemView.getLayoutParams();
//        layoutParams.width = width / ITEM_COUNT;

        switch (position) {
            case C:
                holder.value.setText(String.valueOf(c));
                holder.key.setText("Capital : ");

                if(c == null)
                {
                    setValues(Constants.CAPITAL,holder.value);
                }
                else
                {
                    SPUtils.getInstance(context).saveStringPreferences(Constants.CAPITAL,String.valueOf(c));
                }
                break;
            case UM:
                holder.value.setText(String.valueOf(um));
                holder.key.setText("Used Margin : ");

                if(um == null)
                {
                    setValues(Constants.MARGIN,holder.value);
                }
                else
                {
                    SPUtils.getInstance(context).saveStringPreferences(Constants.MARGIN,String.valueOf(c));
                }

                break;
            case P_AND_L:
                changeColor(holder.value);
                holder.value.setText(String.valueOf(pAndL));
                holder.key.setText("Profit/Loss : ");

                if(pAndL == null)
                {
                    setValues(Constants.PROFIT_LOSS,holder.value);
                }
                else
                {
                    SPUtils.getInstance(context).saveStringPreferences(Constants.PROFIT_LOSS,String.valueOf(c));
                }
                break;
            case SL:
                holder.value.setText(String.valueOf(sl));
                holder.key.setText("Stopout : ");
                if(sl == null)
                {
                    setValues(Constants.STOP_OUT,holder.value);
                }
                else
                {
                    SPUtils.getInstance(context).saveStringPreferences(Constants.STOP_OUT,String.valueOf(c));
                }
                break;
        }
    }

    private void setValues(String key, TextView textView)
    {

        if(key.equals(Constants.CAPITAL))
        {
            textView.setText(SPUtils.getInstance(context).getStringPreferences(Constants.CAPITAL));
        }
        else if(key.equals(Constants.MARGIN))
        {
            String value = SPUtils.getInstance(context).getStringPreferences(Constants.MARGIN);
            textView.setText(value);
        }
        else if(key.equals(Constants.PROFIT_LOSS))
        {
            textView.setText(SPUtils.getInstance(context).getStringPreferences(Constants.PROFIT_LOSS));
        }
        else if(key.equals(Constants.STOP_OUT))
        {
            textView.setText(SPUtils.getInstance(context).getStringPreferences(Constants.STOP_OUT));
        }
    }

    private void changeColor(TextView value)
    {
        if(String.valueOf(pAndL).contains("-"))
        {
            value.setTextColor(ContextCompat.getColor(context,R.color.button_red));
            return;
        }
        value.setTextColor(ContextCompat.getColor(context,R.color.button_green));
    }

    @Override
    public int getItemCount() {
        return ITEM_COUNT;
    }
//
//    public void updateTopHeaderValues(Account account) {
//
//
//        c = myFormat.format(account.C + account.CPL);
//        um = myFormat.format(account.UC);
//        pAndL = myFormat.format(account.OPL);
//        sl = myFormat.format(account.SO);
//
//        notifyDataSetChanged();
//    }

    public void updateTopHeaderValues(Account account, double openTotal) {

        /*Log.i(TAG, "update top header");
        Log.i(TAG, "account" + account.C);
        Log.i(TAG, "CPL" + account.CPL);*/
       // Log.i(TAG, "openTotal" + openTotal);

        c = myFormat.format(account.C + account.CPL + openTotal);
        um = myFormat.format(account.UC);
        pAndL = myFormat.format(openTotal);
        sl = myFormat.format(account.SO);

        notifyDataSetChanged();
    }

}


