package com.fxvalley.exchanger.benestocks.trades;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.fxvalley.exchanger.benestocks.R;
import com.fxvalley.exchanger.benestocks.activities.LoginActivity;
import com.fxvalley.exchanger.benestocks.charts.ChartUtilities;
import com.fxvalley.exchanger.benestocks.data.TradeRepository;
import com.fxvalley.exchanger.benestocks.models.Account;
import com.fxvalley.exchanger.benestocks.models.ChartData;
import com.fxvalley.exchanger.benestocks.models.ContractSpecs;
import com.fxvalley.exchanger.benestocks.models.Instrument;
import com.fxvalley.exchanger.benestocks.models.Trade;
import com.fxvalley.exchanger.benestocks.models.User;
import com.fxvalley.exchanger.benestocks.sockets.ChartSocket;
import com.fxvalley.exchanger.benestocks.sockets.ChartSocketListener;
import com.fxvalley.exchanger.benestocks.sockets.PriceSessionSocket;
import com.fxvalley.exchanger.benestocks.sockets.PriceSocketListener;
import com.fxvalley.exchanger.benestocks.sockets.SocketUtilities;
import com.fxvalley.exchanger.benestocks.sockets.TradeSessionSocket;
import com.fxvalley.exchanger.benestocks.sockets.TradeSocketsConnectionListener;
import com.fxvalley.exchanger.benestocks.utilities.AccountSnapshot;
import com.fxvalley.exchanger.benestocks.utilities.ChartSelector;
import com.fxvalley.exchanger.benestocks.utilities.Constants;
import com.fxvalley.exchanger.benestocks.utilities.EditPositionDataTextWatcher;
import com.fxvalley.exchanger.benestocks.utilities.ToastView;
import com.fxvalley.exchanger.benestocks.utilities.TopHederViewAdapter;
import com.fxvalley.exchanger.benestocks.utilities.validation.BuyValidation;
import com.fxvalley.exchanger.benestocks.utilities.validation.EditPositionTextValidator;
import com.fxvalley.exchanger.benestocks.utilities.validation.SellValidation;
import com.github.mikephil.charting.charts.CandleStickChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.LabelPosition;
import com.github.mikephil.charting.components.Line;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.CandleData;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import static com.fxvalley.exchanger.benestocks.utilities.Constants.ACCOUNT;
import static com.fxvalley.exchanger.benestocks.utilities.Constants.TRADE;

public class PositionsActivity extends AppCompatActivity implements TradeSocketsConnectionListener, PriceSocketListener, ChartSocketListener {

    private TopHederViewAdapter topheaderAdapter;
    private RecyclerView topHeaderRecyclerView;

    private TextView positionsTicketMarginTextview;
    private TextView positionsInterestCommitionTextview;
    private EditText stopProfitEdiText;
    private EditText stopLostEdiText;
    private EditText strategyEdiText;
    private CandleStickChart positionsChart;

    private TextView rowPositionsBuySell;
    private TextView rowPositionsCurrencyPair;
    private TextView rowTradesAmount;
    private TextView rowPositionsGrossPL;
    private TextView rowPositionsDate;
    private TextView rowPositionsRight;
    private TextView rowPositionsRightValue;
    private TextView rowPositionsLeft;

    private TradeSessionSocket tradeSessionSocket;
    private PriceSessionSocket priceSessionSocket;
    private ChartSocket chartSocket;
    public AccountSnapshot accountSnapshot = new AccountSnapshot();

    private ChartSelector chartSelector = new ChartSelector();

    private Trade trade;
    private int user_filter;
    private View progressView;
    private TextView progresTextView;

    private boolean isForSave = false;

    private BroadcastReceiver tradeRespondedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getBooleanExtra(Constants.TRADE_RESPONSE, false)) {

                Toast.makeText(context, R.string.trade_succefull, Toast.LENGTH_LONG).show();
                PositionsActivity.super.onBackPressed();

            } else {
                progressView.setVisibility(View.GONE);
                Toast.makeText(PositionsActivity.this, R.string.request_failed, Toast.LENGTH_LONG).show();
            }
        }
    };

    private User user;
    private Handler handler;
    private Account account;
    private MenuItem editItem;
    private EditPositionTextValidator editPositionTextChecker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        handler = new Handler();

        setContentView(R.layout.activity_positions);

        if (savedInstanceState == null) {
            trade = (Trade) getIntent().getSerializableExtra(TRADE);
            user_filter = getIntent().getIntExtra(Constants.USER_FILTER, 0);
            account = (Account) getIntent().getSerializableExtra(ACCOUNT);
        } else {
            trade = (Trade) savedInstanceState.getSerializable(TRADE);
            user_filter = savedInstanceState.getInt(Constants.USER_FILTER);
            account = (Account) savedInstanceState.getSerializable(Constants.ACCOUNT);

        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.positionsToolbar);
        toolbar.setTitle(R.string.edit_close);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> {
            onBackPressed();
        });

        chartSelector.onCreate(this, savedInstanceState);

        initChartData();


        initViews();
        loadValuesToRow(trade);

        positionsTicketMarginTextview.setText(getString(R.string.ticket_margin, trade.ticket, String.valueOf(trade.col.intValue())));
        positionsInterestCommitionTextview.setText(getString(R.string.interest_commition, Trade.format(trade._int), Trade.format(trade.com)));
        stopProfitEdiText.setText(Trade.format(trade.stopProfit));
        stopLostEdiText.setText(Trade.format(trade.stopLost));
        invalidateOptionsMenu();


        editPositionTextChecker = new EditPositionTextValidator(
                isBuyState() ? new BuyValidation() : new SellValidation(),
                rowPositionsRightValue,
                stopProfitEdiText,
                stopLostEdiText,
                strategyEdiText, (isValid) -> {
            editItem.setEnabled(isValid);
        });

        stopProfitEdiText.addTextChangedListener(new EditPositionDataTextWatcher(editPositionTextChecker));
        stopLostEdiText.addTextChangedListener(new EditPositionDataTextWatcher(editPositionTextChecker));
        strategyEdiText.addTextChangedListener(new EditPositionDataTextWatcher(editPositionTextChecker));

        progressView = findViewById(R.id.progressView);
        progresTextView = (TextView) progressView.findViewById(R.id.progressTextView);
    }

    private boolean isBuyState() {
        return trade.type == 1;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.positions, menu);
        editItem = menu.findItem(R.id.save);
        editPositionTextChecker.check();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.close) {
            new MaterialDialog.Builder(PositionsActivity.this)
                    .title(R.string.close_trade)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            tradeSessionSocket.sendTrade(closeTrade());
                            progresTextView.setText(R.string.sending_request);
                            progressView.setVisibility(View.VISIBLE);

                        }
                    })
                    .positiveText(R.string.close)
                    .content(R.string.are_you_sure_close)
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    })
                    .negativeText(R.string.cancel)
                    .show();
        } else if (item.getItemId() == R.id.save) {
            tradeSessionSocket.sendTrade(updateTrade());
            progresTextView.setText(R.string.sending_request);
            progressView.setVisibility(View.VISIBLE);
        }

        return true;
    }


    private void loadValuesToRow(Trade trade) {

        rowPositionsRight.setVisibility(View.GONE);
        rowPositionsRightValue.setVisibility(View.GONE);
        if (user_filter == TradeRepository.OPEN) {
            rowPositionsLeft.setText("Open: " + trade.open);
            rowPositionsRight.setVisibility(View.VISIBLE);
            rowPositionsRightValue.setVisibility(View.VISIBLE);
            rowPositionsDate.setText(trade.openTime);
            rowPositionsGrossPL.setText(Trade.format(trade.calculatePL(TradeRepository.OPEN)));

            if (trade.calculatePL(TradeRepository.OPEN) < 0) {
                rowPositionsGrossPL.setTextColor(ContextCompat.getColor(this, R.color.button_red));
            } else {
                rowPositionsGrossPL.setTextColor(ContextCompat.getColor(this, R.color.button_green));
            }

            rowPositionsGrossPL.setText(Trade.format(trade.calculatePL(TradeRepository.OPEN)));
        } else if (user_filter == TradeRepository.CLOSED) {

            rowPositionsLeft.setText("Open: " + trade.open);
            rowPositionsRight.setVisibility(View.VISIBLE);
            rowPositionsRightValue.setVisibility(View.VISIBLE);
            rowPositionsRight.setText("Closed: ");
            rowPositionsRightValue.setText(String.valueOf(trade.close));
            rowPositionsDate.setText(trade.closeTime);

            rowPositionsGrossPL.setText(Trade.format(trade.calculatePL(TradeRepository.CLOSED)));

            if (trade.calculatePL(TradeRepository.CLOSED) < 0) {
                rowPositionsGrossPL.setTextColor(ContextCompat.getColor(this, R.color.button_red));
            } else {

                rowPositionsGrossPL.setTextColor(ContextCompat.getColor(this, R.color.button_green));
            }

        } else if (user_filter == TradeRepository.PENDING) {

            rowPositionsRight.setVisibility(View.GONE);
            rowPositionsRightValue.setVisibility(View.GONE);
            rowPositionsLeft.setText("Pending: " + trade.pendingCancelled);
            rowPositionsDate.setText(trade.pendingCancelledTime);
            rowPositionsGrossPL.setText("");

        } else if (user_filter == TradeRepository.CANCELLED) {


            rowPositionsRight.setVisibility(View.GONE);
            rowPositionsRightValue.setVisibility(View.GONE);
            rowPositionsLeft.setText("Cancelled: " + trade.pendingCancelled);
            rowPositionsDate.setText(trade.pendingCancelledTime);
            rowPositionsGrossPL.setText("");
        }

        if (trade.type == 1) {
            rowPositionsBuySell.setText("B");
            rowPositionsBuySell.setBackground(ContextCompat.getDrawable(this, R.drawable.buy_circle));
            if (user_filter == TradeRepository.OPEN) {
                rowPositionsRight.setText("Current: ");
                rowPositionsRightValue.setText(String.valueOf(trade.instrument.instrumentLiveData != null ? trade.instrument.instrumentLiveData.bid : "-"));
            }

        } else {
            rowPositionsBuySell.setText("S");
            rowPositionsBuySell.setBackground(ContextCompat.getDrawable(this, R.drawable.sell_circle));
            if (user_filter == TradeRepository.OPEN) {
                rowPositionsRight.setText("Current: ");
                rowPositionsRightValue.setText(String.valueOf(trade.instrument.instrumentLiveData != null ? trade.instrument.instrumentLiveData.ask : "-"));
            }
        }

        rowPositionsCurrencyPair.setText(trade.instrument.fixName);
        rowTradesAmount.setText(trade.volume + "");


    }

    private void initViews() {

        topHeaderRecyclerView = (RecyclerView) findViewById(R.id.status_recyclerview);
        topHeaderRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        topHeaderRecyclerView.setAdapter(topheaderAdapter = new TopHederViewAdapter(this));

        positionsTicketMarginTextview = (TextView) findViewById(R.id.positionsTicketMarginTextview);
        positionsInterestCommitionTextview = (TextView) findViewById(R.id.positionsInterestCommitionTextview);
        stopProfitEdiText = (EditText) findViewById(R.id.stopProfitEdiText);
        stopLostEdiText = (EditText) findViewById(R.id.stopLostEdiText);
        strategyEdiText = (EditText) findViewById(R.id.strategyEdiText);
        positionsChart = (CandleStickChart) findViewById(R.id.positionsChart);

        rowPositionsBuySell = (TextView) findViewById(R.id.rowPositionsBuySell);
        rowPositionsCurrencyPair = (TextView) findViewById(R.id.rowPositionsCurrencyPair);
        rowTradesAmount = (TextView) findViewById(R.id.rowTradesAmount);
        rowPositionsGrossPL = (TextView) findViewById(R.id.rowPositionsGrossPL);
        rowPositionsDate = (TextView) findViewById(R.id.rowPositionsDate);
        rowPositionsRight = (TextView) findViewById(R.id.rowPositionsRight);
        rowPositionsRightValue = (TextView) findViewById(R.id.rowPositionsRightValue);
        rowPositionsLeft = (TextView) findViewById(R.id.rowPositionsLeft);

        if (user_filter != TradeRepository.OPEN) {

            stopProfitEdiText.setFocusableInTouchMode(false);
            stopProfitEdiText.setEnabled(false);

            stopLostEdiText.setFocusableInTouchMode(false);
            stopLostEdiText.setEnabled(false);

            strategyEdiText.setFocusableInTouchMode(false);
            strategyEdiText.setEnabled(false);

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(totalOpenReceived, new IntentFilter(Constants.UPDATE_STATUS_FILTER));
        LocalBroadcastManager.getInstance(this.getApplicationContext()).registerReceiver(chartsUpdatedReceiver, new IntentFilter(Constants.CHART_UPDATED));
        LocalBroadcastManager.getInstance(this.getApplicationContext()).registerReceiver(tradeRespondedReceiver, new IntentFilter(Constants.TRADE_RESPONDED));
        LocalBroadcastManager.getInstance(this.getApplicationContext()).registerReceiver(changeChartReceiver, new IntentFilter(Constants.CHANGE_CHART));

        user = User.getUser(this);

        if (user == null) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            return;
        }

        priceSessionSocket = new PriceSessionSocket(user);
        tradeSessionSocket = new TradeSessionSocket(user, new ToastView(this, new Handler()));

        accountSnapshot.onResume(this, tradeSessionSocket, priceSessionSocket, topheaderAdapter);

        chartSocket = new ChartSocket(user);

        priceSessionSocket.connect(this);
        tradeSessionSocket.connect(this);
        chartSocket.connect(this);

        handler.post(updateHeadersRunnable);


    }

    @Override
    protected void onPause() {
        super.onPause();

        accountSnapshot.onPause();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(totalOpenReceived);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(chartsUpdatedReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(ticksUpdatedReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(tradeRespondedReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(changeChartReceiver);

        if (handler != null)
            handler.removeCallbacksAndMessages(null);

        if (tradeSessionSocket != null)
            tradeSessionSocket.disconnect();

        if (priceSessionSocket != null) {
            priceSessionSocket.disconnect();
        }

        if (chartSocket != null) {
            chartSocket.disconnect();
        }


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(TRADE, trade);
        outState.putSerializable(ACCOUNT, account);
        outState.putInt(Constants.USER_FILTER, user_filter);
        chartSelector.onSavedInstanceState(outState);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    public void onPriceSocketStateChanged(SocketUtilities.ClientStatus clientStatus) {

    }


    private Runnable runnable = new Runnable() {
        @Override
        public void run() {

            loadValuesToRow(trade);

            handler.postDelayed(this, 1000);

        }
    };

    private BroadcastReceiver ticksUpdatedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int group_id = intent.getIntExtra(Constants.GROUP_ID, -1);
            int symbol_id = intent.getIntExtra(Constants.SYMBOL_ID, -1);


            if (trade.instrument.instrumentCategoryId == group_id && symbol_id == PositionsActivity.this.trade.instrument.id
                    && trade.instrument.instrumentLiveData != null) {

                Instrument instrument = priceSessionSocket.instrumentGroups.get(group_id).instruments.get(symbol_id);

                if (instrument.instrumentLiveData != null) {
                    PositionsActivity.this.trade.instrument.instrumentLiveData = instrument.instrumentLiveData;
                }
//                loadInstrumentValues(instrument);

            }
        }
    };

    @Override
    public void onCategoriesFecthed() {

        LocalBroadcastManager.getInstance(this).unregisterReceiver(ticksUpdatedReceiver);
        LocalBroadcastManager.getInstance(this.getApplicationContext()).registerReceiver(ticksUpdatedReceiver, new IntentFilter(Constants.TICK_UPDATED));

        handler.removeCallbacksAndMessages(null);
        handler.post(runnable);
        priceSessionSocket.subscribeToInstrument(trade.instrument);
        accountSnapshot.onPriceSessionSocketReady();

    }

    @Override
    public void onLastPriceFetched() {

    }

    @Override
    public void onTradeSocketStateChanged(SocketUtilities.ClientStatus clientStatus) {

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                switch (clientStatus) {
                    case CONNECTED:
                        tradeSessionSocket.getSession();
                        break;
                    case DISCONNECTED:


                        break;
                    case CONNECTING:

                        break;
                }
            }
        });
    }

    @Override
    public void onSessionFetched() {
        tradeSessionSocket.getAccount();
    }

    @Override
    public void onGetAccount() {
        accountSnapshot.onTradeSessionSocketReady();
    }

    @Override
    public void onAccountSwitched(boolean success) {

    }

    @Override
    public void onContractFetched(ContractSpecs contractSpecs) {

    }

    @Override
    public void onChartStateChanged(SocketUtilities.ClientStatus clientStatus) {

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                switch (clientStatus) {
                    case CONNECTED:
                        chartSelector.onChartSocketReady();


                        break;
                    case DISCONNECTED:
                        chartSelector.onChartSocketDisconnect();


                        break;
                    case CONNECTING:

                        break;
                }
            }
        });

    }

    private void initChartData() {

        positionsChart = (CandleStickChart) findViewById(R.id.positionsChart);
        positionsChart.getDescription().setEnabled(false);
        positionsChart.setDrawGridBackground(true);
        // scaling can now only be done on x- and y-axis separately
        positionsChart.setPinchZoom(true);
        positionsChart.setTouchEnabled(true);
        positionsChart.setDragEnabled(true);
        positionsChart.enableScroll();

        positionsChart.getXAxis().setGridColor(Color.LTGRAY);
        YAxis rightAxis = positionsChart.getAxisRight();
        rightAxis.setEnabled(false);

        YAxis leftAxis = positionsChart.getAxisLeft();
        leftAxis.setTextColor(Color.WHITE);
//        leftAxis.setEnabled(false);
        leftAxis.setLabelCount(7, false);
        leftAxis.setDrawGridLines(true);
        leftAxis.setDrawAxisLine(true);

        XAxis xAxis = positionsChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(true);
        xAxis.setLabelCount(6);
        xAxis.setTextColor(Color.WHITE);
        positionsChart.setGridBackgroundColor(getResources().getColor(R.color.windowBackground));
        positionsChart.getLegend().setEnabled(false);

    }

    public synchronized void setData(List<com.fxvalley.exchanger.benestocks.models.ChartData> chartData) {

        positionsChart.clear();

        ArrayList<String> xValues = new ArrayList<>();

        CandleData data = ChartUtilities.get().generateCandleData(chartData);

        for (ChartData chart : chartData) {
            xValues.add(chart.getChartDate(chartSelector.chartSelectorState));
        }

        positionsChart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                if (xValues.size() > (int) value) {
                    return xValues.get((int) value);
                } else return null;
            }
        });

        positionsChart.setData(data);
        if (chartData.size() > 0) {
            ChartData lastItem = chartData.get(chartData.size() - 1);
            float value = lastItem.close.floatValue();
            addHorizontalIndicator(value, lastItem.open < lastItem.close ? Color.GREEN : Color.RED);
        }
    }

    private void addHorizontalIndicator(float value, int color) {
        Line ll1 = new Line(value, String.valueOf(value));
        ll1.setLineWidth(1f);
        ll1.enableDashedLine(10f, 10f, 0f);
        ll1.setLabelPosition(LabelPosition.RIGHT_TOP);
        ll1.setTextSize(12f);
        ll1.setLineColor(color);
        ll1.setTextColor(color);
        YAxis leftAxis = positionsChart.getAxisLeft();
        leftAxis.removeAllLines(); // reset all limit lines to avoid overlapping lines
        leftAxis.addLine(ll1);
    }

    private BroadcastReceiver chartsUpdatedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (chartSocket != null && chartSocket.clientStatus == SocketUtilities.ClientStatus.CONNECTED) {
                List<ChartData> chartDatas = new ArrayList<>();

                chartDatas.addAll(chartSocket.chartDatas);

                setData(chartDatas);

            }

        }
    };

    public JSONObject updateTrade() {

        isForSave = true;
        JSONObject msg = new JSONObject();
        JSONObject order = new JSONObject();
        JSONArray messages = new JSONArray();

        JSONObject instrumentjson = new JSONObject();
        try {
            instrumentjson.put("Name", trade.instrument.name);
            instrumentjson.put("I", trade.instrument.id);
            order.put("Instrument", instrumentjson);

            order.put("I", trade.id);

            // IF IS BUY
            if (isBuyState()) {
                if (trade.instrument.instrumentLiveData != null) {
                    order.put("RP", trade.instrument.instrumentLiveData.ask);
                }
                order.put("S", 1);
            } else {
                order.put("RP", trade.instrument.instrumentLiveData.bid);
                order.put("S", 2);
            }

            order.put("VU", trade.volume);
            order.put("TIF", "GoodTillCancel");
            order.put("Ticket", trade.ticket);
//            order.put("RDT", null);
            order.put("ES", 2);
            order.put("SP", stopProfitEdiText.getText().toString().isEmpty() ? 0 : EditPositionTextValidator.getDouble(stopProfitEdiText.getText().toString()));
            order.put("SL", stopLostEdiText.getText().toString().isEmpty() ? 0 : EditPositionTextValidator.getDouble(stopLostEdiText.getText().toString()));
            order.put("UM", 3);
            order.put("LBL", strategyEdiText.getText().toString());
            order.put("TT", trade.tt);
            order.put("MId", 7);
//            order.put("PO", null);
//            order.put("EX", null);

            messages.put(order);
            msg.put("Messages", messages);
            msg.put("MessageType", 20);


        } catch (JSONException e) {
            e.printStackTrace();
        }


        return msg;

    }

    public JSONObject closeTrade() {

        isForSave = false;
        JSONObject msg = new JSONObject();
        JSONObject order = new JSONObject();
        JSONArray messages = new JSONArray();

        JSONObject instrumentjson = new JSONObject();
        try {
            instrumentjson.put("Name", trade.instrument.name);
            instrumentjson.put("I", trade.instrument.id);
            order.put("Instrument", instrumentjson);

            order.put("I", trade.id);

            // IF IS BUY
            if (isBuyState()) {
                order.put("RP", trade.instrument.instrumentLiveData.ask);
                order.put("S", 1);
            } else {
                order.put("RP", trade.instrument.instrumentLiveData.bid);
                order.put("S", 2);
            }

            order.put("VU", trade.volume);
            order.put("TIF", "GoodTillCancel");
            order.put("Ticket", trade.ticket);
//            order.put("RDT", null);
            order.put("ES", 2);
//            order.put("SP ", stopProfitEdiText.getText().toString().isEmpty() ? 0 : Double.valueOf(stopProfitEdiText.getText().toString()));
//            order.put("SL", stopLostEdiText.getText().toString().isEmpty() ? 0 : Double.valueOf(stopLostEdiText.getText().toString()));
            order.put("UM", 1);
            order.put("LBL", strategyEdiText.getText().toString());
            order.put("TT", trade.tt);
            order.put("MId", 7);
//            order.put("PO", null);
//            order.put("EX", null);

            messages.put(order);
            msg.put("Messages", messages);
            msg.put("MessageType", 20);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return msg;

    }

    private BroadcastReceiver totalOpenReceived = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            int selectedAccount = 0;
            for (int i = 0; i < tradeSessionSocket.accounts.size(); i++) {
                if (tradeSessionSocket.accounts.get(i).isActive) {
                    selectedAccount = i;
                    break;
                }
            }

            topheaderAdapter.updateTopHeaderValues(tradeSessionSocket.accounts.get(selectedAccount), intent.getDoubleExtra(Constants.CALCULATED_PL, 0.0));
        }
    };

    private Runnable updateHeadersRunnable = new Runnable() {
        private static final String TAG = "PositionsActivity";

        @Override
        public void run() {

            if (tradeSessionSocket != null && tradeSessionSocket.accountFetched) {

                tradeSessionSocket.getOpen(1, 0, 20).subscribe((result) -> {
                }, (err) -> {
                    Log.e(TAG, "onTradeSessionSocketReady: getOpen ", err);
                });
            }
            handler.postDelayed(this, 1000);

        }
    };

    private BroadcastReceiver changeChartReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            int chartPeriod = 15;
            int hoursToSubstract = 24;

            ChartSelector.ChartSelectorState chartSelectorState = (ChartSelector.ChartSelectorState) intent.getSerializableExtra("chartSelectorState");

            switch (chartSelectorState) {
                case FIFTEEN_MIN:
                    chartPeriod = 15;
                    hoursToSubstract = 24;
                    break;
                case THIRTY_MINS:
                    chartPeriod = 30;
                    hoursToSubstract = 48;
                    break;
                case ONE_HOUR:
                    chartPeriod = 60;
                    hoursToSubstract = 96;
                    break;
                case EIGHT_HOURS:
                    chartPeriod = 480;
                    hoursToSubstract = 768;
                    break;
                case ONE_DAY:
                    chartPeriod = 1440;
                    hoursToSubstract = 2304;
                    break;
            }
            Calendar calendar = GregorianCalendar.getInstance();
            long to = calendar.getTimeInMillis() / 1000;
            calendar.add(Calendar.HOUR_OF_DAY, -hoursToSubstract);
            long from = calendar.getTimeInMillis() / 1000;

            try {

                JSONObject msg = new JSONObject();
                JSONArray messages = new JSONArray();
                JSONObject info = new JSONObject();

                info.put("Name", trade.instrument.name);
                info.put("ChartType", 2);
                info.put("ChartPeriod", chartPeriod);
                info.put("ResponseType", 12);
                info.put("From", from);
                info.put("To", to);
                messages.put(info);

                msg.put("MessageType", 12);
                msg.put("Messages", messages);

                if (chartSocket != null && chartSocket.clientStatus == SocketUtilities.ClientStatus.CONNECTED)
                    chartSocket.getChart(msg.toString(), false);

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    };
}
