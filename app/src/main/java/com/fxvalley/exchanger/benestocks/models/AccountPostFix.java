package com.fxvalley.exchanger.benestocks.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by aloupas on 3/14/2017.
 */

public class AccountPostFix {

    public Integer i;
    public Integer tI;
    public Integer tAT;
    public Integer tAL;
    public TALD tALD;
    public Boolean a;
    public Integer fL;
    public Integer mL;
    public Integer eL;
    public Integer iL;
    public Integer tL;
    public Integer cId;
    public Integer c;
    public Integer uC;
    public Integer cPL;
    public Integer oPL;
    public Integer e;
    public Integer sO;
    public Integer mC;
    public Integer mCL;
    public Boolean mCA;
    public Integer sOL;
    public Boolean sOP;
    public Integer b;
    public Integer bA;
    public Integer bR;

    public class TALD {

        public Integer id;
        public String name;
        public Integer min;
        public Integer max;
        public Integer increment;
        public Integer commission;
        public Integer type;

        public TALD(JSONObject jsonObject) {
            i = jsonObject.optInt("Id");
            name = jsonObject.optString("Name");
            min = jsonObject.optInt("Min");
            max = jsonObject.optInt("Max");
            increment = jsonObject.optInt("Increment");
            commission = jsonObject.optInt("Commission");
            type = jsonObject.optInt("Type");
        }
    }

    public AccountPostFix(JSONObject jsonObject) {

        i = jsonObject.optInt("I");
        tI = jsonObject.optInt("TI");
        tAT = jsonObject.optInt("TAT");
        tAL = jsonObject.optInt("TAL");
        try {
            tALD = new TALD(jsonObject.getJSONObject("TALD"));
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        a = jsonObject.optBoolean("A");
        fL = jsonObject.optInt("FL");
        mL = jsonObject.optInt("ML");
        eL = jsonObject.optInt("EL");
        iL = jsonObject.optInt("IL");
        tL = jsonObject.optInt("TL");
        cId = jsonObject.optInt("CId");
        c = jsonObject.optInt("C");
        uC = jsonObject.optInt("UC");
        cPL = jsonObject.optInt("CPL");
        oPL = jsonObject.optInt("OPL");
        e = jsonObject.optInt("E");
        sO = jsonObject.optInt("SO");
        mC = jsonObject.optInt("MC");
        mCL = jsonObject.optInt("MCL");
        mCA = jsonObject.optBoolean("MCA");
        sOL = jsonObject.optInt("SOL");
        sOP = jsonObject.optBoolean("SOP");
        b = jsonObject.optInt("B");
        bA = jsonObject.optInt("BA");
        bR = jsonObject.optInt("BR");


    }
}
