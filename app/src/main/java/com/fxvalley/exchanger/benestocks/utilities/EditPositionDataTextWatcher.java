package com.fxvalley.exchanger.benestocks.utilities;

import android.text.Editable;
import android.text.TextWatcher;

import com.fxvalley.exchanger.benestocks.utilities.validation.ITextValidator;

public class EditPositionDataTextWatcher implements TextWatcher {

    private ITextValidator textChecker;

    public EditPositionDataTextWatcher(ITextValidator textChecker) {
        this.textChecker = textChecker;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        textChecker.check();
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}