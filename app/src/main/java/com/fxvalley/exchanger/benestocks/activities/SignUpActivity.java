package com.fxvalley.exchanger.benestocks.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.fxvalley.exchanger.benestocks.R;
import com.fxvalley.exchanger.benestocks.utilities.Constants;
import com.fxvalley.exchanger.benestocks.utilities.FadeOnTouchListener;

import net.cocooncreations.template.network.CocoonStringRequest;
import net.cocooncreations.template.network.RequestActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class SignUpActivity extends RequestActivity {

    public static final String FIRST_NAME = "FirstName";
    public static final String LASTNAME = "LastName";
    public static final String EMAIL = "Email";
    public static final String PHONE = "Phone";
    public static final String COUNTRY = "Country";
    public static final String USERNAME = "UserNamøe";
    public static final String PASSWORD = "Password";

    private final String PATH = "auth/register";
    private final String REGISTER_URL = Constants.REST_API_URL  + "auth/register";

    private EditText signUpFirstName;
    private EditText signUpLastName;
    private EditText signUpEmail;
    private EditText signUpUsername;
    private EditText signUpPhone;
    private EditText signUpPassword;
    private EditText signUpCountry;
    private View registerButton;

    private MaterialDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        Toolbar toolbar = (Toolbar) findViewById(R.id.signup_toolbar);
        toolbar.setTitle(R.string.reset_password);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        signUpFirstName = (EditText) findViewById(R.id.signUpFirstName);
        signUpLastName = (EditText) findViewById(R.id.signUpLastName);
        signUpEmail = (EditText) findViewById(R.id.signUpEmail);
        signUpUsername = (EditText) findViewById(R.id.signUpUsername);
        signUpPhone = (EditText) findViewById(R.id.signUpPhone);
        signUpPassword = (EditText) findViewById(R.id.signUpPassword);
        signUpCountry = (EditText) findViewById(R.id.signUpCountry);

        registerButton = findViewById(R.id.registerButton);

        registerButton.setOnTouchListener(new FadeOnTouchListener() {
            @Override
            public void onClick(View view, MotionEvent event) {

                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");

                HashMap<String, String> params = new HashMap<String, String>();
                params.put("username", signUpUsername.getText().toString());
                params.put("password", signUpPassword.getText().toString());
                params.put("firstName", signUpFirstName.getText().toString());
                params.put("lastName", signUpLastName.getText().toString());
                params.put("email", signUpEmail.getText().toString());
                params.put("phone",signUpPhone.getText().toString());
                params.put("country", "CY");
                params.put("referralCode", "mobileApp");
                params.put("referrer", "mobileApp");
                params.put("language", "EN");
                params.put("confirmPassword",  signUpPassword.getText().toString());

                CocoonStringRequest.with(REGISTER_URL, SignUpActivity.this).method(Request.Method.POST).headers(headers).params(params).callAsync();

                progressDialog.show();

            }
        });

        progressDialog = new MaterialDialog.Builder(this)
                .title(R.string.please_wait)
                .content(R.string.registering_account)
                .progress(true, 0).build();


    }

    @Override
    public <T> void onResponse(T response, String requestcode, NetworkResponse networkResponse) {

        if (requestcode.equals( Request.Method.POST + REGISTER_URL)) {

            if (networkResponse.statusCode == 200) {

                new MaterialDialog.Builder(this)
                        .title(R.string.congratulations)
                        .content(R.string.successfuly_registered)
                        .positiveText(R.string.ok).onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                }).show();

            }
            else{
                String message = getString(R.string.error_registering);
                try {
                    JSONObject jsonObject = new JSONObject((String) response);
                    message = jsonObject.getJSONObject("modelState").getJSONArray("").getString(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                new MaterialDialog.Builder(this)
                        .title(R.string.error)
                        .content(message)
                        .positiveText(R.string.ok).onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                    }
                }).show();
            }

        }

        if (progressDialog != null)
            progressDialog.dismiss();
    }

    @Override
    public void onError(VolleyError volleyError) {


        volleyError.printStackTrace();

        if (progressDialog != null)
            progressDialog.dismiss();

        String message = getString(R.string.error_registering);
        try {
            String response = new String(volleyError.networkResponse.data, "UTF-8");
            JSONObject jsonObject = new JSONObject((String) response);
            message = jsonObject.getJSONObject("modelState").getJSONArray("").getString(0);
        } catch (Exception e) {
            e.printStackTrace();
        }


        new MaterialDialog.Builder(this)
                .title(R.string.error)
                .content(message)
                .positiveText(R.string.ok).onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

            }
        }).show();
    }

    public JSONObject getSignUpJson() {

        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put(FIRST_NAME, signUpFirstName.getText().toString());
            jsonObject.put(LASTNAME, signUpLastName.getText().toString());
            jsonObject.put(EMAIL, signUpEmail.getText().toString());
            jsonObject.put(USERNAME, signUpUsername.getText().toString());
            jsonObject.put(PHONE, signUpPhone.getText().toString());
            jsonObject.put(PASSWORD, signUpPassword.getText().toString());
            jsonObject.put(COUNTRY, signUpCountry.getText().toString());
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        return jsonObject;
    }
}
