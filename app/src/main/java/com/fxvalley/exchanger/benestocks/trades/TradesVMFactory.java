package com.fxvalley.exchanger.benestocks.trades;

import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.fxvalley.exchanger.benestocks.sockets.TradeSessionSocket;

public class TradesVMFactory extends ViewModelProvider.NewInstanceFactory{

    private TradeSessionSocket tradeSessionSocket;

    public TradesVMFactory(TradeSessionSocket tradeSessionSocket) {
        this.tradeSessionSocket = tradeSessionSocket;
    }

    @NonNull
    @Override
    public TradesViewModel create(@NonNull Class modelClass) {
        return new TradesViewModel(tradeSessionSocket);
    }
}
