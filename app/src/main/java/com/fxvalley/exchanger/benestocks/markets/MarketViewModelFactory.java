package com.fxvalley.exchanger.benestocks.markets;

import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.fxvalley.exchanger.benestocks.sockets.PriceSessionSocket;

public class MarketViewModelFactory implements ViewModelProvider.Factory {

    private PriceSessionSocket priceSocket;

    public MarketViewModelFactory(PriceSessionSocket priceSocket) {
        this.priceSocket = priceSocket;
    }

    @NonNull
    @Override
    public  MarketsViewModel create(@NonNull Class modelClass) {
        return new MarketsViewModel(priceSocket);
    }
}
