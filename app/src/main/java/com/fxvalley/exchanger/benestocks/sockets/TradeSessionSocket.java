package com.fxvalley.exchanger.benestocks.sockets;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.util.Pair;

import com.fxvalley.exchanger.benestocks.MyApplication;
import com.fxvalley.exchanger.benestocks.data.TradeRepository;
import com.fxvalley.exchanger.benestocks.data.model.PagingResult;
import com.fxvalley.exchanger.benestocks.data.request.TradeRequest;
import com.fxvalley.exchanger.benestocks.models.Account;
import com.fxvalley.exchanger.benestocks.models.Contract;
import com.fxvalley.exchanger.benestocks.models.ContractSpecs;
import com.fxvalley.exchanger.benestocks.models.Instrument;
import com.fxvalley.exchanger.benestocks.models.MessageTypes;
import com.fxvalley.exchanger.benestocks.models.Trade;
import com.fxvalley.exchanger.benestocks.models.TradeStatus;
import com.fxvalley.exchanger.benestocks.models.User;
import com.fxvalley.exchanger.benestocks.utilities.Constants;
import com.fxvalley.exchanger.benestocks.utilities.IView;
import com.fxvalley.exchanger.benestocks.utilities.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.functions.Action1;
import rx.subjects.PublishSubject;

import ws.wamp.jawampa.WampClient;
import ws.wamp.jawampa.WampClientBuilder;
import ws.wamp.jawampa.connection.IWampConnectorProvider;
import ws.wamp.jawampa.transport.netty.NettyWampClientConnectorProvider;
import ws.wamp.jawampa.transport.netty.NettyWampConnectionConfig;

import static com.fxvalley.exchanger.benestocks.data.parse.GsonUtil.gsonToJson;


/**
 * Created by aloupas on 3/2/2017.
 */

public class TradeSessionSocket {

    private static final String TAG = "TradeSessionSocket";

    public SocketUtilities.ClientStatus clientStatus = SocketUtilities.ClientStatus.DISCONNECTED;
    private static final String TRADE_URL = Constants.WEB_SOCKET_API_URL + ":23024/ws";

    private WampClient client;
    private WampClientBuilder builder = new WampClientBuilder();
    private IWampConnectorProvider connectorProvider = new NettyWampClientConnectorProvider();

    private User user;
    private String sessionId;
    public boolean marketOpen;
    public PublishSubject<Boolean> marketOpenState = PublishSubject.create();
    public boolean accountFetched;

    public List<Trade> openPositions = new ArrayList<>();

    private TradeSocketsConnectionListener socketsConnectionListener;
    public ArrayList<Account> accounts;
    public int activeAccount;
    private IView view;

    public TradeSessionSocket(User user, IView view) {
        buildClient();
        this.view = view;
        this.user = user;
    }

    private void buildClient() {
        try {
            builder.withUri(TRADE_URL)
                    .withRealm("fxplayer")
                    .withInfiniteReconnects()
                    .withCloseOnErrors(true)
                    .withReconnectInterval(5, TimeUnit.SECONDS)
                    .withConnectorProvider(connectorProvider)
                    .withConnectionConfiguration(new NettyWampConnectionConfig.Builder().withMaxFramePayloadLength(Integer.MAX_VALUE).build());

            client = builder.build();
        } catch (Exception ex) {
            Log.e(TAG, "buildClient: ", ex);
        }
    }

    public void connect(TradeSocketsConnectionListener socketsConnectionListener) {

        this.socketsConnectionListener = socketsConnectionListener;

        client.statusChanged()
                .subscribe(new Action1<WampClient.State>() {
                    @Override
                    public void call(WampClient.State t1) {
                        if (t1 instanceof WampClient.ConnectedState) {
                            Log.d(TAG, "CONNECTED");
                            clientStatus = SocketUtilities.ClientStatus.CONNECTED;
                        } else if (t1 instanceof WampClient.DisconnectedState) {
                            accountFetched = false;
                            marketOpen = false;
                            marketOpenState.onNext(false);
                            sessionId = "";
                            Log.d(TAG, "DISCONNECTED");
                            clientStatus = SocketUtilities.ClientStatus.DISCONNECTED;
                        } else if (t1 instanceof WampClient.ConnectingState) {
                            Log.d(TAG, "CONNECTING");
                            clientStatus = SocketUtilities.ClientStatus.CONNECTING;
                        }
                        socketsConnectionListener.onTradeSocketStateChanged(clientStatus);
                    }
                });
        client.open();
    }

    public void disconnect() {
        client.close();
    }

    public void getContract(String name) {

        JSONObject msg = new JSONObject();
        JSONObject instrument = new JSONObject();
        JSONArray messages = new JSONArray();

        try {
            instrument.put("Name", name);
            messages.put(instrument);
            msg.put("Messages", messages);
            msg.put("MessageType", 15);

        } catch (JSONException e) {
            Log.e(TAG, "getContract: ", e);
        }

        client.call("com.fxplayer.contract", JSONObject.class, msg.toString())
                .subscribe(new Action1<JSONObject>() {

                    @Override
                    public void call(JSONObject reply) {
                        try {

                            JSONArray messages = reply.getJSONArray("Messages");

                            Log.d(TAG, messages.toString());

                            Instrument instrument = new Instrument(messages.getJSONObject(0));
                            Account account = new Account(messages.getJSONObject(1));
                            Contract contract = new Contract(messages.getJSONObject(2));

                            ContractSpecs contractSpecs = new ContractSpecs(contract, account, instrument);

                            socketsConnectionListener.onContractFetched(contractSpecs);

                        } catch (JSONException e) {
                            Log.e(TAG, "com.fxplayer.contract: ", e);
                        }

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable arg0) {
                        Log.w(TAG, "com.fxplayer.contract: ", arg0);
                    }
                });

    }

    public void getSession() {
        client.call("com.fxplayer.ping", JSONObject.class, getJsonMessage())
                .subscribe(new Action1<JSONObject>() {
                    @Override
                    public void call(JSONObject reply) {

//                Log.d(TAG, "user.token: " + user.token);
                        Log.d(TAG, "got session");
                        try {

                            JSONArray messages = reply.getJSONArray("Messages");
                            sessionId = messages.optString(0);
                            marketOpen = messages.optBoolean(1);
                            marketOpenState.onNext(marketOpen);


                        } catch (JSONException e) {
                            Log.e(TAG, "com.fxplayer.ping: ", e);
                        }

                        socketsConnectionListener.onSessionFetched();

                    }
                }, arg0 -> {
                    Log.w(TAG, "com.fxplayer.ping: ", arg0);
//                Log.d(TAG, reply);
                });

    }

    public Observable<Boolean> subscribeToTradeOpen(){
        return marketOpenState;
    }

    //
    public void subscribe() {

        Log.d(TAG, "Subscribing to session");

        if (marketOpen)
            client.makeSubscription(sessionId, JSONObject.class)
                    .subscribe(new Action1<JSONObject>() {
                        @Override
                        public void call(JSONObject jsonObject) {
                            try {
                                int messageType = jsonObject.getInt("MessageType");
                                switch (messageType) {
                                    case MessageTypes.ERROR: {

                                        break;
                                    }
                                    case MessageTypes.LOGON: {
                                        marketOpen = true;
                                        marketOpenState.onNext(marketOpen);
                                        view.showMessage("Market is open");
                                        break;
                                    }
                                    case MessageTypes.LOGOUT: {
                                        marketOpen = false;
                                        marketOpenState.onNext(marketOpen);
                                        view.showMessage("Market is closed");
                                        break;
                                    }
                                    case MessageTypes.TRADER: {
                                        view.showMessage("update trader account");
                                        updateAccount(accounts.get(activeAccount)); // update Account here
                                        break;
                                    }
                                    case MessageTypes.TRADE: {
                                        Log.i(TAG, "CallFirst");
                                        JSONArray messages = jsonObject.getJSONArray("Messages");
                                        JSONObject message = messages.getJSONObject(0);
                                        int tradeStatus = message.getInt("ES");
                                        view.showMessage(TradeStatus.getStatusText(tradeStatus));
                                        break;
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e(TAG, "makeSubscription call: ", e);
                            }

                            Log.d(TAG, "session id sub: " + jsonObject.toString());
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            Log.e(TAG, "makeSubscription call: ", throwable);
                        }
                    });

    }

    public void getAccount() {

        JSONObject message = new JSONObject();
        try {
//            message.put("Host", "login.thefxplayer.com");
            message.put("Host", Constants.WS_HOST);
            message.put("Token", user.token);
        } catch (JSONException e) {
            Log.e(TAG, "getAccount: ", e);
        }

        client.call("com.fxplayer.accountmob", JSONObject.class, sessionId)
                .subscribe(new Action1<JSONObject>() {
                    @Override
                    public void call(JSONObject reply) {

//              Utilities.printLongText(reply.toString());

                        accounts = new ArrayList<>();

                        try {

                            JSONArray messages = reply.getJSONArray("Messages");

                            Account activeAccountObject = new Account(messages.getJSONObject(0));

                            JSONArray accountsIn = messages.optJSONArray(1);
                            if (accountsIn != null) {
                                for (int i = 0; i < accountsIn.length(); i++) {
                                    Account tempAccount = new Account(accountsIn.getJSONObject(i));
                                    if (tempAccount.isActive) {
                                        accounts.add(activeAccountObject);
                                        activeAccount = i;
                                    } else {
                                        accounts.add(tempAccount);
                                    }
                                }
                            }

                        } catch (JSONException e) {
                            Log.e(TAG, "com.fxplayer.accountmob call: ", e);
                        }

                        accountFetched = true;
                        socketsConnectionListener.onGetAccount();

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable arg0) {
                        Log.e(TAG, "com.fxplayer.accountmob call: ", arg0);
                    }
                });
    }

    public Observable<Pair<Integer, List<Trade>>> getOpen(int type, final int page, int resultsPerPage) {
        String json = gsonToJson(new TradeRequest(type, page, resultsPerPage), TradeRequest.class);
        return client
                .call("com.fxplayer.open", String.class, json)
                .map(this::parseTradesUpdate)
                .doOnNext(items -> {
                    this.openPositions = items.second;
                })
                .doOnNext(pair -> {
                    List<Trade> trades = pair.second;
                    String instrumentsThatAreOpen[] = new String[openPositions.size()];
                    Double openTotal = 0.0;

                    for (int i = 0; i < openPositions.size(); i++) {
                        instrumentsThatAreOpen[i] = openPositions.get(i).instrument.name;
                        openTotal += openPositions.get(i).calculatePL(TradeRepository.OPEN);
                    }

                    Intent intent = new Intent(Constants.ACCOUNT_SNAPSHOT);
                    intent.putExtra(Constants.CALCULATED_PL, openTotal);
                    intent.putExtra(Constants.OPEN_TRADES, instrumentsThatAreOpen);
                    LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(intent);
                    Log.d(TAG, "open total send = " + openTotal);
                });
    }

    public Observable<Pair<Integer, List<Trade>>> getCancelled(int type, final int page, int resultsPerPage) {
        return client.call("com.fxplayer.cancelled", String.class,
                gsonToJson(new TradeRequest(type, page, resultsPerPage), TradeRequest.class))
                .map(this::parseTradesUpdate);
    }

    public Observable<Pair<Integer, List<Trade>>> getClosed(int type, final int page, int resultsPerPage) {
        return client.call("com.fxplayer.close", String.class,
                gsonToJson(new TradeRequest(type, page, resultsPerPage), TradeRequest.class))
                .map(this::parseTradesUpdate);
    }

    public Observable<Pair<Integer, List<Trade>>> getPending(int type, final int page, int resultsPerPage) {
        return client.call("com.fxplayer.pending", String.class,
                gsonToJson(new TradeRequest(type, page, resultsPerPage), TradeRequest.class))
                .map(this::parseTradesUpdate);
    }


    public void updateAccount(Account account) {

        JSONObject msg = new JSONObject();
        JSONObject info = new JSONObject();
        JSONArray messages = new JSONArray();

        try {
            info.put("I", account.id);
            messages.put(info);
            msg.put("Messages", messages);
            msg.put("MessageType", 35);
        } catch (JSONException e) {
            Log.e(TAG, "updateAccount: ", e);
        }

        Log.d(TAG, "updating account");
        Log.d(TAG, msg.toString());

        client.call("com.fxplayer.accounttype", JSONObject.class, msg.toString())
                .subscribe(new Action1<JSONObject>() {
                    @Override
                    public void call(JSONObject reply) {

                        socketsConnectionListener.onAccountSwitched(true);
                        Utilities.printLongText(reply.toString());
                        Log.d(TAG, "account  updated");

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable arg0) {
                        socketsConnectionListener.onAccountSwitched(false);
                        Log.e(TAG, "com.fxplayer.accounttype call: ", arg0);
//                Log.d(TAG, reply);
                    }
                });
    }


    public void sendTrade(JSONObject jsonObject) {
        Log.d(TAG, jsonObject.toString());
        Log.d(TAG, "sendTrade");

        client.call("com.fxplayer.trade", String.class, jsonObject.toString())
                .subscribe(new Action1<String>() {

                    @Override
                    public void call(String reply) {

                        Log.d(TAG, "Trade succeded" + reply);

                        Intent intent = new Intent(Constants.TRADE_RESPONDED);
                        intent.putExtra(Constants.TRADE_RESPONSE, true);

                        LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(intent);


//                Utilities.printLongText(reply);

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable arg0) {
                        Log.e(TAG, "call: ", arg0);
                        Intent intent = new Intent(Constants.TRADE_RESPONDED);
                        intent.putExtra(Constants.TRADE_RESPONSE, false);
                        Log.e(TAG, "com.fxplayer.trade call: ", arg0);
                        LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(intent);
//                Log.d(TAG, reply);
                    }
                });

    }


    private String getJsonMessage() {
        JSONObject message = new JSONObject();
        try {
//            message.put("Host", "login.thefxplayer.com");
            message.put("Host", Constants.WS_HOST);
            message.put("Token", user.token);
        } catch (JSONException e) {
            Log.e(TAG, "getJsonMessage: ", e);
        }
        return message.toString();
    }

    private PagingResult parsePaging(String response) {
        //    "Page":0,"TotalResults":4,"ResultsPerPage":20}]}
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray messages = jsonObject.getJSONArray("Messages");
            JSONObject message = messages.getJSONObject(0);

            int page = message.getInt("Page");
            int totalResults = message.getInt("TotalResults");
            int resultsPerPage = message.getInt("ResultsPerPage");
            Log.d(TAG, "parsePaging() called with:" +
                    " page = [" + page + "]"  +
                    " totalResults = [" + totalResults + "]"  +
                    " resultsPerPage = [" + resultsPerPage + "]"
            );
            return new PagingResult(page, totalResults, resultsPerPage, 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new PagingResult();
    }

    private synchronized Pair<Integer, List<Trade>> parseTradesUpdate(String reply) {
        Log.i(TAG, "reply top get open : " + reply);
        PagingResult pagingResult = parsePaging(reply);
        List<Trade> result = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(reply);
            JSONArray messages = jsonObject.getJSONArray("Messages");
            JSONArray trades = messages.getJSONObject(0).getJSONArray("Trades");// FIXME : server response
            Log.i(TAG, "CallSecond : trades size " + trades.length());
            for (int i = 0; i < trades.length(); i++) {
                result.add(new Trade(trades.getJSONObject(i)));
            }
            return new Pair<>(pagingResult.getTotalResults(), result);

        } catch (JSONException e) {
            Log.e(TAG, "parseTradesUpdate: ", e);
        }
        return null;
    }

    public Account getActiveAccount(){
        return accounts.get(activeAccount);
    }

}
