package com.fxvalley.exchanger.benestocks.models;

/**
 * Created by user on 13/07/2017.
 */

public class ContractSpecs {

    public Contract contract;
    public Account account;
    public Instrument instrument;

    public ContractSpecs(Contract contract, Account account, Instrument instrument){
        this.contract = contract;
        this.account = account;
        this.instrument = instrument;
    }
}
