package com.fxvalley.exchanger.benestocks.database.models;

import com.fxvalley.exchanger.benestocks.utilities.FloatAndString;

import org.json.JSONObject;
import org.w3c.dom.Document;

import java.util.Date;

/**
 * Created by aloupas on 2/5/2017.
 */

public class Instrument extends BaseModel {

    @Database(dataType = Datatype.TEXT, primaryKey = true, notNull = true)
    private String currencyPair;
    @Database(dataType = Datatype.LONG)
    private Date lastUpdate;
    @Database(dataType = Datatype.INTEGER)
    private boolean isFavorited;
    @Database(dataType = Datatype.REAL)
    private FloatAndString open;
    @Database(dataType = Datatype.REAL)
    private FloatAndString high;
    @Database(dataType = Datatype.REAL)
    private FloatAndString low;
    @Database(dataType = Datatype.REAL)
    private FloatAndString close;
    @Database(dataType = Datatype.REAL)
    private FloatAndString buyPrice;
    @Database(dataType = Datatype.REAL)
    private FloatAndString sellPrice;
    @Database(dataType = Datatype.REAL)
    private FloatAndString spread;


    @Override
    protected void loadJson(JSONObject jsonObject) {

    }

    @Override
    protected void loadXml(Document document) {

    }

    @Override
    protected void loadStringArray(String[] stringArray) {

    }

}
