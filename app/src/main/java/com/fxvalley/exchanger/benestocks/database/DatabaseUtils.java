package com.fxvalley.exchanger.benestocks.database;

import android.util.Log;

import com.fxvalley.exchanger.benestocks.database.models.BaseModel;
import com.fxvalley.exchanger.benestocks.database.models.Database;

import java.lang.reflect.Field;

public class DatabaseUtils {

    public static <T extends BaseModel> String createTable(Class<T> t) {

        String createTable = "CREATE TABLE %s IF NOT EXISTS (%s);";
        String comma = "";
        String columns = "";

        for (Field field : t.getDeclaredFields()) {

            if (field.isAnnotationPresent(Database.class)) {
                Database annotation = field.getAnnotation(Database.class);

                columns += comma + field.getName() + " " + annotation.dataType().value + (annotation.primaryKey() ? " PRIMARY_KEY" : "") + (annotation.notNull() ? " NOT NULL" : "");
                comma = ", ";
            }
        }

        Log.d("DatabaseUtils", t.getSimpleName());

        return String.format(createTable, t.getSimpleName(), columns);
    }
}
