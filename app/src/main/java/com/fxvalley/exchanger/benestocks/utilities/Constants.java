package com.fxvalley.exchanger.benestocks.utilities;

import android.graphics.Bitmap;

/**
 * Created by aloupas on 2/4/2017.
 */

public class Constants {

//    //    quanto
//    public static final String WEB_SOCKET_API_URL= "wss://platform.quantomarkets.com";
//    public static final String REST_API_URL = "https://api.quantomarkets.com/";
//
//    //    Price Session Socket host name
//    public static final String WS_HOST = "login.quantomarkets.com";

////      fx player
//    public static final String WEB_SOCKET_API_URL= "ws://13.69.11.80";
//    public static final String REST_API_URL = "http://api.forexvalleytech.com/";
//
////    fx hostname
//    public static final String WS_HOST = "login.thefxplayer.com";

    //    bene stock
    public static final String WEB_SOCKET_API_URL= "ws://23.101.4.254";
    public static final String REST_API_URL = "http://api.benestocks.net/";

    //    bene stocs host name
    public static final String WS_HOST = "login.benestocks.net";

//    // Markets Unlimited
//    public static final String WEB_SOCKET_API_URL= "ws://13.94.149.254";
//    public static final String REST_API_URL = "http://api.markets-unlimited.com/";

    // LOCAL LYNCH
//    public static final String WEB_SOCKET_API_URL= "ws://192.168.10.100";
//    public static final String REST_API_URL = "http://192.168.10.100/";

//    public static final String REST_API_URL = "http://bapi.fxvalley.tech/";
//    public static final String WEB_SOCKET_API_URL= "ws://13.81.109.240";

    public static final String STATUS_VALUES = "status_values";
    public static final String UPDATE_STATUS_FILTER = "update_status_filter";
    public static final String APP_PREFERENCES = "app_preferences";

    public static final String TICK_UPDATED = "tick_updated";
    public static final String GROUP_ID = "group_id;";
    public static final String SYMBOL_ID = "symbol_id;";
    public static final String INSTRUMENT = "instrument";
    public static final String CATEGORY_CHANGED = "category_changed";
    public static final String BUY_SELL_ASTATE = "buy_sell_astate";
    public static final String SELECTION = "selection";
    public static final String CHART_UPDATED = "chart_updated";
    public static final String EVENTS_URL = "https://cdn-nfs.forexfactory.net/ff_calendar_thisweek.xml";
    public static final String LAST_PRICE_FETCHED = "last_price_fetched";
    public static final String TRADE = "trade";
    public static final String USER_FILTER = "user_filter";
    public static final String POSITIONS_SPINNER_SELECTION = "positions_spinner_selection";
    public static final String TRADE_RESPONDED = "trade_responded";
    public static final String TRADE_RESPONSE = "trade_response";
    public static final String CALCULATED_PL = "calculated_pl";
    public static final String ACCOUNT = "account";
    public static final String OPEN_TRADES = "OPEN_TRADES";
    public static final String ACCOUNT_SNAPSHOT = "ACCOUNT_SNAPSHOT";
    public static final String LIVE_DATA = "LIVE_DATA";
    public static final String CHANGE_CHART = "CHANGE_CHART";
    public static final String SELECTION_LEFT = "selection_left";

    protected static String CAPITAL = "capital";
    protected static String MARGIN = "margin";
    protected static String PROFIT_LOSS = "profitLoss";
    protected static String STOP_OUT = "stopOut";

    public static Bitmap bitmap;
}
