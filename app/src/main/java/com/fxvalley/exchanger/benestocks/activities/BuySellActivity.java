package com.fxvalley.exchanger.benestocks.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.fxvalley.exchanger.benestocks.R;
import com.fxvalley.exchanger.benestocks.charts.ChartUtilities;
import com.fxvalley.exchanger.benestocks.charts.TimeValueFormatter;
import com.fxvalley.exchanger.benestocks.models.Account;
import com.fxvalley.exchanger.benestocks.models.ChartData;
import com.fxvalley.exchanger.benestocks.models.ContractSpecs;
import com.fxvalley.exchanger.benestocks.models.Instrument;
import com.fxvalley.exchanger.benestocks.models.User;
import com.fxvalley.exchanger.benestocks.sockets.ChartSocket;
import com.fxvalley.exchanger.benestocks.sockets.ChartSocketListener;
import com.fxvalley.exchanger.benestocks.sockets.PriceSessionSocket;
import com.fxvalley.exchanger.benestocks.sockets.PriceSocketListener;
import com.fxvalley.exchanger.benestocks.sockets.SocketUtilities;
import com.fxvalley.exchanger.benestocks.sockets.TradeSessionSocket;
import com.fxvalley.exchanger.benestocks.sockets.TradeSocketsConnectionListener;
import com.fxvalley.exchanger.benestocks.utilities.AccountSnapshot;
import com.fxvalley.exchanger.benestocks.utilities.ChartSelector;
import com.fxvalley.exchanger.benestocks.utilities.Constants;
import com.fxvalley.exchanger.benestocks.utilities.CustomSpinner;
import com.fxvalley.exchanger.benestocks.utilities.EditPositionDataTextWatcher;
import com.fxvalley.exchanger.benestocks.utilities.ToastView;
import com.fxvalley.exchanger.benestocks.utilities.TopHederViewAdapter;
import com.fxvalley.exchanger.benestocks.utilities.validation.BuyValidation;
import com.fxvalley.exchanger.benestocks.utilities.validation.EditPositionTextValidator;
import com.fxvalley.exchanger.benestocks.utilities.validation.SellValidation;
import com.github.mikephil.charting.charts.CandleStickChart;
import com.github.mikephil.charting.components.LabelPosition;
import com.github.mikephil.charting.components.Line;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.CandleData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import static com.fxvalley.exchanger.benestocks.activities.BuySellActivity.ActivityState.BUY;
import static com.fxvalley.exchanger.benestocks.activities.BuySellActivity.ActivityState.NONE;
import static com.fxvalley.exchanger.benestocks.activities.BuySellActivity.ActivityState.SELL;
import static com.fxvalley.exchanger.benestocks.charts.ChartUtilities.convertMilliSToFormattedDate;
import static com.fxvalley.exchanger.benestocks.utilities.ChartSelector.ChartSelectorState.EIGHT_HOURS;
import static com.fxvalley.exchanger.benestocks.utilities.ChartSelector.ChartSelectorState.FIFTEEN_MIN;
import static com.fxvalley.exchanger.benestocks.utilities.ChartSelector.ChartSelectorState.ONE_DAY;
import static com.fxvalley.exchanger.benestocks.utilities.ChartSelector.ChartSelectorState.ONE_HOUR;
import static com.fxvalley.exchanger.benestocks.utilities.ChartSelector.ChartSelectorState.THIRTY_MINS;

public class BuySellActivity extends AppCompatActivity implements PriceSocketListener, ChartSocketListener, TradeSocketsConnectionListener {

    private static final String TAG = "BuySellActivity";
    private static final int MARKET = 0;
    private static final int LIMIT = 1;
    private static final int STOP = 2;
    public static final int DAY = 24;
    private TextView menuView;

    private PriceSessionSocket priceSocket;
    private TradeSessionSocket tradeSocket;
    private ChartSocket chartSocket;
    public AccountSnapshot accountSnapshot = new AccountSnapshot();

    Account account;
    private YAxis rightAxis;
    private YAxis leftAxis;
    private EditPositionTextValidator editPositionTextChecker;
    private String rowMarketLeftChooserText;

    public enum ActivityState {BUY, SELL, NONE}

    private static final String[] MARKET_CHOICES = new String[]{"Market", "Limit", "Stop"};

    private User user;
    private int selection = MARKET;
    public boolean spinnerIsOpen;
    private ActivityState activityState;
    protected String[] mMonths = new String[]{
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"
    };
    private RecyclerView topHeaderRecyclerView;
    private TopHederViewAdapter topheaderAdapter;
    private CandleStickChart mChart;
    private final int itemcount = 12;

    private EditText marketPriceEditText;
    private EditText stopProfitEditText;
    private EditText stopLostEditText;
    private EditText strategyEditText;
    private TextView marketPriceLabel;

    private TextView marketRowSellValueLastDigit;
    private TextView marketRowBuyValueLastDigit;
    private View marketRowButtonSell;
    private View marketRowButtonBuy;
    private TextView currencyPair;
    private TextView updatedAt;
    private TextView marketRowOpen;
    private TextView marketRowHigh;
    private TextView marketRowLow;
    private TextView marketRowClose;

    private TextView marketRowSellValue;
    private TextView marketRowSellValueTwoDigits;

    private TextView marketRowBuyValue;
    private TextView marketRowBuyValueTwoDigits;

    private TextView marketRowSpread;

    private TextView rowMarketLeftchooser;
    private CustomSpinner rowMarketRightSpinner;

    private Instrument instrument;
    private DecimalFormat decimalFormat = new DecimalFormat("0.00");
    private View progressView;
    private TextView progresTextView;

    private ChartSelector chartSelector = new ChartSelector();

    private BroadcastReceiver tradeRespondedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getBooleanExtra(Constants.TRADE_RESPONSE, false)) {

                progressView.setVisibility(View.GONE);

                Toast.makeText(context, R.string.trade_succefull, Toast.LENGTH_LONG).show();
                BuySellActivity.super.onBackPressed();

//                new MaterialDialog.Builder(BuySellActivity.this)
//                        .title(R.string.success)
//                        .content(R.string.trade_succefull)
//                        .positiveText(R.string.ok)
//                        .onPositive(new MaterialDialog.SingleButtonCallback() {
//                            @Override
//                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                BuySellActivity.super.onBackPressed();
//                            }
//                        })
//                        .show();

            } else {
                progressView.setVisibility(View.GONE);
                Toast.makeText(BuySellActivity.this, R.string.request_failed, Toast.LENGTH_SHORT).show();
            }
        }
    };

    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (handler == null) {
            handler = new Handler();
        }

        setContentView(R.layout.activity_buy_sell);


        rowMarketLeftchooser = (TextView) findViewById(R.id.rowMarketLeftChooser);
        rowMarketLeftchooser.setVisibility(View.VISIBLE);

        if (savedInstanceState == null) {
            activityState = (ActivityState) getIntent().getSerializableExtra(Constants.BUY_SELL_ASTATE);
            instrument = (Instrument) getIntent().getSerializableExtra(Constants.INSTRUMENT);
            account = (Account) getIntent().getSerializableExtra(Constants.ACCOUNT);
            selection = MARKET;
        } else {
            activityState = (ActivityState) savedInstanceState.getSerializable(Constants.BUY_SELL_ASTATE);
            instrument = (Instrument) savedInstanceState.getSerializable(Constants.INSTRUMENT);
            selection = savedInstanceState.getInt(Constants.SELECTION);
            account = (Account) savedInstanceState.getSerializable(Constants.ACCOUNT);
            rowMarketLeftchooser.setText(savedInstanceState.getString(Constants.SELECTION_LEFT, rowMarketLeftChooserText));
        }

        initChartData();

        chartSelector.onCreate(this, savedInstanceState);
        marketRowSellValueLastDigit = (TextView) findViewById(R.id.marketRowSellValueLastDigit);
        marketRowBuyValueLastDigit = (TextView) findViewById(R.id.marketRowBuyValueLastDigit);
        updatedAt = (TextView) findViewById(R.id.marketUpdatedAt);
        marketRowOpen = (TextView) findViewById(R.id.marketRowOpen);
        marketRowHigh = (TextView) findViewById(R.id.marketRowHigh);
        marketRowLow = (TextView) findViewById(R.id.marketRowLow);
        marketRowClose = (TextView) findViewById(R.id.marketRowClose);
        marketRowSellValue = (TextView) findViewById(R.id.marketRowSellValue);
        marketRowSellValueTwoDigits = (TextView) findViewById(R.id.marketRowSellValueTwoDigits);
        marketRowBuyValue = (TextView) findViewById(R.id.marketRowBuyValue);
        marketRowBuyValueTwoDigits = (TextView) findViewById(R.id.marketRowBuyValueTwoDigits);
        marketRowSpread = (TextView) findViewById(R.id.marketRowSpread);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            setupToolbar();
            initInputViews();
            initStatusRecycler();
        }

        initSpinner();

        currencyPair = (TextView) findViewById(R.id.marketCurrencyPair);

        progressView = findViewById(R.id.progressView);
        progresTextView = (TextView) findViewById(R.id.progressTextView);


        marketRowButtonBuy = findViewById(R.id.marketRowButtonBuy);
        marketRowButtonSell = findViewById(R.id.marketRowButtonSell);


        changeButtonsColor();

        marketRowButtonBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (activityState != BUY) {
                    activityState = BUY;
                    changeButtonsColor();
                } else {
                    showTradeConfirmDialog(activityState);
                }
            }
        });
//        uzair
        marketRowButtonSell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (activityState != SELL) {
                    activityState = SELL;
                    changeButtonsColor();
                } else {
                    showTradeConfirmDialog(activityState);
                }
            }
        });

        loadInstrumentValues(instrument);

//        initChartData();
    }

    private void showTradeConfirmDialog(ActivityState activityState) {
        new MaterialDialog.Builder(BuySellActivity.this)
                .title(R.string.trade_confirmation)
                .content("Do you want to " + getStateString(activityState) + " " + instrument.name + " order for " +rowMarketLeftchooser.getText().toString() + "?")
                .onPositive((dialog, which) -> {
                        onTradeSendConfirmed();
                        dialog.dismiss();
                })
                .positiveText(R.string.confirm)
                .onNegative((dialog, which) -> Toast.makeText(BuySellActivity.this, "Cancelled", Toast.LENGTH_SHORT))
                .negativeText(R.string.cancel)
                .show();
    }

    private void onTradeSendConfirmed() {
        sendBuySellRequest();
    }

    private String getStateString(ActivityState activityState) {
        if (activityState == SELL) {
            return getResources().getString(R.string.sell);
        } else if (this.activityState == BUY) {
            return getResources().getString(R.string.buy);
        }
        return null;
    }

    private void initSpinner() {
        rowMarketRightSpinner = (CustomSpinner) findViewById(R.id.rowMarketRightSpinner);
        rowMarketRightSpinner.setVisibility(View.VISIBLE);
//        View spinnerBackgroundView = findViewById(R.id.spinnerBackgroundView);
//        spinnerBackgroundView.setVisibility(View.GONE);

        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<>(this, R.layout.transparent_spinner_blac_text, MARKET_CHOICES);
        stringArrayAdapter.setDropDownViewResource(R.layout.drop_down_category);
        rowMarketRightSpinner.setAdapter(stringArrayAdapter);
        rowMarketRightSpinner.setSelection(selection);


        rowMarketRightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selection = i;
                configureSelection();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        rowMarketRightSpinner.setSpinnerEventsListener(new CustomSpinner.OnSpinnerEventsListener() {
            @Override
            public void onSpinnerOpened(Spinner spinner) {
                spinnerIsOpen = true;
                Log.d(TAG, "ON SPINNER OPEN");
            }

            @Override
            public void onSpinnerClosed(Spinner spinner) {
                spinnerIsOpen = false;
                Log.d(TAG, "ON SPINNER CLOSED");
            }
        });
    }

    private void initInputViews() {

        marketPriceEditText = (EditText) findViewById(R.id.marketPriceEditPanel);
        marketPriceEditText.setEnabled(false);
        marketPriceEditText.setFocusableInTouchMode(false);
        marketPriceLabel = (TextView) findViewById(R.id.marketPriceLabel);

        stopProfitEditText = (EditText) findViewById(R.id.stopProfitEdiText);
        stopLostEditText = (EditText) findViewById(R.id.stopLostEdiText);
        strategyEditText = (EditText) findViewById(R.id.strategyEdiText);
        configureSelection();

    }

    private void initStatusRecycler() {
        topHeaderRecyclerView = (RecyclerView) findViewById(R.id.status_recyclerview);
        topHeaderRecyclerView.setLayoutManager(new GridLayoutManager(BuySellActivity.this,2));
        topHeaderRecyclerView.setAdapter(topheaderAdapter = new TopHederViewAdapter(this));
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.buySellToolbar);
        toolbar.setTitle(R.string.market);
        toolbar.inflateMenu(R.menu.buy_sell_menu);

        menuView = (TextView) toolbar.findViewById(R.id.save);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
//        uzair
        toolbar.setOnMenuItemClickListener(item -> {
//            sendBuySellRequest();
            showTradeConfirmDialog(activityState);
            return true;
        });
    }

    private void sendBuySellRequest() {
        progresTextView.setText(R.string.sending_trade);
        progressView.setVisibility(View.VISIBLE);

        tradeSocket.sendTrade(createTrade());
    }

    private void configureSelection() {
        if (marketPriceLabel != null && marketPriceEditText != null) {

            switch (selection) {
                case MARKET:
                    marketPriceLabel.setText(R.string.marketPrice);
                    marketPriceEditText.setEnabled(false);
                    marketPriceEditText.setFocusableInTouchMode(false);
                    break;
                case LIMIT:
                    marketPriceLabel.setText(R.string.limitPrice);
                    marketPriceEditText.setEnabled(true);
                    marketPriceEditText.setFocusableInTouchMode(true);
                    break;
                case STOP:
                    marketPriceLabel.setText(R.string.stop_price);
                    marketPriceEditText.setEnabled(true);
                    marketPriceEditText.setFocusableInTouchMode(true);
                    break;

            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(Constants.SELECTION, selection);
        outState.putString(Constants.SELECTION_LEFT, rowMarketLeftChooserText);
        outState.putSerializable(Constants.BUY_SELL_ASTATE, activityState);
        outState.putSerializable(Constants.INSTRUMENT, instrument);
        outState.putSerializable(Constants.ACCOUNT, account);
        chartSelector.onSavedInstanceState(outState);
    }

    private void changeButtonsColor() {
        initTextValidation();

        if (activityState == BUY) {
            marketRowButtonBuy.setBackgroundResource(R.drawable.market_button_blue_background);
            marketRowButtonSell.setBackgroundResource(R.drawable.market_button_grey_background);
            marketRowButtonBuy.setAlpha(1f);
            marketRowButtonSell.setAlpha(0.7f);
            if (marketPriceEditText != null) {
                if (instrument.instrumentLiveData != null) {
                    Double ask = instrument.instrumentLiveData.ask;
                    marketPriceEditText.setText(String.valueOf(ask));
                }
            }
            if (menuView != null) {
                menuView.setText(R.string.buy);
            }

            if (editPositionTextChecker != null) {
                editPositionTextChecker.check();
            }
        } else if (activityState == SELL) {
            marketRowButtonBuy.setBackgroundResource(R.drawable.market_button_grey_background);
            marketRowButtonSell.setBackgroundResource(R.drawable.market_button_blue_background);
            marketRowButtonBuy.setAlpha(0.7f);
            marketRowButtonSell.setAlpha(1f);
            if (marketPriceEditText != null) {
                if (instrument.instrumentLiveData != null) {
                    marketPriceEditText.setText(String.valueOf(instrument.instrumentLiveData.bid));
                }
            }
            if (menuView != null) {
                menuView.setText(R.string.sell);
            }
            if (editPositionTextChecker != null) {
                editPositionTextChecker.check();
            }
        } else {

            marketRowButtonBuy.setBackgroundResource(R.drawable.market_button_grey_background);
            marketRowButtonSell.setBackgroundResource(R.drawable.market_button_grey_background);
            marketRowButtonBuy.setAlpha(1f);
            marketRowButtonSell.setAlpha(1f);
            if (marketPriceEditText != null) {
                if (instrument.instrumentLiveData != null) {
                    marketPriceEditText.setText(String.valueOf(instrument.instrumentLiveData.bid));
                }
            }
            if (menuView != null) {
                menuView.setText(R.string.select_mode);
                menuView.setEnabled(false);
            }
        }
    }

    private void initTextValidation() {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            editPositionTextChecker = new EditPositionTextValidator(
                    activityState == BUY ? new BuyValidation() : new SellValidation(),
                    marketPriceEditText,
                    stopProfitEditText,
                    stopLostEditText,
                    strategyEditText, (isValid) -> {
                if (activityState != NONE) {
                    menuView.setEnabled(isValid);
                }
            });


            stopProfitEditText.addTextChangedListener(new EditPositionDataTextWatcher(editPositionTextChecker));
            stopLostEditText.addTextChangedListener(new EditPositionDataTextWatcher(editPositionTextChecker));
            strategyEditText.addTextChangedListener(new EditPositionDataTextWatcher(editPositionTextChecker));
        }
    }

    private void loadInstrumentValues(Instrument instrument) {

        if (instrument == null || instrument.instrumentLiveData == null)
            return;

        if (currencyPair != null) {
            currencyPair.setText(instrument.name);
        }

        if (updatedAt != null) {
            updatedAt.setText("U:" + instrument.instrumentLiveData.dT);
        }
        if (marketRowOpen != null) {
            marketRowOpen.setText("O:" + String.valueOf(instrument.instrumentLiveData.opn));
        }
        if (marketRowHigh != null) {
            marketRowHigh.setText("H:" + String.valueOf(instrument.instrumentLiveData.high));
        }
        if (marketRowLow != null) {
            marketRowLow.setText("L:" + String.valueOf(instrument.instrumentLiveData.low));
        }

        if (marketRowClose != null) {
            try {
                marketRowClose.setText(String.valueOf("C:" + decimalFormat.format(instrument.instrumentLiveData.chP) + "%"));
                if (instrument.instrumentLiveData.chP > 0) {
                    marketRowClose.setTextColor(getResources().getColor(R.color.button_green));
                } else if (instrument.instrumentLiveData.chP < 0) {
                    marketRowClose.setTextColor(getResources().getColor(R.color.button_red));
                } else {
                    marketRowClose.setTextColor(Color.WHITE);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                marketRowClose.setText(String.valueOf("C:" + "-"));
                marketRowClose.setTextColor(Color.WHITE);
            }
        }

        if (marketRowSellValue != null) {
            marketRowSellValue.setText(String.valueOf(instrument.instrumentLiveData.bidMain));
        }
        if (marketRowSellValueTwoDigits != null) {
            marketRowSellValueTwoDigits.setText(String.valueOf(instrument.instrumentLiveData.bidPip));
        }
        if (marketRowSellValueLastDigit != null) {
            marketRowSellValueLastDigit.setText(String.valueOf(instrument.instrumentLiveData.bidPoint));
        }

        if (marketRowBuyValue != null) {
            marketRowBuyValue.setText(String.valueOf(instrument.instrumentLiveData.askMain));
        }
        if (marketRowBuyValueTwoDigits != null) {
            marketRowBuyValueTwoDigits.setText(String.valueOf(instrument.instrumentLiveData.askPip));
        }
        if (marketRowBuyValueLastDigit != null) {
            marketRowBuyValueLastDigit.setText(String.valueOf(instrument.instrumentLiveData.askPoint));
        }
        if (marketRowSpread != null) {
            marketRowSpread.setText(String.valueOf(instrument.instrumentLiveData.sIP));
        }

        if (activityState == BUY) {


            if (marketPriceEditText != null) {
                marketPriceEditText.setText(String.valueOf(instrument.instrumentLiveData.ask));
            }

            if (marketRowButtonBuy != null) {
                if (instrument.instrumentLiveData.dPA > 0 && activityState == BUY) {
                    marketRowButtonBuy.setBackgroundResource(R.drawable.market_button_green_background);
                } else if (instrument.instrumentLiveData.dPA < 0) {
                    marketRowButtonBuy.setBackgroundResource(R.drawable.market_button_red_background);
                } else {
                    marketRowButtonBuy.setBackgroundResource(R.drawable.market_button_blue_background);
                }
            }
        }

        if (activityState == SELL) {

            if (marketPriceEditText != null) {
                marketPriceEditText.setText(String.valueOf(instrument.instrumentLiveData.bid));
            }

            if (marketRowButtonSell != null) {
                if (instrument.instrumentLiveData.dPB > 0) {
                    marketRowButtonSell.setBackgroundResource(R.drawable.market_button_green_background);
                } else if (instrument.instrumentLiveData.dPB < 0) {
                    marketRowButtonSell.setBackgroundResource(R.drawable.market_button_red_background);
                } else {
                    marketRowButtonSell.setBackgroundResource(R.drawable.market_button_blue_background);
                }
            }
        }

    }

    private void showValueAletDialog(TextView textView, AppCompatCheckBox checkbox) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(checkbox.getText().toString());
        builder.setView(R.layout.alert_edittext);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                AlertDialog alertDialog = (AlertDialog) dialogInterface;
                EditText editText = (EditText) alertDialog.findViewById(R.id.alert_edittext);
                textView.setText(editText.getText().toString());
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                checkbox.setChecked(false);
            }
        });
        builder.create().show();

    }


    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this.getApplicationContext()).registerReceiver(totalOpenReceived, new IntentFilter(Constants.UPDATE_STATUS_FILTER));
        LocalBroadcastManager.getInstance(this.getApplicationContext()).registerReceiver(chartsUpdatedReceiver, new IntentFilter(Constants.CHART_UPDATED));
        LocalBroadcastManager.getInstance(this.getApplicationContext()).registerReceiver(tradeRespondedReceiver, new IntentFilter(Constants.TRADE_RESPONDED));
        LocalBroadcastManager.getInstance(this.getApplicationContext()).registerReceiver(changeChartReceiver, new IntentFilter(Constants.CHANGE_CHART));


        user = User.getUser(this);

        if (user == null) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            return;
        }

        priceSocket = new PriceSessionSocket(user);
        chartSocket = new ChartSocket(user);
        tradeSocket = new TradeSessionSocket(user, new ToastView(this, new Handler()));

        accountSnapshot.onResume(this, tradeSocket, priceSocket, topheaderAdapter);

        priceSocket.connect(this);
        chartSocket.connect(this);
        tradeSocket.connect(this);

        handler.post(updateHeadersRunnable);

        LocalBroadcastManager.getInstance(this).registerReceiver(totalOpenReceived, new IntentFilter(Constants.CALCULATED_PL));
        if (editPositionTextChecker != null) {
            editPositionTextChecker.check();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        accountSnapshot.onPause();

        if (priceSocket != null)
            priceSocket.disconnect();

        if (tradeSocket != null)
            tradeSocket.disconnect();

        if (chartSocket != null)
            chartSocket.disconnect();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(totalOpenReceived);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(ticksUpdatedReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(chartsUpdatedReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(tradeRespondedReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(changeChartReceiver);

        if (handler != null)
            handler.removeCallbacksAndMessages(null);


    }

    private synchronized void initChartData() {

        mChart = (CandleStickChart) findViewById(R.id.buySellChart);
        mChart.getDescription().setEnabled(false);
        mChart.setBackgroundColor(Color.TRANSPARENT);
        mChart.setDrawGridBackground(false);

        mChart.setPinchZoom(false);
        mChart.setTouchEnabled(true);
        mChart.setDragEnabled(true);
        mChart.enableScroll();

        rightAxis = mChart.getAxisRight();
        rightAxis.setEnabled(false);

        leftAxis = mChart.getAxisLeft();
        leftAxis.setTextColor(Color.WHITE);
//        leftAxis.setEnabled(false);
        leftAxis.setLabelCount(7, false);
        leftAxis.setDrawGridLines(false);
        leftAxis.setDrawAxisLine(false);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setLabelCount(6);
        xAxis.setTextColor(Color.WHITE);
        mChart.setAutoScaleMinMaxEnabled(true);
        mChart.getLegend().setEnabled(false);
        mChart.getViewPortHandler().setOnZoomOutXMaxReachedCallback(() -> chartSelector.sendChartBroadcast(true));
    }

    public synchronized void setData(List<com.fxvalley.exchanger.benestocks.models.ChartData> chartData) {
        mChart.clear();
        CandleData data = ChartUtilities.get().generateCandleData(chartData);

        if (data != null && data.getEntryCount() > 0)
            mChart.setData(data);

        mChart.getXAxis().setValueFormatter(new TimeValueFormatter(chartData, mChart, chartSelector.chartSelectorState));
        if (chartData.size() > 0) {
            ChartData lastItem = chartData.get(chartData.size() - 1);
            float value = lastItem.close.floatValue();
            addHorizontalIndicator(value, lastItem.open < lastItem.close ? Color.GREEN : Color.RED);
        }
    }

    private void addHorizontalIndicator(float value, int color) {
        Line ll1 = new Line(value, String.valueOf(value));
        ll1.setLineWidth(1f);
        ll1.enableDashedLine(10f, 10f, 0f);
        ll1.setLabelPosition(LabelPosition.RIGHT_TOP);
        ll1.setTextSize(12f);
        ll1.setLineColor(color);
        ll1.setTextColor(color);
        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.removeAllLines(); // reset all limit lines to avoid overlapping lines
        leftAxis.addLine(ll1);
    }


    @Override
    public void onBackPressed() {
        BuySellActivity.super.onBackPressed();
//        new MaterialDialog.Builder(this)
//                .title(R.string.warning)
//                .content(R.string.are_you_sure_discard_changes)
//                .positiveText(R.string.discard_changes)
//                .onPositive(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                        BuySellActivity.super.onBackPressed();
//                    }
//                })
//
//                .negativeText(R.string.cancel)
//                .show();


    }


    private BroadcastReceiver ticksUpdatedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            int group_id = intent.getIntExtra(Constants.GROUP_ID, -1);
            int symbol_id = intent.getIntExtra(Constants.SYMBOL_ID, -1);


            if (symbol_id == BuySellActivity.this.instrument.id) {

                Instrument instrument = priceSocket.instrumentGroups.get(priceSocket.groupId).instruments.get(symbol_id);
                if (instrument != null) {
                    if ( instrument.instrumentLiveData != null) {
                        BuySellActivity.this.instrument.instrumentLiveData = instrument.instrumentLiveData;
                    }
                }
//                loadInstrumentValues(instrument);

            }
        }
    };

    @Override
    public void onPriceSocketStateChanged(SocketUtilities.ClientStatus clientStatus) {

    }

    @Override
    public void onCategoriesFecthed() {

        LocalBroadcastManager.getInstance(this).unregisterReceiver(ticksUpdatedReceiver);
        LocalBroadcastManager.getInstance(this.getApplicationContext()).registerReceiver(ticksUpdatedReceiver, new IntentFilter(Constants.TICK_UPDATED));

        handler.removeCallbacksAndMessages(null);
        handler.post(getRunnable());

        priceSocket.subscribeToInstrument(instrument);
        tradeSocket.getContract(instrument.name);

        accountSnapshot.onPriceSessionSocketReady();

    }

    @Override
    public void onLastPriceFetched() {

    }

    @Override
    public void onChartStateChanged(SocketUtilities.ClientStatus clientStatus) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (clientStatus) {
                    case CONNECTED:
                        chartSelector.onChartSocketReady();
                        break;
                    case DISCONNECTED:
                        chartSelector.onChartSocketDisconnect();
                        break;
                    case CONNECTING:
                        break;
                }
            }
        });
    }

    public Runnable getRunnable() {
        if (runnable == null) {
            runnable = new Runnable() {
                @Override
                public void run() {

                    loadInstrumentValues(instrument);

                    handler.postDelayed(this, 1000);
                }
            };

        }
        return runnable;
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {

            loadInstrumentValues(instrument);

            handler.postDelayed(this, 1000);
        }
    };

    private BroadcastReceiver chartsUpdatedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (chartSocket != null &&
                    chartSocket.clientStatus == SocketUtilities.ClientStatus.CONNECTED) {
                List<ChartData> chartDatas = new ArrayList<>(chartSocket.chartDatas);
                if (chartDatas.size() > 0) {
                    setData(chartDatas);
                }
            }
        }
    };

    public JSONObject createTrade() {

        JSONObject msg = new JSONObject();
        JSONObject order = new JSONObject();
        JSONArray messages = new JSONArray();

        JSONObject instrumentjson = new JSONObject();
        try {
            instrumentjson.put("Name", instrument.name);
            order.put("Instrument", instrumentjson);
            if (marketPriceEditText!= null) {
                order.put("RP", Double.valueOf(marketPriceEditText.getText().toString()));
            }
            order.put("VU", Double.valueOf(rowMarketLeftchooser.getText().toString()));
            order.put("TT", MARKET_CHOICES[rowMarketRightSpinner.getSelectedItemPosition()]);
            order.put("TIF", "GoodTillCancel");
            order.put("S", activityState == BUY ? 1 : 2);
//            order.put("RDT", null);
            order.put("ES", 4);
            if (stopProfitEditText != null) {
                order.put("SP ", stopProfitEditText.getText().toString().isEmpty() ? 0 : Double.valueOf(stopProfitEditText.getText().toString()));
            }
            if (stopLostEditText != null) {
                order.put("SL", stopLostEditText.getText().toString().isEmpty() ? 0 : Double.valueOf(stopLostEditText.getText().toString()));
            }
            order.put("UM", 1);
            if (strategyEditText != null) {
                order.put("LBL", strategyEditText.getText().toString());
            }
            order.put("MId", 7);
//            order.put("PO", null);
//            order.put("EX", null);

            messages.put(order);
            msg.put("Messages", messages);
            msg.put("MessageType", 20);


        } catch (JSONException e) {
            e.printStackTrace();
        }


        return msg;

    }

    @Override
    public void onTradeSocketStateChanged(SocketUtilities.ClientStatus clientStatus) {

        if (clientStatus == SocketUtilities.ClientStatus.CONNECTED) {
            tradeSocket.getSession();
        }

    }

    @Override
    public void onSessionFetched() {
        tradeSocket.getContract(instrument.name);
        tradeSocket.getAccount();
    }

    @Override
    public void onGetAccount() {

        accountSnapshot.onTradeSessionSocketReady();

    }

    @Override
    public void onAccountSwitched(boolean success) {

    }

    @Override
    public void onContractFetched(ContractSpecs contractSpecs) {

        Log.d(TAG, "Contract Fetched");

        handler.post(new Runnable() {

            double minValue = -1;
            double step = -1;
            double maxValue = -1;

            @Override
            public void run() {
                try {

                    if (contractSpecs.account.TAT == 5 || contractSpecs.account.TAT == 6 || contractSpecs.account.TAT == 7) {

                        minValue = contractSpecs.account.TALD.Min;
                        step = contractSpecs.account.TALD.Increment;
                        maxValue = contractSpecs.account.TALD.Max;

                    } else {

                        if (contractSpecs.instrument.product == 4) {
                            minValue = contractSpecs.account.TALD.Min * contractSpecs.contract.minimum;
                            step = contractSpecs.account.TALD.Increment * contractSpecs.contract.minimum;
                            maxValue = contractSpecs.account.TALD.Max * contractSpecs.contract.minimum;
                        } else {
                            minValue = contractSpecs.account.TALD.Min;
                            step = contractSpecs.account.TALD.Increment;
                            maxValue = contractSpecs.account.TALD.Max;
                        }
                    }

                    if (minValue == -1 || step == -1 || maxValue == -1)
                        return;

                    int sizeOfArray = (int) (1 + (maxValue - minValue) / step);

                    String[] values = new String[sizeOfArray];

                    DecimalFormat decimalFormat = new DecimalFormat("##.##");
                    for (int i = 0; i < sizeOfArray; i++) {
                        values[i] = decimalFormat.format(minValue + i * step);
                    }

                    rowMarketLeftChooserText = values[0];
                    rowMarketLeftchooser.setText(rowMarketLeftChooserText);

                    rowMarketLeftchooser.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {


                            String s = rowMarketLeftchooser.getText().toString();
                            double value = Double.valueOf(s);

                            int index = (int) ((value - minValue) / step);

                            MaterialDialog show = new MaterialDialog.Builder(view.getContext())
                                    .customView(R.layout.number_picker, false)
                                    .title(R.string.choose_type).dismissListener(new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialogInterface) {
                                            MaterialDialog dialog = (MaterialDialog) dialogInterface;
                                            NumberPicker numberPicker = (NumberPicker) dialog.findViewById(R.id.number_edittext);
                                            rowMarketLeftchooser.setText(values[numberPicker.getValue()]);
                                        }
                                    })
                                    .show();

                            NumberPicker numberPicker = (NumberPicker) show.findViewById(R.id.number_edittext);
                            numberPicker.setDisplayedValues(values);
                            numberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                            numberPicker.setMinValue(0);
                            numberPicker.setMaxValue(sizeOfArray - 1);
                            numberPicker.setValue(index);
                        }
                    });


                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });

    }

    private BroadcastReceiver totalOpenReceived = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            int selectedAccount = 0;
            for (int i = 0; i < tradeSocket.accounts.size(); i++) {
                if (tradeSocket.accounts.get(i).isActive) {
                    selectedAccount = i;
                    break;
                }
            }

            topheaderAdapter.updateTopHeaderValues(tradeSocket.accounts.get(selectedAccount), intent.getDoubleExtra(Constants.CALCULATED_PL, 0.0));
        }
    };

    private Runnable updateHeadersRunnable = new Runnable() {

        @Override
        public void run() {

            if (tradeSocket != null && tradeSocket.accountFetched) {

                tradeSocket.getOpen(1, 0, 20).subscribe((result) -> {
                }, (err) -> {
                    Log.e(TAG, "onTradeSessionSocketReady: getOpen ", err);
                });
            }
            handler.postDelayed(this, 1000);

        }
    };

    private BroadcastReceiver changeChartReceiver = new BroadcastReceiver() {

        Map<Integer, Integer> hoursToSubstract = new ConcurrentHashMap<>();
        Map<Integer, Integer> hoursToLoadEveryZoomout = new ConcurrentHashMap<>();

        {
            hoursToSubstract.put(FIFTEEN_MIN.ordinal(), DAY * 5);
            hoursToSubstract.put(THIRTY_MINS.ordinal(), DAY * 10);
            hoursToSubstract.put(ONE_HOUR.ordinal(), DAY * 10);
            hoursToSubstract.put(EIGHT_HOURS.ordinal(), DAY * 30);
            hoursToSubstract.put(ONE_DAY.ordinal(), DAY * 60);

            hoursToLoadEveryZoomout.put(FIFTEEN_MIN.ordinal(), DAY);
            hoursToLoadEveryZoomout.put(THIRTY_MINS.ordinal(), DAY * 10);
            hoursToLoadEveryZoomout.put(ONE_HOUR.ordinal(), DAY * 10);
            hoursToLoadEveryZoomout.put(EIGHT_HOURS.ordinal(), DAY * 30);
            hoursToLoadEveryZoomout.put(ONE_DAY.ordinal(), DAY * 60);
        }

        @Override
        public void onReceive(Context context, Intent intent) {

            int chartPeriod = 15;

            ChartSelector.ChartSelectorState chartSelectorState = (ChartSelector.ChartSelectorState) intent.getSerializableExtra("chartSelectorState");

            boolean isZoomedOut = intent.getBooleanExtra("isZoomedOut", false);

            int hoursToSubtract = getHoursToSubtract(isZoomedOut, hoursToLoadEveryZoomout.get(chartSelectorState.ordinal()), chartSelectorState.ordinal());
            if (hoursToSubtract >= hoursToLoadEveryZoomout.get(chartSelectorState.ordinal()) * 10) {
                return;
            }
            switch (chartSelectorState) {
                case FIFTEEN_MIN:
                    chartPeriod = 15;
                    hoursToSubstract.put(chartSelectorState.ordinal(),
                            hoursToSubtract);
                    break;
                case THIRTY_MINS:
                    chartPeriod = 30;
                    hoursToSubstract.put(chartSelectorState.ordinal(),
                            hoursToSubtract);
                    break;
                case ONE_HOUR:
                    chartPeriod = 60;
                    hoursToSubstract.put(chartSelectorState.ordinal(),
                            hoursToSubtract);
                    break;
                case EIGHT_HOURS:
                    chartPeriod = 480;
                    hoursToSubstract.put(chartSelectorState.ordinal(),
                            hoursToSubtract);
                    break;
                case ONE_DAY:
                    chartPeriod = 1440;
                    hoursToSubstract.put(chartSelectorState.ordinal(),
                            hoursToSubtract);
                    break;
            }

            Calendar calendar = Calendar.getInstance();

            if (isZoomedOut) {
                calendar.add(Calendar.HOUR_OF_DAY, -hoursToSubstract.get(chartSelectorState.ordinal()));
            }
            long to = calendar.getTimeInMillis() / 1000;
            Log.i(TAG, "to:" + to);

            if (isZoomedOut) {
                calendar.add(Calendar.HOUR_OF_DAY, -hoursToLoadEveryZoomout.get(chartSelectorState.ordinal()));
            } else {
                calendar.add(Calendar.HOUR_OF_DAY, -hoursToSubstract.get(chartSelectorState.ordinal()));
            }
            long from = calendar.getTimeInMillis() / 1000;
            Log.i(TAG, "from:" + from);

            try {

                JSONObject msg = new JSONObject();
                JSONArray messages = new JSONArray();
                JSONObject info = new JSONObject();

                info.put("Name", instrument.name);
                info.put("ChartType", 2);
                info.put("ChartPeriod", chartPeriod);
                info.put("ResponseType", 12);
                info.put("From", from);
                info.put("To", to);
                messages.put(info);

                msg.put("MessageType", 12);
                msg.put("Messages", messages);
                Log.i(TAG, "about to load from:"
                        + convertMilliSToFormattedDate(from * 1000)
                        + " to: "
                        + convertMilliSToFormattedDate(to * 1000));
                Log.i(TAG, "range: " + TimeUnit.MILLISECONDS.toDays(1000 * (from - to)));
                if (chartSocket != null && chartSocket.clientStatus == SocketUtilities.ClientStatus.CONNECTED)
                    chartSocket.getChart(msg.toString(), isZoomedOut);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private int getHoursToSubtract(boolean isZoomedOut, int daysToLoad, int chartSelector) {
            int value = hoursToSubstract.get(chartSelector);
            int increasedDays = value + daysToLoad;
            int valueToSubstract = isZoomedOut ? increasedDays : value;
            Log.i(TAG, "getHoursToSubtract: " + valueToSubstract);
            return valueToSubstract;
        }
    };

}
