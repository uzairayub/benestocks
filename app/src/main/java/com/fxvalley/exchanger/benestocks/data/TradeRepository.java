package com.fxvalley.exchanger.benestocks.data;

import android.arch.lifecycle.LiveData;
import android.arch.paging.PagedList;

import com.fxvalley.exchanger.benestocks.data.model.DataLoadState;
import com.fxvalley.exchanger.benestocks.data.model.PagingResult;
import com.fxvalley.exchanger.benestocks.models.Trade;


public interface TradeRepository {

    int OPEN = 0;
    int CLOSED = 1;
    int PENDING = 2;
    int CANCELLED = 3;

    LiveData<PagedList<Trade>> getTrades(int type);

    LiveData<DataLoadState> getDataLoadStatus();

    LiveData<PagingResult> getPageInfo();
}
