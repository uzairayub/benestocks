package com.fxvalley.exchanger.benestocks.models;

import net.cocooncreations.template.GeneralUtilities;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by aloupas on 3/8/2017.
 */

public class Base implements Serializable{
    public Integer id;
    public String name;
    public String symbol;
    public String description;
    public String centralBank;

    public Base(JSONObject jsonObject){
        id = jsonObject.optInt("Id");
        name = jsonObject.optString("Name");
        symbol = jsonObject.optString("Symbol");
        description = jsonObject.optString("Description");
        centralBank = jsonObject.optString("CentralBank");
    }

    @Override
    public String toString() {
        return GeneralUtilities.toString(this);
    }
}
