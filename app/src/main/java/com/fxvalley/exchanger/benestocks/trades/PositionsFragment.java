package com.fxvalley.exchanger.benestocks.trades;


import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.recyclerview.extensions.AsyncDifferConfig;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fxvalley.exchanger.benestocks.R;
import com.fxvalley.exchanger.benestocks.activities.MainActivity;
import com.fxvalley.exchanger.benestocks.data.TradeRepository;
import com.fxvalley.exchanger.benestocks.markets.MarketFragment;
import com.fxvalley.exchanger.benestocks.models.Instrument;
import com.fxvalley.exchanger.benestocks.models.InstrumentGroup;
import com.fxvalley.exchanger.benestocks.models.InstrumentLiveData;
import com.fxvalley.exchanger.benestocks.models.Trade;
import com.fxvalley.exchanger.benestocks.utilities.Constants;
import com.fxvalley.exchanger.benestocks.utilities.SPUtils;

import java.util.HashSet;

import static com.fxvalley.exchanger.benestocks.utilities.Constants.POSITIONS_SPINNER_SELECTION;


/**
 * A simple {@link Fragment} subclass.
 */
public class PositionsFragment extends Fragment {

    private static final String[] DROP_DOWN_AVALUES = new String[]{"OPEN", "CLOSED", "PENDING", "CANCELLED"};
    private static final String TAG = "PositionsFragment";

    private PositionsFragment.OnPositionsFragmentFragmentInteractionListener mListener;
    private RecyclerView recyclerView;
    private AppCompatSpinner positionsSpinner;
    private ArrayAdapter spinnerAdapter;
    private MainActivity mainActivity;
    private PositionsAdapter positionsAdapter;
    private HashSet<String> subscribedInstruments = new HashSet<>();
    private Handler handler;
    private Runnable updateRunnable = new Runnable() {
        @Override
        public void run() {

            if (positionsAdapter != null)
                positionsAdapter.notifyDataSetChanged();
            handler.postDelayed(this, 1000);
        }
    };

    TradesViewModel vm;
    ProgressBar progressBar;
    private TextView positionTextView;

    private int spinnerSelection = TradeRepository.OPEN;

    public PositionsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mainActivity = (MainActivity) activity;
    }

    public static PositionsFragment newInstance() {
        PositionsFragment fragment = new PositionsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
        if (savedInstanceState == null) {
            spinnerSelection = getArguments().getInt(POSITIONS_SPINNER_SELECTION);
        } else {
            spinnerSelection = savedInstanceState.getInt(POSITIONS_SPINNER_SELECTION);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);


        outState.putInt(POSITIONS_SPINNER_SELECTION, spinnerSelection);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_positions, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = (RecyclerView) view.findViewById(R.id.positionsRecyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        progressBar = view.findViewById(R.id.progressBar);
        positionTextView = view.findViewById(R.id.position);
        DividerItemDecoration horizontalDecoration = new DividerItemDecoration(recyclerView.getContext(),
                ((LinearLayoutManager) recyclerView.getLayoutManager()).getOrientation());
        Drawable horizontalDivider = ContextCompat.getDrawable(getActivity(), R.drawable.horizontal_white_divider);
        horizontalDecoration.setDrawable(horizontalDivider);
        recyclerView.addItemDecoration(horizontalDecoration);
        positionsAdapter = new PositionsAdapter(DIFF_CALLBACK);
        recyclerView.setAdapter(positionsAdapter);

        positionsSpinner = (AppCompatSpinner) view.findViewById(R.id.positionsSpinner);
        positionsSpinner.setAdapter(spinnerAdapter = new ArrayAdapter<>(view.getContext(), R.layout.transparent_spinner_blac_text, DROP_DOWN_AVALUES));
        positionsSpinner.setSelection(spinnerSelection);
        spinnerAdapter.setDropDownViewResource(R.layout.drop_down_category);

        positionsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                mainActivity.priceSocket.unsubscribeAll();
                subscribedInstruments.clear();
                spinnerSelection = i;
                subscribeToTrades(spinnerSelection);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        vm = ViewModelProviders.of(requireActivity(), mainActivity.tradesVMFactory).get(TradesViewModel.class);
    }

    private void subscribeToTrades(int type) {
        positionTextView.setText("");
        vm.getTrades(type).observe(PositionsFragment.this, pagedList -> {
            positionsAdapter.submitList(pagedList);
        });
        vm.dataLoadStatus().observe(PositionsFragment.this, loadStatus -> {
            switch (loadStatus) {
                case LOADING:
                    progressBar.setVisibility(View.VISIBLE);
                    break;
                case LOADED:
                    progressBar.setVisibility(View.GONE);
                    break;
                case FAILED:
                    progressBar.setVisibility(View.GONE);
                    break;
            }
        });
        vm.getPageInfo().observe(this, info -> {
            positionTextView.setText(
                    String.valueOf(info.getTotalLoaded()) + "/" + String.valueOf(info.getTotalResults()));
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MarketFragment.OnMarketFragmentInteractionListener) {
            mListener = (OnPositionsFragmentFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.i(TAG, "RESUME");
        handler.removeCallbacksAndMessages(null);
        handler.post(updateRunnable);

    }

    @Override
    public void onPause() {
        super.onPause();
        mainActivity.positionsSavedInstanceState(spinnerSelection);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnPositionsFragmentFragmentInteractionListener {

    }


    private class PositionsAdapter extends PagedListAdapter<Trade, PositionsViewHolder> {

        protected PositionsAdapter(@NonNull DiffUtil.ItemCallback<Trade> diffCallback)
        {
            super(diffCallback);
        }

        protected PositionsAdapter(@NonNull AsyncDifferConfig<Trade> config) {
            super(config);
        }

        @Override
        public PositionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            return new PositionsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_positions, parent, false));
        }

        @Override
        public void onBindViewHolder(PositionsViewHolder holder, int position) {

            if (getItem(position) == null) {
                return;
            }

            if (!subscribedInstruments.contains(getItem(position).instrument.name)) {
                mainActivity.priceSocket.subscribeToInstrument(getItem(position).instrument);
                subscribedInstruments.add(getItem(position).instrument.name);
            }

            String profitCurrency = getItem(position).instrument.quote.name +
                    mainActivity.tradeSessionSocket.accounts.get(mainActivity.tradeSessionSocket.activeAccount).currency;
            String profitCurrency2 = mainActivity.tradeSessionSocket.accounts.get(mainActivity.tradeSessionSocket.activeAccount).currency
                    + getItem(position).instrument.quote.name;


            if (!subscribedInstruments.contains(profitCurrency)) {
                mainActivity.priceSocket.subscribe(profitCurrency, String.valueOf(mainActivity.priceSocket.postFix.tAL));
                subscribedInstruments.add(profitCurrency);
            }

            if (!subscribedInstruments.contains(profitCurrency2)) {
                mainActivity.priceSocket.subscribe(profitCurrency2, String.valueOf(mainActivity.priceSocket.postFix.tAL));
                subscribedInstruments.add(profitCurrency2);
            }

            holder.rowPositionsRight.setVisibility(View.GONE);

            InstrumentLiveData instrumentLiveData = null;

            double rate = 1;
            try {

                InstrumentLiveData instrumentLiveData1 = mainActivity.priceSocket.instrumentGroups.get(getItem(position).instrument.instrumentCategoryId)
                        .instruments.get(getItem(position).instrument.id).instrumentLiveData;
                if (instrumentLiveData1 != null) {
                    getItem(position).instrument.instrumentLiveData = instrumentLiveData1;
                }

                for (InstrumentGroup instrumentGroup : mainActivity.priceSocket.instrumentGroups.values()) {
                    for (Instrument instrument : instrumentGroup.instruments.values()) {
                        if (instrument.name.equals(profitCurrency)) {
                            instrumentLiveData = instrument.instrumentLiveData;
                            rate = getItem(position).type == 1 ? instrumentLiveData.ask : instrumentLiveData.bid;
                            break;
                        }
                        if (instrument.name.equals(profitCurrency2)) {
                            instrumentLiveData = instrument.instrumentLiveData;
                            rate = getItem(position).type == 1 ? 1 / instrumentLiveData.ask : 1 / instrumentLiveData.bid;
                            break;
                        }
                    }

                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }

            if (positionsSpinner.getSelectedItemPosition() == TradeRepository.OPEN) {

                double pL = 0;


                pL = getItem(position).calculatePL(positionsSpinner.getSelectedItemPosition(), rate);

                holder.rowPositionsLeft.setText("Open: " + getItem(position).open);
                holder.rowPositionsRight.setVisibility(View.VISIBLE);
                holder.rowPositionsDate.setText(getItem(position).openTime);
                holder.rowPositionsGrossPL.setText(Trade.format(pL));

                if (pL < 0) {
                    holder.rowPositionsGrossPL.setTextColor(ContextCompat.getColor(getContext(), R.color.button_red));
                } else {
                    holder.rowPositionsGrossPL.setTextColor(ContextCompat.getColor(getContext(), R.color.button_green));
                }


            } else if (positionsSpinner.getSelectedItemPosition() == TradeRepository.CLOSED) {

                Log.d(TAG, "CLOSED");

                holder.rowPositionsLeft.setText("Open: " + getItem(position).open);
                holder.rowPositionsRight.setVisibility(View.VISIBLE);
                holder.rowPositionsRight.setText("Closed: " + getItem(position).close);
                holder.rowPositionsDate.setText(getItem(position).closeTime);

                holder.rowPositionsGrossPL.setText(Trade.format(getItem(position).calculatePL(positionsSpinner.getSelectedItemPosition())));

                if (getItem(position).calculatePL(positionsSpinner.getSelectedItemPosition()) < 0) {
                    holder.rowPositionsGrossPL.setTextColor(ContextCompat.getColor(getContext(), R.color.button_red));
                } else {

                    holder.rowPositionsGrossPL.setTextColor(ContextCompat.getColor(getContext(), R.color.button_green));
                }

            } else if (positionsSpinner.getSelectedItemPosition() == TradeRepository.PENDING) {

                Log.d(TAG, "PENDING");

                holder.rowPositionsRight.setVisibility(View.GONE);
                holder.rowPositionsLeft.setText("Pending: " + getItem(position).pendingCancelled);
                holder.rowPositionsDate.setText(getItem(position).pendingCancelledTime);
                holder.rowPositionsGrossPL.setText("");

            } else if (positionsSpinner.getSelectedItemPosition() == TradeRepository.CANCELLED) {

                Log.d(TAG, "CANCELLED");

                holder.rowPositionsRight.setVisibility(View.GONE);
                holder.rowPositionsLeft.setText("Cancelled: " + getItem(position).pendingCancelled);
                holder.rowPositionsDate.setText(getItem(position).pendingCancelledTime);
                holder.rowPositionsGrossPL.setText("");
            }

            if (getItem(position).type == 1) {
                holder.rowPositionsBuySell.setText("B");
                holder.rowPositionsBuySell.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.buy_circle));
                if (positionsSpinner.getSelectedItemPosition() == TradeRepository.OPEN) {
                    holder.rowPositionsRight.setText("Current: " + (getItem(position).instrument.instrumentLiveData != null ? getItem(position).instrument.instrumentLiveData.bid : "-"));
                }

            } else {
                holder.rowPositionsBuySell.setText("S");
                holder.rowPositionsBuySell.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.sell_circle));
                if (positionsSpinner.getSelectedItemPosition() == TradeRepository.OPEN) {
                    holder.rowPositionsRight.setText("Current: " + (getItem(position).instrument.instrumentLiveData != null ? getItem(position).instrument.instrumentLiveData.ask : "-"));
                }
            }

            holder.rowPositionsCurrencyPair.setText(getItem(position).instrument.fixName);
            holder.rowTradesAmount.setText(getItem(position).volume + "");
            holder.rowPositionTicket.setText(" Ticket: " + (getItem(position).ticket));

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SPUtils.getInstance(getContext()).saveStringPreferences("callback","trades");
                    Intent intent = new Intent(view.getContext(), PositionsActivity.class);
                    intent.putExtra(Constants.ACCOUNT, mainActivity.tradeSessionSocket.accounts.get(mainActivity.tradeSessionSocket.activeAccount));
                    intent.putExtra(Constants.TRADE, getItem(position));
                    intent.putExtra(Constants.USER_FILTER, positionsSpinner.getSelectedItemPosition());
                    startActivityForResult(intent,102);

                }
            });

        }

    }

    static class PositionsViewHolder extends RecyclerView.ViewHolder {

        private TextView rowPositionsBuySell;
        private TextView rowPositionsCurrencyPair;
        private TextView rowTradesAmount;
        private TextView rowPositionsGrossPL;
        private TextView rowPositionsDate;
        private TextView rowPositionsRight;
        private TextView rowPositionTicket;
        private TextView rowPositionsLeft;

        public PositionsViewHolder(View itemView) {
            super(itemView);
            rowPositionsBuySell = (TextView) itemView.findViewById(R.id.rowPositionsBuySell);
            rowPositionsCurrencyPair = (TextView) itemView.findViewById(R.id.rowPositionsCurrencyPair);
            rowTradesAmount = (TextView) itemView.findViewById(R.id.rowTradesAmount);
            rowPositionsGrossPL = (TextView) itemView.findViewById(R.id.rowPositionsGrossPL);
            rowPositionsDate = (TextView) itemView.findViewById(R.id.rowPositionsDate);
            rowPositionsRight = (TextView) itemView.findViewById(R.id.rowPositionsRight);
            rowPositionTicket = (TextView) itemView.findViewById(R.id.rowTicket);
            rowPositionsLeft = (TextView) itemView.findViewById(R.id.rowPositionsLeft);

        }
    }

    public DiffUtil.ItemCallback<Trade> DIFF_CALLBACK = new DiffUtil.ItemCallback<Trade>() {
        @Override
        public boolean areItemsTheSame(@NonNull Trade oldTrade, @NonNull Trade newTrade) {
            return oldTrade.id == newTrade.id;
        }

        @Override
        public boolean areContentsTheSame(@NonNull Trade oldTrade, @NonNull Trade newTrade) {
            return oldTrade.equals(newTrade);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(SPUtils.getInstance(getContext()).getStringPreferences("callback").equalsIgnoreCase("trades"))
        {
            MainActivity mainActivity = (MainActivity)getActivity();
            mainActivity.navigationView.getMenu().getItem(1).setCheckable(true);
        }
    }
}
