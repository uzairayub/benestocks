package com.fxvalley.exchanger.benestocks.models;

public interface MessageTypes {

    int ERROR = -3;
    int TRADE = 20;
    int TRADER = 30;
    int LOGON = 40;
    int LOGOUT = 118;
    int COPY = 65;
}
