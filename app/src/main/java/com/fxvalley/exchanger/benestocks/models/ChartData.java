package com.fxvalley.exchanger.benestocks.models;

import com.fxvalley.exchanger.benestocks.utilities.ChartSelector;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by aloupas on 3/15/2017.
 */

public class ChartData {

    private static final SimpleDateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
    private static final SimpleDateFormat toFormat = new SimpleDateFormat("MMM yy", Locale.US);
    public Integer id;
    public String dateTime;
    public String dayDateTime;
    public Double open;
    public Double high;
    public Double low;
    public Double close;
    public Integer instrumentId;
    public Integer barTypeValue;

    public ChartData(JSONObject jsonObject, SimpleDateFormat sdf) {


        id = jsonObject.optInt("Id");
        Date date= null;
        try {
            fromFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = fromFormat.parse(jsonObject.optString("DateTime"));
            dateTime = sdf.format(date);
            dayDateTime = format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            dateTime = jsonObject.optString("DateTime");
        }

        open = jsonObject.optDouble("Open");
        high = jsonObject.optDouble("High");
        low = jsonObject.optDouble("Low");
        close = jsonObject.optDouble("Close");
        instrumentId = jsonObject.optInt("InstrumentId");
        barTypeValue = jsonObject.optInt("BarTypeValue");
    }

    public synchronized String format(Date date) throws ParseException{
        return toFormat.format(date);
    }

    public String getChartDate(ChartSelector.ChartSelectorState selectorState){
        if (selectorState == ChartSelector.ChartSelectorState.ONE_DAY)
            return dayDateTime;
        else
            return dateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChartData chartData = (ChartData) o;

        if (id != null ? !id.equals(chartData.id) : chartData.id != null) return false;
        if (dateTime != null ? !dateTime.equals(chartData.dateTime) : chartData.dateTime != null)
            return false;
        if (dayDateTime != null ? !dayDateTime.equals(chartData.dayDateTime) : chartData.dayDateTime != null)
            return false;
        if (open != null ? !open.equals(chartData.open) : chartData.open != null) return false;
        if (high != null ? !high.equals(chartData.high) : chartData.high != null) return false;
        if (low != null ? !low.equals(chartData.low) : chartData.low != null) return false;
        if (close != null ? !close.equals(chartData.close) : chartData.close != null) return false;
        if (instrumentId != null ? !instrumentId.equals(chartData.instrumentId) : chartData.instrumentId != null)
            return false;
        return barTypeValue != null ? barTypeValue.equals(chartData.barTypeValue) : chartData.barTypeValue == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (dateTime != null ? dateTime.hashCode() : 0);
        result = 31 * result + (dayDateTime != null ? dayDateTime.hashCode() : 0);
        result = 31 * result + (open != null ? open.hashCode() : 0);
        result = 31 * result + (high != null ? high.hashCode() : 0);
        result = 31 * result + (low != null ? low.hashCode() : 0);
        result = 31 * result + (close != null ? close.hashCode() : 0);
        result = 31 * result + (instrumentId != null ? instrumentId.hashCode() : 0);
        result = 31 * result + (barTypeValue != null ? barTypeValue.hashCode() : 0);
        return result;
    }
}
