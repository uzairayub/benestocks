package com.fxvalley.exchanger.benestocks.data;

import android.arch.lifecycle.LiveData;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.arch.paging.PagedList.Config.Builder;
import android.support.annotation.MainThread;
import android.util.SparseArray;

import com.fxvalley.exchanger.benestocks.data.model.DataLoadState;
import com.fxvalley.exchanger.benestocks.data.model.PagingResult;
import com.fxvalley.exchanger.benestocks.models.Trade;

import static android.arch.lifecycle.Transformations.switchMap;


public class TradeRepositoryImpl implements TradeRepository {

    public static final int PAGE_SIZE = 20;
    private LiveData<PagedList<Trade>> trades;
    private SparseArray<TradeDataFactory> dataSourceFactories;
    private int type;

    public TradeRepositoryImpl(SparseArray<TradeDataFactory> dataSourceFactories) {
        this.dataSourceFactories = dataSourceFactories;
    }

    @Override
    @MainThread
    public LiveData<PagedList<Trade>> getTrades(int type) {
        this.type = type;
        PagedList.Config config = new Builder()
                .setInitialLoadSizeHint(PAGE_SIZE)
                .setPageSize(PAGE_SIZE)
                //.setEnablePlaceholders(false)
                .build();
        TradeDataFactory tradeDataFactory = dataSourceFactories.get(type);
        trades = new LivePagedListBuilder<>(tradeDataFactory, config)
                .setInitialLoadKey(0)
                .build();
        return trades;
    }

    @Override
    public LiveData<DataLoadState> getDataLoadStatus() {
        return switchMap(dataSourceFactories.get(type).datasourceLiveData,
                dataSource -> dataSource.loadState);
    }

    @Override
    public LiveData<PagingResult> getPageInfo() {
        return switchMap(dataSourceFactories.get(type).datasourceLiveData,
                dataSource -> dataSource.pageInfo);
    }
}
