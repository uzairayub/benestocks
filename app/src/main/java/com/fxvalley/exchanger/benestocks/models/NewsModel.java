package com.fxvalley.exchanger.benestocks.models;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by aloupas on 3/16/2017.
 */

public class NewsModel {

    private static final SimpleDateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
    private static final SimpleDateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US);


    public String title;
    public String link;
    public String enclosure;
    public String pubDate ="";
    public String author;

    public NewsModel(Node node) {

        NodeList childNodes = node.getChildNodes();

        for (int i = 0; i < childNodes.getLength(); i++) {

            if (childNodes.item(i).getNodeName().equals("title")) {
                title = childNodes.item(i).getTextContent();
            } else if (childNodes.item(i).getNodeName().equals("link")) {
                link = childNodes.item(i).getTextContent();
            } else if (childNodes.item(i).getNodeName().equals("enclosure")) {
                enclosure = childNodes.item(i).getAttributes().getNamedItem("url").getTextContent();
            } else if (childNodes.item(i).getNodeName().equals("pubDate")) {
                try {
                    pubDate = toFormat(fromFormat(childNodes.item(i).getTextContent()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else if (childNodes.item(i).getNodeName().equals("author")) {
                author = childNodes.item(i).getTextContent();
            }
        }
    }

    private synchronized static Date fromFormat(String date) throws ParseException {

        return fromFormat.parse(date);
    }

    private synchronized static String toFormat(Date date){
        return toFormat.format(date);
    }
}
