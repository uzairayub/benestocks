package com.fxvalley.exchanger.benestocks.fragments;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.fxvalley.exchanger.benestocks.R;
import com.fxvalley.exchanger.benestocks.models.NewsModel;

import net.cocooncreations.template.network.CocoonStringRequest;
import net.cocooncreations.template.network.RequestFragment;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import static android.content.Intent.ACTION_VIEW;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends RequestFragment {

    public static final String NEWS_URL = "https://www.investing.com/rss/investing_news.rss";

    private RecyclerView newsRecyclerView;
    private NewsAdapter newsAdapter;
    public List<NewsModel> newsModelList = new ArrayList<>();

    public NewsFragment() {
        // Required empty public constructor
    }

    public static NewsFragment newInstance() {
        NewsFragment fragment = new NewsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_news, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        newsRecyclerView = (RecyclerView) view.findViewById(R.id.newsRecyclerView);
        newsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        newsRecyclerView.setAdapter(newsAdapter = new NewsAdapter());
    }

    @Override
    public void onResume() {
        super.onResume();

        CocoonStringRequest.with(NEWS_URL, this).callAsync();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public class NewsViewholder extends RecyclerView.ViewHolder {

        public TextView title;
        public ImageView enclosure;
        public TextView pubDate;
//        public TextView author;

        public NewsViewholder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.newsRowTitle);
            enclosure = (ImageView) itemView.findViewById(R.id.newsRowImage);
            pubDate = (TextView) itemView.findViewById(R.id.newsRowTime);
        }
    }

    public class NewsAdapter extends RecyclerView.Adapter<NewsViewholder> {

        @Override
        public NewsViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new NewsViewholder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_news, parent, false));
        }

        @Override
        public void onBindViewHolder(NewsViewholder holder, int position) {

            holder.title.setText(newsModelList.get(position).title);
            Glide.with(holder.itemView.getContext()).load(newsModelList.get(position).enclosure).into(holder.enclosure);
            holder.pubDate.setText(newsModelList.get(position).pubDate);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        Intent intent = new Intent(ACTION_VIEW, Uri.parse(newsModelList.get(position).link));
                        startActivity(intent);
                    } catch (ActivityNotFoundException ex) {
                        ex.printStackTrace();

                        Toast.makeText(getContext(), R.string.could_not_open_url, Toast.LENGTH_LONG).show();
                    }

                }
            });

        }

        @Override
        public int getItemCount() {
            return newsModelList.size();
        }
    }

    @Override
    public <T> void onResponse(T response, String requestcode, NetworkResponse networkResponse) {

        String reply = (String) response;

        DocumentBuilderFactory documentBuilder = DocumentBuilderFactory.newInstance();
        try {

            DocumentBuilder documentBuilder1 = documentBuilder.newDocumentBuilder();

            InputStream stream = new ByteArrayInputStream(reply.getBytes("UTF-8"));

            Document parse = documentBuilder1.parse(stream);

            NodeList items = parse.getElementsByTagName("item");

            newsModelList.clear();

            for (int i = 0; i < items.getLength(); i++) {
                newsModelList.add(new NewsModel(items.item(i)));
            }

            newsAdapter.notifyDataSetChanged();


        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(VolleyError volleyError) {

    }


    public interface OnNewsFragmentInteractionListener {
    }

}
