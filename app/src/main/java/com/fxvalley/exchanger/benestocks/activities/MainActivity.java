package com.fxvalley.exchanger.benestocks.activities;

import android.app.SearchManager;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.fxvalley.exchanger.benestocks.MyApplication;
import com.fxvalley.exchanger.benestocks.R;
import com.fxvalley.exchanger.benestocks.data.TradeRepository;
import com.fxvalley.exchanger.benestocks.fragments.CalendarFragment;
import com.fxvalley.exchanger.benestocks.markets.MarketFragment;
import com.fxvalley.exchanger.benestocks.fragments.NewsFragment;
import com.fxvalley.exchanger.benestocks.markets.MarketViewModelFactory;
import com.fxvalley.exchanger.benestocks.markets.MarketsViewModel;
import com.fxvalley.exchanger.benestocks.trades.PositionsFragment;
import com.fxvalley.exchanger.benestocks.models.ContractSpecs;
import com.fxvalley.exchanger.benestocks.models.User;
import com.fxvalley.exchanger.benestocks.sockets.PriceSessionSocket;
import com.fxvalley.exchanger.benestocks.sockets.PriceSocketListener;
import com.fxvalley.exchanger.benestocks.sockets.SocketUtilities;
import com.fxvalley.exchanger.benestocks.sockets.TradeSessionSocket;
import com.fxvalley.exchanger.benestocks.sockets.TradeSocketsConnectionListener;
import com.fxvalley.exchanger.benestocks.trades.TradesVMFactory;
import com.fxvalley.exchanger.benestocks.utilities.AccountSnapshot;
import com.fxvalley.exchanger.benestocks.utilities.Constants;
import com.fxvalley.exchanger.benestocks.utilities.SPUtils;
import com.fxvalley.exchanger.benestocks.utilities.ToastView;
import com.fxvalley.exchanger.benestocks.utilities.TopHederViewAdapter;

import static com.fxvalley.exchanger.benestocks.utilities.Constants.POSITIONS_SPINNER_SELECTION;


public class MainActivity extends AppCompatActivity implements MarketFragment.OnMarketFragmentInteractionListener,
        NewsFragment.OnNewsFragmentInteractionListener,
        CalendarFragment.OnCalendarFragmentFragmentInteractionListener,
        PositionsFragment.OnPositionsFragmentFragmentInteractionListener, TradeSocketsConnectionListener, PriceSocketListener {

    private static final String ITEM_ID = "item_id";
    private static final String TAG = "MainActivity";

    private Toolbar toolbar;
    private ActionBarDrawerToggle mDrawerToggle;

    private DrawerLayout mDrawerLayout;
    public NavigationView navigationView;
    private int fragmentItemId;

    private RecyclerView topHeaderRecyclerView;
    private TopHederViewAdapter topheaderAdapter;
    private User user;

    private View progressView;
    private ProgressBar progressBar;
    private TextView progresTextView;
    private AppCompatSpinner accountSpinner;


    public TradeSessionSocket tradeSessionSocket;
    public PriceSessionSocket priceSocket;
    public AccountSnapshot accountSnapshot = new AccountSnapshot();

    private int positionsSpinnerPosition = TradeRepository.OPEN;

    private boolean tradeSessionSocketConnected;
    private boolean priceSessionConnected;

    private boolean isPaused;

    public TradesVMFactory tradesVMFactory;
    public MarketViewModelFactory marketViewModelFactory;
    private SearchView searchView;
    private MarketsViewModel marketsViewModel;
    private MenuItem searchItem;

//    uzair start here
    private TextView tvVersionName;
    private boolean restart;
    private ImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        if (savedInstanceState != null) {
            fragmentItemId = savedInstanceState.getInt(ITEM_ID);
            positionsSpinnerPosition = savedInstanceState.getInt(POSITIONS_SPINNER_SELECTION);
        } else {
            fragmentItemId = R.id.market;
            positionsSpinnerPosition = TradeRepository.OPEN;
        }
        user = User.getUser(this);

        tradeSessionSocket = new TradeSessionSocket(user, new ToastView(this, new Handler()));
        priceSocket = new PriceSessionSocket(user);
        tradesVMFactory = new TradesVMFactory(tradeSessionSocket);
        marketViewModelFactory = new MarketViewModelFactory(priceSocket);
        marketsViewModel = ViewModelProviders.of(this, marketViewModelFactory).get(MarketsViewModel.class);

        toolbar = (Toolbar) findViewById(R.id.mainactivity_toolbar);
        toolbar.setTitle("");
//        if (fragmentItemId == R.id.market || fragmentItemId == R.id.group_market) {
//            toolbar.setTitle(R.string.market);
//        } else if (fragmentItemId == R.id.news || fragmentItemId == R.id.group_news) {
//            toolbar.setTitle(R.string.news);
//        } else if (fragmentItemId == R.id.calendar || fragmentItemId == R.id.group_calendar) {
//            toolbar.setTitle(R.string.calendar);
//        } else if (fragmentItemId == R.id.positions || fragmentItemId == R.id.group_positions) {
//            toolbar.setTitle(R.string.positions);
//        }
        setSupportActionBar(toolbar);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.activity_main);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        View headerView = navigationView.getHeaderView(0);

//        headerView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                tradeSessionSocket.updateAccount();
//            }
//        });

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                mDrawerLayout.closeDrawers();
                if (item.getItemId() != R.id.logout)
                {
                    fragmentItemId = item.getItemId();
                    if (item.isChecked()) item.setChecked(false);
                    else item.setChecked(true);
                    switchChildFragment(getFragment(fragmentItemId), fragmentItemId);
                }
                else
                {
                    logout();
                }
                return true;
            }
        });

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar,
                R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);

                if (fragmentItemId == R.id.market || fragmentItemId == R.id.group_market) {
//                    toolbar.setTitle(R.string.market);
                } else if (fragmentItemId == R.id.news || fragmentItemId == R.id.group_news) {
//                    toolbar.setTitle(R.string.news);
                } else if (fragmentItemId == R.id.calendar || fragmentItemId == R.id.group_calendar) {
//                    toolbar.setTitle(R.string.calendar);
                } else if (fragmentItemId == R.id.positions || fragmentItemId == R.id.group_positions) {
//                    toolbar.setTitle(R.string.positions);
                }

                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                toolbar.setTitle(R.string.menu);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        topHeaderRecyclerView = (RecyclerView) findViewById(R.id.status_recyclerview);
        topHeaderRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        topHeaderRecyclerView.setAdapter(topheaderAdapter = new TopHederViewAdapter(this));

        progressView = findViewById(R.id.progressView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progresTextView = (TextView) findViewById(R.id.progressTextView);

        accountSpinner = (AppCompatSpinner) headerView.findViewById(R.id.accountSpinner);

//        uzair
        tvVersionName = findViewById(R.id.tvVersionName);
        setVersionName();

        restart = getIntent().getExtras().getBoolean("restart");
        Log.d("","");

        imageView = findViewById(R.id.imageView);
        if(restart && Constants.bitmap != null)
        {
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageBitmap(Constants.bitmap);
        }
    }

    private void setVersionName()
    {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = "App Version "+pInfo.versionName;
            tvVersionName.setText(version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void logout() {
        new MaterialDialog.Builder(MainActivity.this)
                .title(R.string.logut)
                .onPositive((dialog, which) -> {
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                })
                .positiveText(R.string.yes)
                .content(R.string.are_you_sure_logout)
                .onNegative((dialog, which) -> dialog.dismiss())
                .negativeText(R.string.cancel)
                .show();
    }

    private void switchChildFragment(Fragment fragment, int itemId) {

        if (fragment == null) return;

        fragmentItemId = itemId;

        FragmentManager supportFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_placeholder, fragment);
        fragmentTransaction.commitAllowingStateLoss();
        changeMenuAppearance(itemId);

    }

    private void changeMenuAppearance(int itemId) {
        searchItem.setVisible(itemId == R.id.market);
        supportInvalidateOptionsMenu();
        //todo fix menu appearance
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.mainactivity_menu, menu);

        searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) MainActivity.this.getSystemService(Context.SEARCH_SERVICE);
        searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(MainActivity.this.getComponentName()));
        }
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                searchItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                marketsViewModel.filterResults(s);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(ITEM_ID, fragmentItemId);
        outState.putInt(POSITIONS_SPINNER_SELECTION, positionsSpinnerPosition);
    }

    @Override
    protected void onResume() {
        super.onResume();

        user = User.getUser(this);

        isPaused = false;

        if (user == null) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }

        accountSnapshot.onResume(this, tradeSessionSocket, priceSocket, topheaderAdapter);
        priceSocket.connect(this);
        tradeSessionSocket.connect(this);
    }

    @Override
    protected void onPause() {

        isPaused = true;
        accountSnapshot.onPause();
        accountSpinner.setAdapter(null);
        accountSpinner.setOnItemSelectedListener(null);
        takeScreenshot();
        super.onPause();
    }

    private void takeScreenshot()
    {
        mDrawerLayout.setDrawingCacheEnabled(true);
        mDrawerLayout.buildDrawingCache(true);
        Constants.bitmap = Bitmap.createBitmap(mDrawerLayout.getDrawingCache());
        mDrawerLayout.setDrawingCacheEnabled(false);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isFinishing()) {
            if (tradeSessionSocket != null)
                tradeSessionSocket.disconnect();

            if (priceSocket != null) {
                priceSocket.disconnect();
            }
        }
    }

    @Override
    public void onTradeSocketStateChanged(SocketUtilities.ClientStatus clientStatus) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (clientStatus) {
                    case CONNECTED:
                        if (!isPaused)
                            tradeSessionSocket.getSession();
                        break;
                    case DISCONNECTED:
                        tradeSessionSocketConnected = false;
                        if (progressView != null) {
                            progressBar.setVisibility(View.GONE);
                            progressView.setVisibility(View.VISIBLE);
                            progresTextView.setText(R.string.diconnected);
                        }
                        break;
                    case CONNECTING:
                        tradeSessionSocketConnected = false;
                        if (progressView != null) {
                            progressBar.setVisibility(View.VISIBLE);
                            progressView.setVisibility(View.VISIBLE);
                            progresTextView.setText(R.string.establishing_connection);
                            if(restart)
                            {
                                progresTextView.setText("Please wait");
                            }
                        }
                        break;
                }
            }
        });
    }

    @Override
    public void onSessionFetched() {
        tradeSessionSocket.subscribe();
        tradeSessionSocket.getAccount();
    }


    @Override
    public void onLastPriceFetched() {

        LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(new Intent(Constants.LAST_PRICE_FETCHED));
        if(imageView.getVisibility() == View.VISIBLE)
        {
            imageView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onGetAccount() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                int selectedAccount = 0;

                if (priceSessionConnected) {
                    switchChildFragment(getFragment(fragmentItemId), fragmentItemId);
                    if (progressView != null)
                    {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressView.setVisibility(View.GONE);
                            }
                        },3000);
                    }

                }
                tradeSessionSocketConnected = true;

                for (int i = 0; i < tradeSessionSocket.accounts.size(); i++) {
                    if (tradeSessionSocket.accounts.get(i).isActive) {
                        selectedAccount = i;
                        break;
                    }
                }

                accountSpinner.setAdapter(new AccountSpinnerAdapter());
                accountSpinner.setSelection(selectedAccount);
                accountSpinner.post(new Runnable() {
                    @Override
                    public void run() {
                        accountSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                                if (progressView != null) {
                                    progressBar.setVisibility(View.VISIBLE);
                                    progressView.setVisibility(View.VISIBLE);
                                    progresTextView.setText(R.string.switching_account);
                                    tradeSessionSocket.updateAccount(tradeSessionSocket.accounts.get(i));
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }
                });

                accountSnapshot.onTradeSessionSocketReady();


            }
        });

    }

    @Override
    public void onAccountSwitched(boolean success) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (success) {
                    if (progressView != null) {
                        progressView.setVisibility(View.GONE);
                        Toast.makeText(MainActivity.this, R.string.succesfully_switched_acc, Toast.LENGTH_SHORT).show();
                        switchChildFragment(getFragment(R.id.market), R.id.market);
                        accountSnapshot.onTradeSessionSocketReady();
                    }

                } else {
                    Toast.makeText(MainActivity.this, R.string.error_switching_account, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void onContractFetched(ContractSpecs contractSpecs) {

    }

    @Override
    public void onPriceSocketStateChanged(SocketUtilities.ClientStatus clientStatus) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (clientStatus) {
                    case CONNECTED:
//                        priceSocket.getInstrumentsCategories();
                        break;
                    case DISCONNECTED:
                        priceSessionConnected = false;
                        if (progressView != null) {
                            progressBar.setVisibility(View.GONE);
                            progressView.setVisibility(View.VISIBLE);
                            progresTextView.setText(R.string.diconnected);
                        }
                        break;
                    case CONNECTING:
                        priceSessionConnected = false;
                        if (progressView != null) {
                            progressBar.setVisibility(View.VISIBLE);
                            progressView.setVisibility(View.VISIBLE);
                            progresTextView.setText(R.string.establishing_connection);
                            if(restart)
                            {
                                progresTextView.setText("Please wait");
                            }
                        }
                        break;
                }
            }
        });
    }

    @Override
    public void onCategoriesFecthed() {


        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                priceSessionConnected = true;


                if (tradeSessionSocketConnected) {

                    if (progressView != null)
                        progressView.setVisibility(View.GONE);
                    imageView.setVisibility(View.GONE);

                    switchChildFragment(getFragment(fragmentItemId), fragmentItemId);


                }

                accountSnapshot.onPriceSessionSocketReady();

            }
        });


    }

    private Fragment getFragment(int id) {
        Fragment fragment = null;
        Bundle bundle = new Bundle();

        if (id == R.id.market || id == R.id.group_market) {
            Log.i(TAG, "position fragment == MarketFragment");
            fragment = MarketFragment.newInstance();
        } else if (id == R.id.news || id == R.id.group_news) {
            fragment = NewsFragment.newInstance();
            Log.i(TAG, "position fragment == NewsFragment");
        } else if (id == R.id.calendar || id == R.id.group_calendar) {
            fragment = CalendarFragment.newInstance();
            Log.i(TAG, "position fragment == CalendarFragment");
        } else if (id == R.id.positions || id == R.id.group_positions) {
            fragment = PositionsFragment.newInstance();
            Log.i(TAG, "position fragment == PositionsFragment");
            bundle.putInt(POSITIONS_SPINNER_SELECTION, positionsSpinnerPosition);
        }

        if (fragment != null)
            fragment.setArguments(bundle);

        return fragment;
    }


    public void positionsSavedInstanceState(int position) {
        positionsSpinnerPosition = position;
    }


    public class AccountSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

        @Override
        public int getCount() {
            return tradeSessionSocket.accounts.size();
        }

        @Override
        public Object getItem(int i) {
            return tradeSessionSocket.accounts.get(i).name;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            TextView textView = (TextView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.positions_spinner, viewGroup, false);
            textView.setText(tradeSessionSocket.accounts.get(i).name);
            textView.setGravity(Gravity.LEFT);

            return textView;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {

            TextView textView = (TextView) LayoutInflater.from(parent.getContext()).inflate(R.layout.drop_down_category, parent, false);
            textView.setText(tradeSessionSocket.accounts.get(position).name);

            return textView;

        }
    }

//    private BroadcastReceiver totalOpenReceived = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//
//            Log.d(TAG, "totalopenreceived");
//
//            topheaderAdapter.updateTopHeaderValues(tradeSessionSocket.accounts.get(tradeSessionSocket.activeAccount), intent.getDoubleExtra(Constants.CALCULATED_PL, 0.0));
//        }
//    };

//    private Runnable updateHeadersRunnable = new  Runnable(){
//
//        @Override
//        public void run() {
//
//            if (tradeSessionSocket !=null && tradeSessionSocket.accountFetched){
//
//                tradeSessionSocket.getOpen();
//            }
//            handler.postDelayed(this, 1000);
//
//        }
//    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(SPUtils.getInstance(MainActivity.this).getStringPreferences("callback").equalsIgnoreCase("market"))
        {
            finish();
            Intent intent = this.getIntent();
            intent.putExtra("restart", true);
            startActivity(intent);
        }

    }
}
