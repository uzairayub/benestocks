package com.fxvalley.exchanger.benestocks.sockets;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.fxvalley.exchanger.benestocks.MyApplication;
import com.fxvalley.exchanger.benestocks.models.AccountPostFix;
import com.fxvalley.exchanger.benestocks.models.Instrument;
import com.fxvalley.exchanger.benestocks.models.InstrumentGroup;
import com.fxvalley.exchanger.benestocks.models.InstrumentLiveData;
import com.fxvalley.exchanger.benestocks.models.User;
import com.fxvalley.exchanger.benestocks.utilities.Constants;
import com.fxvalley.exchanger.benestocks.utilities.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import ws.wamp.jawampa.WampClient;
import ws.wamp.jawampa.WampClientBuilder;
import ws.wamp.jawampa.connection.IWampConnectorProvider;
import ws.wamp.jawampa.transport.netty.NettyWampClientConnectorProvider;

/**
 * Created by aloupas on 3/2/2017.
 */

public class PriceSessionSocket {

    private final String TAG = "PriceSessionSocket";

    private final String PRICE_URL = Constants.WEB_SOCKET_API_URL + ":23023/ws";

    public SocketUtilities.ClientStatus clientStatus = SocketUtilities.ClientStatus.DISCONNECTED;

    private WampClient client;
    private WampClientBuilder builder = new WampClientBuilder();
    private IWampConnectorProvider connectorProvider = new NettyWampClientConnectorProvider();

    private List<Subscription> subscriptionList = new ArrayList<>();

    private HashMap<String, Subscription> statusOpenMap = new HashMap<>();

    private User user;
    private Subscription subscription;
    public AccountPostFix postFix;

    public static Map<Integer, InstrumentGroup> instrumentGroups = new LinkedHashMap<>();
    public Integer groupId = 1;

    private PriceSocketListener socketsConnectionListener;
    public boolean categoriesFetched;

    public PriceSessionSocket(User user) {
        buildClient();
        this.user = user;
    }

    private void buildClient() {

        try {
            builder.withUri(PRICE_URL)
                    .withRealm("fxplayer")
                    .withInfiniteReconnects()
                    .withCloseOnErrors(true)
                    .withReconnectInterval(5, TimeUnit.SECONDS)
                    .withConnectorProvider(connectorProvider);

            client = builder.build();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void connect(PriceSocketListener socketsConnectionListener) {

        this.socketsConnectionListener = socketsConnectionListener;
        // subscribe for session status changes
        client.statusChanged().subscribe(new Action1<WampClient.State>() {
            @Override
            public void call(WampClient.State t1) {
                Log.i(TAG, "Session status changed to " + t1);
                if (t1 instanceof WampClient.ConnectedState) {
                    Log.d(TAG, "CONNECTED");
                    clientStatus = SocketUtilities.ClientStatus.CONNECTED;
                    ping(user.token);
                } else if (t1 instanceof WampClient.DisconnectedState) {
                    Log.d(TAG, "DISCONNECTED");
                    clientStatus = SocketUtilities.ClientStatus.DISCONNECTED;
                } else if (t1 instanceof WampClient.ConnectingState) {
                    // Client starts connecting to the remote router
                    Log.d(TAG, "CONNECTING");
                    clientStatus = SocketUtilities.ClientStatus.CONNECTING;
                }

                socketsConnectionListener.onPriceSocketStateChanged(clientStatus);
            }
        });
        // request to open the connection with the server
        client.open();
    }

    public void disconnect() {
        client.close();
        client = null;
    }


    public void ping(String token) {
        // was fixed
        JSONObject message = new JSONObject();

        try {
//            message.put("Host", "login.thefxplayer.com");
            message.put("Host", Constants.WS_HOST);
            message.put("Token", token);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        client.call("com.fxplayer.ping", JSONObject.class, message.toString())
                .doOnNext(jsonObject -> {
                    //                Log.d(TAG, "accountPostfix : " + reply.toString());
                    Log.d(TAG, "accountPostfix fetched ");
                    JSONArray messages = null;
                    try {

                        messages = jsonObject.getJSONArray("Messages");
                        postFix = new AccountPostFix(messages.getJSONObject(0));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                })
                .flatMap(jsonObject -> getInstrumentsCategories(true))
                .subscribe(reply -> {
                    Log.i(TAG, "ping: success");
                }, arg0 -> {
                    Log.e(TAG, "ping: ", arg0);
//                Log.d(TAG, reply);
                });

    }

    public Observable<Map<Integer, InstrumentGroup>> getInstrumentsCategories(boolean shouldNotifyGlobalCallback) {
        Log.i(TAG, "getInstrumentsCategories: ");

        return client.call("com.fxplayer.instrumentscategories", JSONObject.class, "")
                .map(jsonObject -> {

                    Log.d(TAG, "categoried fetched ");

                    try {
                        JSONArray messages = jsonObject.getJSONArray("Messages");

                        for (int i = 0; i < messages.length(); i++) {


                            InstrumentGroup instrumentGroup = new InstrumentGroup(messages.getJSONObject(i));
                            Log.d(TAG, "instrumentGroup: " + instrumentGroup.name);

                            instrumentGroups.put(instrumentGroup.id, instrumentGroup);

                            Utilities.sortByValueInstrument(instrumentGroup.instruments);
                        }

                        Utilities.sortByValue(instrumentGroups);
                        categoriesFetched = true;
                        if (shouldNotifyGlobalCallback) {
                            socketsConnectionListener.onCategoriesFecthed();
                        }
//                    LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcastSync(new Intent(Constants.CATEGORY_CHANGED));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return instrumentGroups;
                });
    }

    public synchronized void subscribeTo(Integer integer) {

        groupId = integer;

        for (Subscription subscription : subscriptionList) {
            unsubscribe(subscription);
        }

        subscriptionList.clear();

        for (Instrument instrument : instrumentGroups.get(integer).instruments.values()) {

            subscriptionList.add(subscribe(instrument.name, String.valueOf(postFix.tAL)));
        }
    }

    public synchronized void subscribeForStatus(String instrumentNames[]) {

        if (postFix == null)
            return;

        for (Subscription subscription : statusOpenMap.values()) {
            unsubscribe(subscription);
        }

        statusOpenMap.clear();

        for (int i = 0; i < instrumentNames.length; i++) {
            statusOpenMap.put(instrumentNames[i], subscribe(instrumentNames[i], String.valueOf(postFix.tAL)));
        }

    }

    public void getInstruments(String filter) {

        client.call("com.fxplayer.instruments", JSONObject.class, filter).subscribe(new Action1<JSONObject>() {
            @Override
            public void call(JSONObject reply) {

                Log.d(TAG, "instruments : " + reply.toString());
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable arg0) {
                arg0.printStackTrace();
            }
        });
    }

    public synchronized void getLastPriceGeneral(JSONObject jsonObject) {
        Log.i(TAG, "getLastPriceGeneral: ");
        client.call("com.fxplayer.quote", JSONObject.class, jsonObject.toString()).subscribe(new Action1<JSONObject>() {
            @Override
            public void call(JSONObject reply) {

                Log.d(TAG, "lastprice fetched ");

                try {
                    JSONArray messages = reply.getJSONArray("Messages");

                    for (int i = 0; i < messages.length(); i++) {

                        Instrument instrument1 = new Instrument(messages.getJSONObject(i).getJSONObject("I"));
                        instrument1.instrumentLiveData = new InstrumentLiveData(messages.getJSONObject(i));

//                        Log.d(TAG, instrument1.name);
//                        Log.d(TAG, instrument1.fixName);

                        for (InstrumentGroup instrumentGroup : instrumentGroups.values()) {
                            for (Instrument instrument : instrumentGroup.instruments.values()) {

                                if (instrument.id.equals(instrument1.id)) {
                                    if (instrument1.instrumentLiveData != null) {
                                        instrument.instrumentLiveData = instrument1.instrumentLiveData;
                                    }
                                    break;
                                }

                            }
                        }

//                        instrumentGroups.get(groupId).instruments.get(instrument1.id).instrumentLiveData = instrument1.instrumentLiveData;
                    }


                    socketsConnectionListener.onLastPriceFetched();


                } catch (JSONException e) {
                    e.printStackTrace();
                }

//                    Log.d(TAG, "last price : " + reply.toString());
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable arg0) {
                arg0.printStackTrace();
            }
        });

    }

    public synchronized void getLastPriceInstrument(String[] names) {

        try {

            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("messagetype", 1);

            JSONArray jsonArray = new JSONArray();

            for (String name : names) {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", name);
                jsonObject.put("responsetype", 10);
                jsonArray.put(jsonObject);
            }

            jsonObject1.put("messages", jsonArray);

            getLastPriceGeneral(jsonObject1);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public void getLastPrice(Integer integer) {

        groupId = integer;

        try {

            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("messagetype", 1);

            JSONArray jsonArray = new JSONArray();

            for (Instrument instrument : instrumentGroups.get(integer).instruments.values()) {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", instrument.name);
                jsonObject.put("responsetype", 10);
                jsonArray.put(jsonObject);
            }

            jsonObject1.put("messages", jsonArray);

            getLastPriceGeneral(jsonObject1);

        } catch (JSONException e) {
            e.printStackTrace();

        }
    }

    public synchronized Subscription subscribe(String instrument, String postFix) {

        Log.d(TAG, "subscribing to " + instrument);

        subscription = client.makeSubscription(instrument + postFix, JSONObject.class).subscribe(new Action1<JSONObject>() {

            @Override
            public void call(JSONObject jsonObject) {

                try {
                    JSONArray messages = jsonObject.getJSONArray("Messages");

                    Instrument instrument1 = new Instrument(messages.getJSONObject(0).getJSONObject("I"));

                    instrument1.instrumentLiveData = new InstrumentLiveData(messages.getJSONObject(0));

                    if (instrument1.instrumentCategoryId.intValue() == groupId.intValue() ||
                            statusOpenMap.containsKey(instrument1.name)) {

                        Intent intent = new Intent(Constants.TICK_UPDATED);

                        for (InstrumentGroup instrumentGroup : instrumentGroups.values())
                        {
                            for (Instrument instrument : instrumentGroup.instruments.values())
                            {
                                if (instrument.id.equals(instrument1.id))
                                {
                                    if (instrument1.instrumentLiveData != null)
                                    {
                                        instrument.instrumentLiveData = instrument1.instrumentLiveData;
                                    }
                                    intent.putExtra(Constants.GROUP_ID, instrument1.instrumentCategoryId);
                                    intent.putExtra(Constants.SYMBOL_ID, instrument1.id);
                                    break;
                                }

                            }
                        }

                        if (instrument1.instrumentCategoryId.intValue() == groupId.intValue())
                            if (instrument1.instrumentLiveData!=null) {
                                instrumentGroups.get(groupId).instruments.get(instrument1.id).instrumentLiveData = instrument1.instrumentLiveData;
                            }

                        if (statusOpenMap.containsKey(instrument1.name))
                            intent.putExtra(Constants.INSTRUMENT, instrument1);
                        LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(intent);

                    } else {
                        unsubscribe(subscription);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                throwable.printStackTrace();
            }
        });

        return subscription;

    }

    public synchronized void unsubscribe(Subscription subscription) {
        if (subscription != null && !subscription.isUnsubscribed())
            this.subscription.unsubscribe();
    }

    public synchronized void unsubscribeAll() {
        if (subscriptionList != null)
            for (Subscription subscription : subscriptionList) {
                unsubscribe(subscription);
            }
    }

    public synchronized void subscribeToInstrument(Instrument instrument) {

        subscribe(instrument.name, String.valueOf(postFix.tAL));

    }

    public Observable<Map<Integer, InstrumentGroup>> getInstrumentGroups() {
        return getInstrumentsCategories(false);

//    public void callPing(String token) {
//
//        mConnection.call("com.fxplayer.ping", String.class, new Wamp.CallHandler() {
//
//            @Override
//            public void onResult(Object result) {
//
//                Log.d(TAG, (String) result);
//            }
//
//            @Override
//            public void onError(String errorUri, String errorDesc) {
//                Log.d(TAG, "error: " + errorUri);
//            }
//        }, token);
//
//    }
//

//
//    public void getInstruments(String filter) {
//        mConnection.call("com.fxplayer.instruments", String.class, new Wamp.CallHandler() {
//
//            @Override
//            public void onResult(Object result) {
//
//                Log.d(TAG, (String) result);
//            }
//
//            @Override
//            public void onError(String errorUri, String errorDesc) {
//                Log.d(TAG, "error: " + errorUri);
//            }
//        }, filter);
//    }
//
//    public void getLastPrice(List<Instrument> instruments) {
//        mConnection.call("com.fxplayer.quote", String.class, new Wamp.CallHandler() {
//
//            @Override
//            public void onResult(Object result) {
//
//                Log.d(TAG, (String) result);
//            }
//
//            @Override
//            public void onError(String errorUri, String errorDesc) {
//                Log.d(TAG, "error: " + errorUri);
//            }
//        }, instruments);
//    }
//
//    public void subscribe(String name, String accountPostFix) {
//        mConnection.subscribe(name + accountPostFix, WampMessage.Event.class, new Wamp.EventHandler() {
//            @Override
//            public void onEvent(String topicUri, Object event) {
//
//            }
//        });
//    }
//
//    public void unsubscribe(Object name, String accountPostFix) {
//        mConnection.unsubscribe(name + accountPostFix);
//    }
    }

    public Observable<Map<Integer, InstrumentGroup>> getInstrumentsNow() {
        return Observable.just(instrumentGroups);
    }
}
