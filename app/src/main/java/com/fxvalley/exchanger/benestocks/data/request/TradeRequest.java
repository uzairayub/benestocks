package com.fxvalley.exchanger.benestocks.data.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TradeRequest {
    @SerializedName("Type")
    @Expose
    int type;
    @SerializedName("Page")
    @Expose
    int page;
    @SerializedName("ResultsPerPage")
    @Expose
    int resultsPerPage;

    public TradeRequest(int type, int page, int resultsPerPage) {
        this.type = type;
        this.page = page;
        this.resultsPerPage = resultsPerPage;
    }
}
