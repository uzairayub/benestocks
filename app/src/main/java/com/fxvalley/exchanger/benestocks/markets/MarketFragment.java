package com.fxvalley.exchanger.benestocks.markets;

import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fxvalley.exchanger.benestocks.R;
import com.fxvalley.exchanger.benestocks.activities.MainActivity;
import com.fxvalley.exchanger.benestocks.utilities.Constants;

import java.util.LinkedList;


public class MarketFragment extends Fragment {
    private OnMarketFragmentInteractionListener mListener;

    private static final String TAG = "MarketFragment";

    private LinkedList<Integer> instrumentsIds;
    private TextView rowMarketLeftchooser;
    private AppCompatSpinner rightChoiceSpinner;
    private ViewPager pager;
    private MarketsViewModel marketsViewModel;
    private MarketViewModelFactory marketViewModelFactory;
    private MarketPagerAdapter adapter;
    private TabLayout tabLayout;
    private ProgressBar pbMarket;

    public MarketFragment() {
        // Required empty public constructor
    }

    public static MarketFragment newInstance() {
        MarketFragment fragment = new MarketFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (savedInstanceState == null) {
//            spinnerposition = 0;
//        } else {
//            spinnerposition = savedInstanceState.getInt("spinnerposition");
//        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_market, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tabLayout = view.findViewById(R.id.tabs);
        pager = view.findViewById(R.id.pager);
        pbMarket = view.findViewById(R.id.pbMarket);
        this.marketViewModelFactory = ((MainActivity) getActivity()).marketViewModelFactory;

        marketsViewModel = ViewModelProviders.of(requireActivity(), marketViewModelFactory).get(MarketsViewModel.class);

        marketsViewModel.instrumentGroup.observe(getActivity(), instrumentGroups -> {
            if (adapter == null) {
                adapter = new MarketPagerAdapter(getChildFragmentManager(), instrumentGroups);
                if (pager != null) {
                    pager.setAdapter(adapter);
                    pager.setOffscreenPageLimit(5);
                    tabLayout.setupWithViewPager(pager);
                }
            }
        });
        marketsViewModel.start();
    }


    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getContext().getApplicationContext()).registerReceiver(ticksUpdatedReceiver, new IntentFilter(Constants.TICK_UPDATED));
        LocalBroadcastManager.getInstance(getContext().getApplicationContext()).registerReceiver(lastPriceFetchedReceiver, new IntentFilter(Constants.LAST_PRICE_FETCHED));
    }


    @Override
    public void onPause() {

        marketsViewModel.pause();

        LocalBroadcastManager.getInstance(getContext().getApplicationContext()).unregisterReceiver(ticksUpdatedReceiver);
        LocalBroadcastManager.getInstance(getContext().getApplicationContext()).unregisterReceiver(lastPriceFetchedReceiver);
        super.onPause();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //  outState.putInt("spinnerposition", spinnerposition);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMarketFragmentInteractionListener) {
            mListener = (OnMarketFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnMarketFragmentInteractionListener {
    }


    private BroadcastReceiver ticksUpdatedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            marketsViewModel.getUpdatedTicks();
//            int group_id = intent.getIntExtra(Constants.GROUP_ID, -1);
//            int symbol_id = intent.getIntExtra(Constants.SYMBOL_ID, -1);
//
//            if (symbol_id != -1)
//                marketAdapter.notifyItemChanged(keys.indexOf(symbol_id));
            Log.d("","");
            pbMarket.setVisibility(View.GONE);
        }
    };

    private BroadcastReceiver lastPriceFetchedReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            // marketAdapter.notifyDataSetChanged();
            Log.d("","");
        }
    };

}
