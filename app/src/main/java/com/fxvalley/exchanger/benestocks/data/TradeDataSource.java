package com.fxvalley.exchanger.benestocks.data;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PositionalDataSource;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Pair;

import com.fxvalley.exchanger.benestocks.data.model.DataLoadState;
import com.fxvalley.exchanger.benestocks.data.model.PagingResult;
import com.fxvalley.exchanger.benestocks.models.Trade;
import com.fxvalley.exchanger.benestocks.sockets.TradeSessionSocket;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

import static com.fxvalley.exchanger.benestocks.data.TradeRepository.CANCELLED;
import static com.fxvalley.exchanger.benestocks.data.TradeRepository.CLOSED;
import static com.fxvalley.exchanger.benestocks.data.TradeRepository.PENDING;
import static com.fxvalley.exchanger.benestocks.data.TradeRepositoryImpl.PAGE_SIZE;
import static com.fxvalley.exchanger.benestocks.data.TradeRepository.OPEN;


public class TradeDataSource extends PositionalDataSource<Trade> {

    private static final String TAG = "TradeDataSource";
    private TradeSessionSocket socket;

    public final MutableLiveData<DataLoadState> loadState;
    public final MutableLiveData<PagingResult> pageInfo;
    private int type;
    private int totalLoaded;
    private int page;

    public TradeDataSource(TradeSessionSocket socket, int type) {
        this.socket = socket;
        this.type = type;
        loadState = new MutableLiveData<>();
        pageInfo = new MutableLiveData<>();
    }


    private Observable<Pair<Integer, List<Trade>>> getData(int page) {
        switch (type) {
            case OPEN:
                return socket.getOpen(1, page, PAGE_SIZE);
            case CLOSED:
                return socket.getClosed(1, page, PAGE_SIZE);
            case PENDING:
                return socket.getPending(1, page, PAGE_SIZE);
            case CANCELLED:
                return socket.getCancelled(1, page, PAGE_SIZE);
        }
        return Observable.just(new Pair<>(0, new ArrayList<>()));
    }


    public void setType(int type) {
        this.type = type;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams params, @NonNull LoadInitialCallback<Trade> callback) {
        loadState.postValue(DataLoadState.LOADING);
        page = 0;
        totalLoaded = 0;
        Log.i(TAG, "loadInitial: page" + page);
        getData(page)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(integerListPair -> {
                    List<Trade> openPositions = integerListPair.second;
                    totalLoaded += openPositions.size();
                    pageInfo.setValue(new PagingResult(0, integerListPair.first, PAGE_SIZE, totalLoaded));
                    int totalCount = integerListPair.first;
                    int position = computeInitialLoadPosition(params, totalCount);

                    loadState.setValue(DataLoadState.LOADED);
                    callback.onResult(openPositions, position, totalCount);

                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable arg0) {
                        Log.e(TAG, "com.fxplayer.open call: ", arg0);
//                Log.d(TAG, reply);
                        loadState.setValue(DataLoadState.FAILED);
                    }
                });
    }

    @Override
    public void loadRange(@NonNull LoadRangeParams params, @NonNull LoadRangeCallback<Trade> callback) {
        loadState.postValue(DataLoadState.LOADING);
        page+=PAGE_SIZE;
        Log.i(TAG, "loadRange: page " + page);
        getData(page)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        integerListPair -> {
                            totalLoaded += integerListPair.second.size();
                            pageInfo.setValue(new PagingResult(params.startPosition, integerListPair.first, PAGE_SIZE, totalLoaded));
                            loadState.setValue(DataLoadState.LOADED);
                            callback.onResult(integerListPair.second);
                        }, new Action1<Throwable>() {
                            @Override
                            public void call(Throwable arg0) {
                                Log.e(TAG, "com.fxplayer.open call: ", arg0);
//                Log.d(TAG, reply);
                                loadState.setValue(DataLoadState.FAILED);
                            }
                        });


    }

}
