package com.fxvalley.exchanger.benestocks.sockets;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.fxvalley.exchanger.benestocks.MyApplication;
import com.fxvalley.exchanger.benestocks.models.ChartData;
import com.fxvalley.exchanger.benestocks.models.User;
import com.fxvalley.exchanger.benestocks.utilities.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import rx.functions.Action1;

import ws.wamp.jawampa.WampClient;
import ws.wamp.jawampa.WampClientBuilder;
import ws.wamp.jawampa.connection.IWampConnectorProvider;
import ws.wamp.jawampa.transport.netty.NettyWampClientConnectorProvider;

/**
 * Created by aloupas on 3/14/2017.
 */

public class ChartSocket {

    private final String TAG = "ChartSocket";

    private final String PRICE_URL = Constants.WEB_SOCKET_API_URL + ":23025/ws";
    public SocketUtilities.ClientStatus clientStatus = SocketUtilities.ClientStatus.DISCONNECTED;

    private WampClient client;
    private WampClientBuilder builder = new WampClientBuilder();
    private IWampConnectorProvider connectorProvider = new NettyWampClientConnectorProvider();

    private User user;
    private ChartSocketListener chartSocketListener;

    public List<ChartData> chartDatas = new CopyOnWriteArrayList<>();

    ReentrantLock lock = new ReentrantLock();


    public ChartSocket(User user) {
        buildClient();
        this.user = user;
    }

    private void buildClient() {

        try {
            builder.withUri(PRICE_URL)
                    .withRealm("fxplayer")
                    .withInfiniteReconnects()
                    .withCloseOnErrors(true)
                    .withReconnectInterval(5, TimeUnit.SECONDS)
                    .withConnectorProvider(connectorProvider);

            client = builder.build();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void connect(ChartSocketListener socketsConnectionListener) {

        this.chartSocketListener = socketsConnectionListener;
        // subscribe for session status changes
        client.statusChanged().subscribe(new Action1<WampClient.State>() {
            @Override
            public void call(WampClient.State t1) {
                SocketUtilities.ClientStatus clientStatus = null;
                Log.i(TAG, "Session status changed to " + t1);
                if (t1 instanceof WampClient.ConnectedState) {
                    Log.d(TAG, "CONNECTED");
                    clientStatus = SocketUtilities.ClientStatus.CONNECTED;

                } else if (t1 instanceof WampClient.DisconnectedState) {
                    Log.d(TAG, "DISCONNECTED");
                    clientStatus = SocketUtilities.ClientStatus.DISCONNECTED;
                } else if (t1 instanceof WampClient.ConnectingState) {
                    // Client starts connecting to the remote router
                    Log.d(TAG, "CONNECTING");
                    clientStatus = SocketUtilities.ClientStatus.CONNECTING;
                }
                if (ChartSocket.this.clientStatus != clientStatus) {
                    ChartSocket.this.clientStatus = clientStatus;
                    socketsConnectionListener.onChartStateChanged(clientStatus);
                }
            }
        });
        // request to open the connection with the server
        client.open();
    }

    public void disconnect() {
        client.close();
        client = null;
    }

    public void getChart(String msg, boolean isZoomedOut) {

        Log.d(TAG, msg);

        client.call("com.fxplayer.chart", JSONObject.class, msg).subscribe(new Action1<JSONObject>() {
            @Override
            public void call(JSONObject reply) {

                lock.lock();
                try {

                    JSONArray chartData = reply.getJSONArray("Messages").getJSONArray(2);

                    if (!isZoomedOut) {
                        chartDatas.clear();
                    } else {
                        Collections.reverse(chartDatas);
                    }

                    TimeZone timezone = GregorianCalendar.getInstance().getTimeZone();
                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.ENGLISH);

                    sdf.setTimeZone(timezone);

                    if (isZoomedOut) {
                        for (int i = chartData.length() - 1; i >= 0; i--) {
                            chartDatas.add(new ChartData(chartData.getJSONObject(i), sdf));
                        }
                    } else {
                        for (int i = 0; i < chartData.length(); i++) {
                            chartDatas.add(new ChartData(chartData.getJSONObject(i), sdf));
                        }
                    }

                    if (isZoomedOut) {
                        Collections.reverse(chartDatas);
                    }

                    Intent chartDataIntent = new Intent(Constants.CHART_UPDATED);
                    Log.i(TAG, "com.fxplayer.chart: " + chartDatas.size() + " " + "f: " + chartDatas.get(0).dayDateTime + " l: " + chartDatas.get(chartDatas.size() - 1).dayDateTime);
                    LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(chartDataIntent);


                } catch (JSONException e) {
                    Log.e(TAG, "com.fxplayer.chart: ", e);
                }
                lock.unlock();

            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable arg0) {
                arg0.printStackTrace();
                Log.e(TAG, "call: com.fxplayer.chart", arg0);
            }
        });
    }

    public void getInstruments(String filter) {

        client.call("com.fxplayer.instruments", JSONObject.class, filter).subscribe(new Action1<JSONObject>() {
            @Override
            public void call(JSONObject reply) {

                Log.d(TAG, "instruments : " + reply.toString());
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable arg0) {
                arg0.printStackTrace();
            }
        });
    }

}
