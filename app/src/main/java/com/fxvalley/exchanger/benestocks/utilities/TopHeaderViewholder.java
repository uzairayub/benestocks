package com.fxvalley.exchanger.benestocks.utilities;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.fxvalley.exchanger.benestocks.R;

class TopHeaderViewholder extends RecyclerView.ViewHolder {

        public TextView value;
        public TextView key;

        TopHeaderViewholder(View itemView) {
            super(itemView);

            key = (TextView) itemView.findViewById(R.id.headerKey);
            value = (TextView) itemView.findViewById(R.id.headerValue);

        }

    }