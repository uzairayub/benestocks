package com.fxvalley.exchanger.benestocks.sockets;

import com.fxvalley.exchanger.benestocks.models.ContractSpecs;

/**
 * Created by aloupas on 3/5/2017.
 */

public interface TradeSocketsConnectionListener {
    void onTradeSocketStateChanged(SocketUtilities.ClientStatus clientStatus);
    void onSessionFetched();
    void onGetAccount();
    void onAccountSwitched(boolean success);
    void onContractFetched(ContractSpecs contractSpecs);


}
