package com.fxvalley.exchanger.benestocks.models;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;

import net.cocooncreations.template.GeneralUtilities;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by aloupas on 3/15/2017.
 */

public class EventData {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-2017", Locale.US);

    public static final String LOW = "Low";
    public static final String HIGH = "High";
    public static final String MEDIUM = "Medium";

    public String title ="";
    public String country="";
    public String date="";
    public String time="";
    public String impact="";
    public String forecast="";
    public String previous="";

    public EventData(Node node) {

        NodeList childNodes = node.getChildNodes();

        for (int i = 0; i < childNodes.getLength(); i++) {
            if (childNodes.item(i).getNodeName().equals("title")) {
                title = childNodes.item(i).getTextContent();
            } else if (childNodes.item(i).getNodeName().equals("country")) {
                country = childNodes.item(i).getTextContent();
            } else if (childNodes.item(i).getNodeName().equals("date")) {
                date = childNodes.item(i).getTextContent();
            } else if (childNodes.item(i).getNodeName().equals("time")) {
                time = childNodes.item(i).getTextContent();
            } else if (childNodes.item(i).getNodeName().equals("impact")) {
                impact = childNodes.item(i).getTextContent();
            } else if (childNodes.item(i).getNodeName().equals("forecast")) {
                forecast = childNodes.item(i).getTextContent();
            } else if (childNodes.item(i).getNodeName().equals("previous")) {
                previous = childNodes.item(i).getTextContent();
            }

        }
    }

    public Spannable getImpactString() {

        Spannable spannable = new SpannableString("!!!");
        spannable.setSpan(new ForegroundColorSpan(0xaaffffff), 0, 3, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        if (impact.equals(HIGH)) {
            spannable.setSpan(new ForegroundColorSpan(0xffE75138), 0, 3, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else if (impact.equals(MEDIUM)) {
            spannable.setSpan(new ForegroundColorSpan(0xffE75138), 0, 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else if (impact.equals(LOW)) {
            spannable.setSpan(new ForegroundColorSpan(0xffE75138), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        return spannable;
    }

    @Override
    public String toString() {
        return GeneralUtilities.toString(this);
    }
}
