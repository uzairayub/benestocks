package com.fxvalley.exchanger.benestocks.utilities.validation;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

public class EditPositionTextValidator implements ITextValidator {

    private IValidation validation;
    private final TextView marketPrice;
    private final EditText stopProfitEditText;
    private final EditText stopLostEditText;
    private final EditText strategyEditText;
    private OnValidCallback onValidCallback;

    public EditPositionTextValidator(IValidation validation,
                                     @Nullable TextView marketPrice,
                                     EditText stopProfitEditText,
                                     EditText stopLostEditText, EditText strategyEditText, OnValidCallback onValidCallback) {
        this.validation = validation;
        this.marketPrice = marketPrice;
        this.stopProfitEditText = stopProfitEditText;
        this.stopLostEditText = stopLostEditText;
        this.strategyEditText = strategyEditText;
        this.onValidCallback = onValidCallback;
    }

    @Override
    public void check() {
        boolean isValid = true;
        String stopLostText = stopLostEditText.getText().toString();
        String stopProfitText = stopProfitEditText.getText().toString();

        if (!TextUtils.isEmpty(stopLostText) && !TextUtils.isEmpty(stopProfitText)) {
            double stopLost = getDouble(stopLostText);
            double stopProfit = getDouble(stopProfitText);
            double marketPriceValue = 0;
            if (marketPrice != null) {
                marketPriceValue = getDouble(marketPrice.getText().toString());
            }

            if (!validation.isStopLostValid(stopLost, marketPriceValue)) {
                isValid = false;
                stopLostEditText.setError(validation.getStopLostError());
            }

            if (!validation.isStopProfitValid(stopProfit, marketPriceValue)) {
                isValid = false;
                stopProfitEditText.setError(validation.getStopProfitError());
            }


        }
        onValidCallback.onValidated(isValid);
    }

    public static double getDouble(String source) {
        if (TextUtils.isEmpty(source)) return 0;
        return Double.parseDouble(source.replace(',', '.'));
    }

    public interface OnValidCallback {
        void onValidated(boolean isValid);
    }

    private static final String TAG = "EditPositionTextValidator";
}
