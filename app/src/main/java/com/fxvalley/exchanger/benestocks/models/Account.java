package com.fxvalley.exchanger.benestocks.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by aloupas on 3/27/2017.
 */

public class Account implements Serializable{

    // Name
    // I
/*    E (CURRENT ACCOUNT BALANCE->C)  C+CPL+ total from open trades
    UC (->UM)
    OPL (->P&L)
    SO (->SL)*/

    public int id;
    public String name;
    public Double E;
    public Double UC;
    public Double OPL;
    public Double SO;
    public boolean isActive;

    public Double CPL;
    public Double C;

    public String currency;

    public int TAT;
    public TraderAccountLevel TALD;


    public Account(JSONObject jsonObject) {

        id = jsonObject.optInt("I");
        try {
            name = jsonObject.getString("Name");
        } catch (JSONException ex) {
            try {
                name = jsonObject.getJSONObject("TATD").optString("Name");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        TAT = jsonObject.optInt("TAT");
        E = jsonObject.optDouble("E");
        UC = jsonObject.optDouble("UC");
        OPL = jsonObject.optDouble("OPL");
        SO = jsonObject.optDouble("SO");
        isActive = jsonObject.optBoolean("A");
        CPL = jsonObject.optDouble("CPL");
        C = jsonObject.optDouble("C");
        try {
            TALD = new TraderAccountLevel(jsonObject.getJSONObject("TALD"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try{
            currency = jsonObject.getJSONObject("Cur").optString("Name");
        }catch (JSONException e){
            e.printStackTrace();
        }

    }

}
