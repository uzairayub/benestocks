package com.fxvalley.exchanger.benestocks.data.model;


public enum DataLoadState {
    LOADING,
    LOADED,
    FAILED
}
