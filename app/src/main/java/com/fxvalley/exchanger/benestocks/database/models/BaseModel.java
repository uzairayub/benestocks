package com.fxvalley.exchanger.benestocks.database.models;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;
import org.w3c.dom.Document;

import java.lang.reflect.Field;

import static com.fxvalley.exchanger.benestocks.database.models.BaseModel.Datatype.BLOB;
import static com.fxvalley.exchanger.benestocks.database.models.BaseModel.Datatype.INTEGER;
import static com.fxvalley.exchanger.benestocks.database.models.BaseModel.Datatype.LONG;
import static com.fxvalley.exchanger.benestocks.database.models.BaseModel.Datatype.REAL;
import static com.fxvalley.exchanger.benestocks.database.models.BaseModel.Datatype.TEXT;

/**
 * Created by aloupas on 2/5/2017.
 */

public abstract class BaseModel {

    public enum Datatype{
        TEXT("TEXT"), INTEGER("INTEGER"), BLOB("BLOB"), REAL("REAL"), NULL("TEXT"), LONG("INTEGER");

        public final String value;

        Datatype(final String value) {
            this.value = value;
        }
    }


    private static String TAG = "BaseModel";

    protected ContentValues getContentValues() throws IllegalAccessException {

        ContentValues contentValues = new ContentValues();

        for (Field field : getClass().getDeclaredFields()) {

            if (field.isAnnotationPresent(Database.class)) {

                Database annotation = field.getAnnotation(Database.class);

                if (annotation.dataType() == TEXT) {
                    contentValues.put(field.getName(), (String)field.get(this));
                } else if (annotation.dataType() == INTEGER) {
                    contentValues.put(field.getName(), (int) field.get(this));
                } else if (annotation.dataType() == BLOB) {
                    contentValues.put(field.getName(), (byte[]) field.get(this));
                } else if (annotation.dataType() == REAL) {
                    contentValues.put(field.getName(), (float) field.get(this));
                } else if (annotation.dataType() == LONG) {
                    contentValues.put(field.getName(), (long) field.get(this));
                }
            }
        }

        return contentValues;
    }

    protected abstract void loadJson(JSONObject jsonObject);

    protected abstract void loadXml(Document document);

    protected abstract void loadStringArray(String[] stringArray);
//    protected abstract void loadCursor(Cursor cursor);

    protected void loadCursor(Cursor cursor) throws IllegalAccessException {

        for (Field field : getClass().getDeclaredFields()) {

            if (field.isAnnotationPresent(Database.class)) {

                Database annotation = field.getAnnotation(Database.class);

                if (annotation.dataType() == TEXT) {
                    if (cursor.getColumnIndex(field.getName()) != -1)
                        field.set(this, cursor.getString(cursor.getColumnIndex(field.getName())));
                } else if (annotation.dataType() ==INTEGER) {
                    if (cursor.getColumnIndex(field.getName()) != -1)
                        field.set(this, cursor.getInt(cursor.getColumnIndex(field.getName())));
                } else if (annotation.dataType() == BLOB) {
                    if (cursor.getColumnIndex(field.getName()) != -1)
                        field.set(this, cursor.getBlob(cursor.getColumnIndex(field.getName())));
                } else if (annotation.dataType() == REAL) {
                    if (cursor.getColumnIndex(field.getName()) != -1)
                        field.set(this, cursor.getFloat(cursor.getColumnIndex(field.getName())));
                } else if (annotation.dataType() == LONG) {
                    if (cursor.getColumnIndex(field.getName()) != -1)
                        field.set(this, cursor.getLong(cursor.getColumnIndex(field.getName())));
                }

            }
        }
    }

    public static <T extends BaseModel> T from(JSONObject jsonObject, Class<T> modelClass) {

        try {
            T baseModel = modelClass.newInstance();
            baseModel.loadJson(jsonObject);
            return baseModel;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static <T extends BaseModel> T from(Document document, Class<T> modelClass) {

        try {
            T baseModel = modelClass.newInstance();
            baseModel.loadXml(document);
            return baseModel;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static <T extends BaseModel> T from(String[] stringArray, Class<T> modelClass) {

        try {
            T baseModel = modelClass.newInstance();
            baseModel.loadStringArray(stringArray);
            return baseModel;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static <T extends BaseModel> T from(Cursor cursor, Class<T> modelClass) {

        if (cursor != null) {
            try {
                T baseModel = modelClass.newInstance();
                baseModel.loadCursor(cursor);
                return baseModel;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return null;
    }


}
