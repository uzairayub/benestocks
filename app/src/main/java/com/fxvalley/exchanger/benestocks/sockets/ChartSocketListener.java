package com.fxvalley.exchanger.benestocks.sockets;

/**
 * Created by aloupas on 3/15/2017.
 */

public interface ChartSocketListener {

    void onChartStateChanged(SocketUtilities.ClientStatus clientStatus);

}
