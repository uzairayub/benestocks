package com.fxvalley.exchanger.benestocks.models;

import net.cocooncreations.template.GeneralUtilities;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by aloupas on 3/8/2017.
 */

public class Contract implements Serializable {
    public Integer id;
    public String name;
    public String description;
    public Integer minimum;
    public Integer maximum;
    public String expiration;
    public Integer margin;
    public Integer stop;
    public Double commission;
    public String unit;
    public Integer value;
    public Integer cost;
    public String patternMatch;
    public Integer lot;

    public Contract(JSONObject jsonObject) {
        id = jsonObject.optInt("Id");
        name = jsonObject.optString("Name");
        description = jsonObject.optString("Description");
        minimum = jsonObject.optInt("Minimum");
        maximum = jsonObject.optInt("Maximum");
        expiration = jsonObject.optString("Expiration");
        margin = jsonObject.optInt("Margin");
        stop = jsonObject.optInt("Stop");
        commission = jsonObject.optDouble("Commission");
        unit = jsonObject.optString("Unit");
        value = jsonObject.optInt("Value");
        cost = jsonObject.optInt("Cost");
        patternMatch = jsonObject.optString("PatternMatch");
        lot = jsonObject.optInt("Lot");

    }

    @Override
    public String toString() {
        return GeneralUtilities.toString(this);
    }
}
