package com.fxvalley.exchanger.benestocks.markets;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.fxvalley.exchanger.benestocks.models.InstrumentGroup;

import java.util.LinkedList;
import java.util.Map;

public class MarketPagerAdapter extends FragmentPagerAdapter {

    private static final String TAG = "MarketPagerAdapter";

    private LinkedList<Integer> instrumentsIds;
    private Map<Integer, InstrumentGroup> instrumentGroups;

    public MarketPagerAdapter(FragmentManager fm, Map<Integer, InstrumentGroup> instrumentGroups) {
        super(fm);
        this.instrumentGroups = instrumentGroups;
        instrumentsIds = new LinkedList<>();
        for (InstrumentGroup instrumentGroup : instrumentGroups.values()) {
            instrumentsIds.add(instrumentGroup.id);
            Log.d(TAG, "adding id: " +instrumentGroup.name );
        }


    }

    @Override
    public Fragment getItem(int position) {
        return MarketPage.newInstance(position, instrumentsIds.get(position));
    }

    @Override
    public int getCount() {
        return instrumentsIds.size();
    }


    @Override
    public CharSequence getPageTitle(final int position) {
        return instrumentGroups.get(position+1).name;
    }

}
