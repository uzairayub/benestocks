package com.fxvalley.exchanger.benestocks.models;

public interface TradeStatus {
    int OPEN = 1;
    int CLOSED = 2;
    int PENDING = 4;
    int INVALID = 5;
    int COLLATERAL = 6;
    int CANCEL = 7;

    static String getStatusText(int tradeStatus){
        String messageText = "";
        switch (tradeStatus){
            case OPEN:
                messageText = "Trade Opened";
                break;
            case CLOSED:
                messageText = "Trade Closed";
                break;
            case PENDING:
                messageText = "Trade Updated";
                break;
            case INVALID:
                messageText = "Invalid Trade";
                break;
            case COLLATERAL:
                messageText = "Margin Needed";
                break;
            case CANCEL:
                messageText = "Trade Cancelled";
                break;
        }
        return messageText;
    }
}

