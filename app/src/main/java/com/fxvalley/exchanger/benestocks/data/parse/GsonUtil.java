package com.fxvalley.exchanger.benestocks.data.parse;

import com.google.gson.GsonBuilder;

public class GsonUtil {

    private GsonUtil() {
    }

    public static <T> T gsonFromJson(String json, Class<T> classOfT) {
        return
                new GsonBuilder()
                        .excludeFieldsWithoutExposeAnnotation()
                        .create()
                        .fromJson(json, classOfT);
    }

    public static String gsonToJson(Object src, Class tClass) {
        return
                new GsonBuilder()
                        .excludeFieldsWithoutExposeAnnotation()
                        .create()
                        .toJson(src, tClass);
    }

}
