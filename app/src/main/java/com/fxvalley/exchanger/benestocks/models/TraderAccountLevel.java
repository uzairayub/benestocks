package com.fxvalley.exchanger.benestocks.models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by user on 13/07/2017.
 */

public class TraderAccountLevel implements Serializable{

    public double Increment;
    public double Min;
    public double Max;

    public TraderAccountLevel(JSONObject jsonObject){
        Increment = jsonObject.optDouble("Increment");
        Min = jsonObject.optDouble("Min");
        Max = jsonObject.optDouble("Max");
    }

}
