package com.fxvalley.exchanger.benestocks.utilities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.fxvalley.exchanger.benestocks.MyApplication;
import com.fxvalley.exchanger.benestocks.R;

/**
 * Created by user on 12/09/2017.
 */

public class ChartSelector {

    public void setSelectedView() {

        fifteenMinsButton.setBackgroundResource(R.drawable.grey_border);
        thirtyMinsButton.setBackgroundResource(R.drawable.grey_border);
        oneHourMinsButton.setBackgroundResource(R.drawable.grey_border);
        eightHoursMinsButton.setBackgroundResource(R.drawable.grey_border);
        oneDayButton.setBackgroundResource(R.drawable.grey_border);

        switch (chartSelectorState){
            case FIFTEEN_MIN:
                fifteenMinsButton.setBackgroundResource(R.drawable.horizontal_white_divider);
                break;
            case THIRTY_MINS:
                thirtyMinsButton.setBackgroundResource(R.drawable.horizontal_white_divider);
                break;
            case ONE_HOUR:
                oneHourMinsButton.setBackgroundResource(R.drawable.horizontal_white_divider);
                break;
            case EIGHT_HOURS:
                eightHoursMinsButton.setBackgroundResource(R.drawable.horizontal_white_divider);
                break;
            case ONE_DAY:
                oneDayButton.setBackgroundResource(R.drawable.horizontal_white_divider);
                break;

        }

    }

    public enum ChartSelectorState {FIFTEEN_MIN, THIRTY_MINS, ONE_HOUR, EIGHT_HOURS, ONE_DAY}


    private View fifteenMinsButton;
    private View thirtyMinsButton;
    private View oneHourMinsButton;
    private View eightHoursMinsButton;
    private View oneDayButton;

    public ChartSelectorState chartSelectorState;

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switchSelector(view);
            sendChartBroadcast(false);
        }
    };

    private int getSelectedViewId(ChartSelectorState state) {

        switch (state) {
            case FIFTEEN_MIN:
                return R.id.fifteen_mins;
            case THIRTY_MINS:
                return R.id.thirty_mins;

            case ONE_HOUR:
                return R.id.one_hour;

            case EIGHT_HOURS:
                return R.id.eight_hours;

            case ONE_DAY:
                return R.id.one_day;

            default:
                return R.id.fifteen_mins;
        }
    }

    public void sendChartBroadcast(boolean isZoomedOut) {
        Intent intent = new Intent(Constants.CHANGE_CHART);
        intent.putExtra("chartSelectorState", chartSelectorState);
        intent.putExtra("isZoomedOut", isZoomedOut);
        LocalBroadcastManager.getInstance(MyApplication.getAppContext()).sendBroadcast(intent);
    }

    public void onChartSocketReady() {

        fifteenMinsButton.setOnClickListener(onClickListener);
        thirtyMinsButton.setOnClickListener(onClickListener);
        oneHourMinsButton.setOnClickListener(onClickListener);
        eightHoursMinsButton.setOnClickListener(onClickListener);
        oneDayButton.setOnClickListener(onClickListener);

        sendChartBroadcast(false);

    }

    public void onChartSocketDisconnect() {
        fifteenMinsButton.setOnClickListener(null);
        thirtyMinsButton.setOnClickListener(null);
        oneHourMinsButton.setOnClickListener(null);
        eightHoursMinsButton.setOnClickListener(null);
        oneDayButton.setOnClickListener(null);
    }

    public void onCreate(AppCompatActivity mActivity, Bundle savedInstance) {

        fifteenMinsButton = mActivity.findViewById(R.id.fifteen_mins);
        thirtyMinsButton = mActivity.findViewById(R.id.thirty_mins);
        oneHourMinsButton = mActivity.findViewById(R.id.one_hour);
        eightHoursMinsButton = mActivity.findViewById(R.id.eight_hours);
        oneDayButton = mActivity.findViewById(R.id.one_day);

        if (savedInstance == null) {
            chartSelectorState = ChartSelectorState.FIFTEEN_MIN;
        } else {
            chartSelectorState = (ChartSelectorState) savedInstance.getSerializable("chartSelectorState");
        }

        setSelectedView();
    }

    public void onSavedInstanceState(Bundle outState) {
        outState.putSerializable("chartSelectorState", chartSelectorState);
    }


    public void switchSelector(View view) {
        switch (view.getId()) {
            case R.id.fifteen_mins:
                chartSelectorState = ChartSelectorState.FIFTEEN_MIN;

                break;
            case R.id.thirty_mins:
                chartSelectorState = ChartSelectorState.THIRTY_MINS;

                break;
            case R.id.one_hour:
                chartSelectorState = ChartSelectorState.ONE_HOUR;

                break;
            case R.id.eight_hours:
                chartSelectorState = ChartSelectorState.EIGHT_HOURS;

                break;
            case R.id.one_day:
                chartSelectorState = ChartSelectorState.ONE_DAY;

                break;
        }

        setSelectedView();


    }

}
