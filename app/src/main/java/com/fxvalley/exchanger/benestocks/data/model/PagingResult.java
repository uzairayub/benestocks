package com.fxvalley.exchanger.benestocks.data.model;

public class PagingResult {
    private final int page;
    private final int totalResults;
    private final int resultsPerPage;
    private final int totalLoaded;

    public PagingResult(int page, int totalResults, int resultsPerPage, int totalLoaded) {
        this.page = page;
        this.totalResults = totalResults;
        this.resultsPerPage = resultsPerPage;
        this.totalLoaded = totalLoaded;
    }

    public PagingResult() {
        this.page = 0;
        this.resultsPerPage = 1;
        this.totalResults = 0;
        this.totalLoaded = 0;
    }

    public boolean isLastPage(){
        return getLastPage() == page;

    }

    private double getLastPage() {
        double last = Math.ceil(totalResults / resultsPerPage);
        return last;
    }


    public int getPage() {
        return page;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public int getTotalLoaded() {
        return totalLoaded;
    }

    public int getResultsPerPage() {
        return resultsPerPage;
    }
}
