package com.fxvalley.exchanger.benestocks.utilities.validation;

public interface IValidation {
    boolean isStopLostValid(double stopLost, double marketPriceValue);

    boolean isStopProfitValid(double stopProfit, double marketPriceValue);

    CharSequence getStopLostError();

    CharSequence getStopProfitError();
}
