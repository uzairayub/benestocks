package com.fxvalley.exchanger.benestocks.utilities.validation;

public class BuyValidation implements IValidation {

    private String stopLostError = "";
    private String stopProfitError = "";

    @Override
    public boolean isStopLostValid(double stopLost, double marketPriceValue) {
        boolean stopListAboveZero = stopLost > 0;
        if(!stopListAboveZero){
            stopLostError = "Stop lost should be above zero";
        }
        boolean stopLostLessThanMarket = stopLost < marketPriceValue;
        if(!stopLostLessThanMarket){
            stopLostError = "Stop lost should be less than market price";
        }

        if(!stopListAboveZero && !stopLostLessThanMarket){
            stopLostError = "Stop lost should be less than market price and above zero";
        }
        return stopListAboveZero && stopLostLessThanMarket;
    }

    @Override
    public boolean isStopProfitValid(double stopProfit, double marketPriceValue) {
        boolean stopProfitAboveZero = stopProfit > 0;
        if(!stopProfitAboveZero){
            stopProfitError = "Stop profit should be above zero";
        }
        boolean stopProfitHigherThanMarket = stopProfit > marketPriceValue;
        if(!stopProfitHigherThanMarket){
            stopProfitError = "Stop profit should be higher than market price";
        }

        if(!stopProfitAboveZero && !stopProfitHigherThanMarket){
            stopProfitError = "Stop profit should be higher than market price and above zero";
        }
        return stopProfitAboveZero && stopProfitHigherThanMarket;
    }

    @Override
    public CharSequence getStopLostError() {
        return stopLostError;
    }

    @Override
    public CharSequence getStopProfitError() {
        return stopProfitError;
    }
}
