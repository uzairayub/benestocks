package com.fxvalley.exchanger.benestocks.sockets;

import com.fxvalley.exchanger.benestocks.models.InstrumentGroup;

import java.util.Comparator;

/**
 * Created by aloupas on 3/8/2017.
 */

public class InstrumentCategoriesComparator implements Comparator<InstrumentGroup> {

    @Override
    public int compare(InstrumentGroup instrumentGroup1, InstrumentGroup instrumentGroup2) {

        return instrumentGroup1.order.compareTo(instrumentGroup2.order);


    }
}
