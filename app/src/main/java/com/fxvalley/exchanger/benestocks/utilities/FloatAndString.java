package com.fxvalley.exchanger.benestocks.utilities;

import com.fxvalley.exchanger.benestocks.database.models.BaseModel;
import com.fxvalley.exchanger.benestocks.database.models.Database;

public class FloatAndString {
        public String stringValue;
        @Database(dataType = BaseModel.Datatype.REAL)
        public float floatValue;
    }
