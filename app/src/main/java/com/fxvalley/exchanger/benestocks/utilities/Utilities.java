package com.fxvalley.exchanger.benestocks.utilities;

import android.content.Context;
import android.util.Log;

import com.fxvalley.exchanger.benestocks.models.Instrument;
import com.fxvalley.exchanger.benestocks.models.InstrumentGroup;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


/**
 * Created by aloupas on 3/4/2017.
 */

public class Utilities {

    public static String TAG = "Utilities";

    public static void logoutUser(Context context) {
//
//        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE);
//        sharedPreferences.edit();

    }

    public static void printLongText(String str) {
        if (str.length() > 1000) {
            Log.i(TAG, str.substring(0, 1000));
            printLongText(str.substring(1000));
        } else
            Log.i(TAG, str);

    }

    public static  Map<Integer, InstrumentGroup> sortByValue(Map<Integer, InstrumentGroup> unsortMap) {

        List<Map.Entry<Integer, InstrumentGroup>> list =
                new LinkedList<Map.Entry<Integer, InstrumentGroup>>(unsortMap.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<Integer, InstrumentGroup>>() {
            public int compare(Map.Entry<Integer, InstrumentGroup> o1, Map.Entry<Integer, InstrumentGroup> o2) {
                return (o1.getValue().order).compareTo(o2.getValue().order);
            }
        });

        Map<Integer, InstrumentGroup> result = new LinkedHashMap<Integer, InstrumentGroup>();
        for (Map.Entry<Integer, InstrumentGroup> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;

    }

    public static  Map<Integer, Instrument> sortByValueInstrument(Map<Integer, Instrument> unsortMap) {

        List<Map.Entry<Integer, Instrument>> list =
                new LinkedList<Map.Entry<Integer, Instrument>>(unsortMap.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<Integer, Instrument>>() {
            public int compare(Map.Entry<Integer, Instrument> o1, Map.Entry<Integer, Instrument> o2) {
                return (o1.getValue().order).compareTo(o2.getValue().order);
            }
        });

        Map<Integer, Instrument> result = new LinkedHashMap<Integer, Instrument>();
        for (Map.Entry<Integer, Instrument> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;

    }

}
