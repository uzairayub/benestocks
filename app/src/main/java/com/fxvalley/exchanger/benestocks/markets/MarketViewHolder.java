package com.fxvalley.exchanger.benestocks.markets;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.fxvalley.exchanger.benestocks.R;
import com.fxvalley.exchanger.benestocks.utilities.CustomSpinner;

public class MarketViewHolder extends RecyclerView.ViewHolder {
//
//        private static final int MARKET = 0;
//        private static final int LIMIT = 1;
//        private static final int STOP = 2;
//
//        private static final String[] MARKET_CHOICES = new String[]{"MARKET", "LIMIT", "STOP"};
//
//        public static boolean spinnerIsOpen;

    public final TextView marketRowSellValueLastDigit;
    public final TextView marketRowBuyValueLastDigit;
    public View marketRowButtonSell;
    public View marketRowButtonBuy;
    public TextView currencyPair;
    public TextView updatedAt;
    public TextView marketRowOpen;
    public TextView marketRowHigh;
    public TextView marketRowLow;
    public TextView marketRowClose;

    public TextView marketRowSellValue;
    public TextView marketRowSellValueTwoDigits;

    public TextView marketRowBuyValue;
    public TextView marketRowBuyValueTwoDigits;

    public TextView marketRowSpread;

    public TextView rowMarketLeftchooser;
    public CustomSpinner rowMarketRightSpinner;
    private View root;


    public MarketViewHolder(View itemView) {
        super(itemView);
        root = itemView;
        marketRowButtonBuy = itemView.findViewById(R.id.marketRowButtonBuy);
        marketRowButtonSell = itemView.findViewById(R.id.marketRowButtonSell);

        marketRowSellValueLastDigit = (TextView) itemView.findViewById(R.id.marketRowSellValueLastDigit);
        marketRowBuyValueLastDigit = (TextView) itemView.findViewById(R.id.marketRowBuyValueLastDigit);

        currencyPair = (TextView) itemView.findViewById(R.id.marketCurrencyPair);
        updatedAt = (TextView) itemView.findViewById(R.id.marketUpdatedAt);
        marketRowOpen = (TextView) itemView.findViewById(R.id.marketRowOpen);
        marketRowHigh = (TextView) itemView.findViewById(R.id.marketRowHigh);
        marketRowLow = (TextView) itemView.findViewById(R.id.marketRowLow);
        marketRowClose = (TextView) itemView.findViewById(R.id.marketRowClose);
        marketRowSellValue = (TextView) itemView.findViewById(R.id.marketRowSellValue);
        marketRowSellValueTwoDigits = (TextView) itemView.findViewById(R.id.marketRowSellValueTwoDigits);
        marketRowBuyValue = (TextView) itemView.findViewById(R.id.marketRowBuyValue);
        marketRowBuyValueTwoDigits = (TextView) itemView.findViewById(R.id.marketRowBuyValueTwoDigits);
        marketRowSpread = (TextView) itemView.findViewById(R.id.marketRowSpread);


        rowMarketLeftchooser = itemView.findViewById(R.id.rowMarketLeftChooser);
        rowMarketLeftchooser.setVisibility(View.GONE);
//            rowMarketLeftchooser.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    MaterialDialog show = new MaterialDialog.Builder(view.getContext())
//                            .customView(R.layout.number_picker, false)
//                            .title(R.string.choose_type).dismissListener(new DialogInterface.OnDismissListener() {
//                                @Override
//                                public void onDismiss(DialogInterface dialogInterface) {
//                                    MaterialDialog dialog = (MaterialDialog) dialogInterface;
//                                    rowMarketLeftchooser.setText(((EditText) dialog.findViewById(R.id.number_edittext)).getText());
//                                }
//                            })
//                            .show();
//                    ((EditText) show.findViewById(R.id.number_edittext)).setText(rowMarketLeftchooser.getText());
//                }
//            });

//        itemView.findViewById(R.id.spinnerBackgroundView).setVisibility(View.GONE);
        rowMarketRightSpinner = itemView.findViewById(R.id.rowMarketRightSpinner);
        rowMarketRightSpinner.setVisibility(View.GONE);

//            rowMarketRightSpinner.setAdapter(new ArrayAdapter<>(itemView.getContext(), R.layout.row_market_spinner_textview, MARKET_CHOICES));
//            rowMarketRightSpinner.setSpinnerEventsListener(new CustomSpinner.OnSpinnerEventsListener() {
//                @Override
//                public void onSpinnerOpened(Spinner spinner) {
//                    spinnerIsOpen = true;
//                    Log.d(TAG, "ON SPINNER OPEN");
//                }
//
//                @Override
//                public void onSpinnerClosed(Spinner spinner) {
//                    spinnerIsOpen = false;
//                    Log.d(TAG, "ON SPINNER CLOSED");
//                }
//            });
    }

    public View getRoot() {
        return root;
    }
}
