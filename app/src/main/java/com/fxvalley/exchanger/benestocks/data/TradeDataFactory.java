package com.fxvalley.exchanger.benestocks.data;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;

import com.fxvalley.exchanger.benestocks.models.Trade;


public class TradeDataFactory extends DataSource.Factory<Integer, Trade> {

    public MutableLiveData<TradeDataSource> datasourceLiveData = new MutableLiveData<>();
    private TradeDataSource dataSource;

    public TradeDataFactory(TradeDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public DataSource<Integer, Trade> create() {
        datasourceLiveData.postValue(dataSource);
        return dataSource;
    }
}
