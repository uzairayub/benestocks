package com.fxvalley.exchanger.benestocks.models;

import com.fxvalley.exchanger.benestocks.data.TradeRepository;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by aloupas on 3/16/2017.
 */

public class Trade implements Serializable {

    private static final SimpleDateFormat fromFormat1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
    private static final SimpleDateFormat fromFormat2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
    private static final SimpleDateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US);
    private static final DecimalFormat decimalFormat = new DecimalFormat("0.00");

    public Instrument instrument;
    public double volume;
    public Double open;
    public Double close;
    public int type;
    public String openTime;
    public String closeTime;
    public Double pl;
    public Double pendingCancelled;
    public String pendingCancelledTime;
    public Double com;
    public Double _int;
    public String ticket;
    public Double col;
    public Double stopProfit;
    public Double stopLost;
    public String label;
    public int id;
    public String tt;
    public Instrument profit;
//    public Double ask;
//    public Double bid;

    public Trade(JSONObject jsonObject) throws JSONException {

        instrument = new Instrument(jsonObject.getJSONObject("Instrument"));
        volume = jsonObject.optDouble("VU");
        open = jsonObject.optDouble("EP");
        close = jsonObject.optDouble("CEP");
        type = jsonObject.optInt("S");
        id = jsonObject.optInt("I");
        try {
            openTime = toFormat(fromFormat1(jsonObject.optString("EDT")));
        } catch (ParseException e) {
            try {
                openTime = toFormat(fromFormat2(jsonObject.optString("EDT")));
            } catch (ParseException e1) {
                e.printStackTrace();
            }
        }
        try {
            closeTime = toFormat(fromFormat1(jsonObject.optString("CEDT")));
        } catch (ParseException e) {
            try {
                closeTime = toFormat(fromFormat2(jsonObject.optString("CEDT")));
            } catch (ParseException e1) {
                e.printStackTrace();
            }
        }

        try {
            pendingCancelledTime = toFormat(fromFormat1(jsonObject.optString("RDT")));
        } catch (ParseException e) {
            try {
                pendingCancelledTime = toFormat(fromFormat2(jsonObject.optString("RDT")));
            } catch (ParseException e1) {
                e.printStackTrace();
            }
        }

        pl = jsonObject.optDouble("PL");

        pendingCancelled = jsonObject.optDouble("RP");

        _int = jsonObject.optDouble("Int");
        com = jsonObject.optDouble("Com");
        ticket = jsonObject.optString("Ticket");
        col = jsonObject.optDouble("Col");
        stopLost = jsonObject.optDouble("SL");
        stopProfit = jsonObject.optDouble("SP");
        label = jsonObject.optString("LBL");
        tt = jsonObject.optString("TT");

        // AN INE BUY PERNW BID PRICE (CURRENT PRICE)
        // AN INE SELL PERNW ASK PRICE (CURRENT PRICE)

        // OPEN: (CURRENT PRICE - OPEN PRICE ) * VOLUME + Int + Com   (UPDATED)

        // CLOSE: PL + Int + Com

        // OPEN OPEN PRICE, OPEN TIME, CURRENT PRICE
        // CLOSE: OPEN PRICE, OPEN TIME, CLOSE TIME, CLOSE PRICE
        // PENDING: ORA KAI TIMI RDT, RP
        // CANCELLED: ORA KAI TIMI RDT, RP

        //        "RDT": "2017-03-10T13:10:41.463", // PENDING // CANCELLED
        //        RP //PENDING // CANCELLED


    }

    private synchronized static Date fromFormat1(String date) throws ParseException {

        return fromFormat1.parse(date);
    }

    private synchronized static Date fromFormat2(String date) throws ParseException {

        return fromFormat2.parse(date);
    }


    private synchronized static String toFormat(Date date) {
        return toFormat.format(date);
    }

    public synchronized static String format(Double d) {
        return decimalFormat.format(d);
    }

    public Double calculatePL(int tradeType){
        return calculatePL(tradeType, 1);
    }

    public Double calculatePL(int tradeType, double rate) {

        // BUY
        if (type == 1) {

            if (tradeType == TradeRepository.OPEN) {
                if (instrument.instrumentLiveData == null)
                    return pl + _int + com;
                else
                    return (((instrument.instrumentLiveData.bid - open) * volume) * rate + _int + com) ;
            } else if (tradeType == TradeRepository.CLOSED) {
                return pl + _int + com;
            } else {
                return 0.0;
            }

        } else {
            if (tradeType == TradeRepository.OPEN) {
                if (instrument.instrumentLiveData == null)
                    return pl + _int + com;
                else
                    return (((open - instrument.instrumentLiveData.ask) * volume) * rate + _int + com) ;
            } else if (tradeType == TradeRepository.CLOSED) {
                return pl + _int + com;
            } else {
                return 0.0;
            }
        }
    }


}
