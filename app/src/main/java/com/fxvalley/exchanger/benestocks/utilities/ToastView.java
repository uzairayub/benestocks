package com.fxvalley.exchanger.benestocks.utilities;

import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

public class ToastView implements IView {

    private Context context;
    private Handler handler;

    public ToastView(Context context, Handler handler) {
        this.context = context;
        this.handler = handler;
    }

    @Override
    public void showMessage(String message) {
        handler.post(() -> Toast.makeText(context, message, Toast.LENGTH_SHORT).show());

    }
}
