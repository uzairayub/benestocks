package com.fxvalley.exchanger.benestocks.markets;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fxvalley.exchanger.benestocks.R;
import com.fxvalley.exchanger.benestocks.activities.BuySellActivity;
import com.fxvalley.exchanger.benestocks.activities.MainActivity;
import com.fxvalley.exchanger.benestocks.models.Instrument;
import com.fxvalley.exchanger.benestocks.models.InstrumentGroup;
import com.fxvalley.exchanger.benestocks.trades.TradesVMFactory;
import com.fxvalley.exchanger.benestocks.trades.TradesViewModel;
import com.fxvalley.exchanger.benestocks.utilities.Constants;
import com.fxvalley.exchanger.benestocks.utilities.SPUtils;

import java.util.ArrayList;
import java.util.List;

import static android.support.v7.widget.RecyclerView.VERTICAL;

public class MarketPage extends Fragment implements MarketAdapter.MarketCallback {

    public static final String KEY_POSITION = "KEY_POSITION";
    public static final String KEY_INSTR_ID = "KEY_INSTR_ID";

    private static final String TAG = "MarketPage";
    private RecyclerView recyclerView;
    private MarketAdapter marketAdapter;

    private MarketsViewModel marketsViewModel;
    private int position;

    List<Integer> keys;
    private int instrID;

    private TradesVMFactory tradesVMFactory;
    private TradesViewModel tradesVM;


    private boolean isViewShown = false, isLoading;




    public static MarketPage newInstance(int position, Integer instrumentID) {
        Bundle args = new Bundle();
        args.putInt(KEY_POSITION, position);
        args.putInt(KEY_INSTR_ID, instrumentID);
        MarketPage fragment = new MarketPage();
        fragment.setArguments(args);
        return fragment;
    }

    private View view;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        FragmentActivity activity = getActivity();
        if (activity != null && activity instanceof MainActivity) {
            marketsViewModel = ViewModelProviders.of(activity, ((MainActivity) activity).marketViewModelFactory)
                    .get(MarketsViewModel.class);


            this.tradesVMFactory = ((MainActivity) getActivity()).tradesVMFactory;
            tradesVM = ViewModelProviders.of(activity, tradesVMFactory).get(TradesViewModel.class);
            tradesVM.tradesOpen.observe(getActivity(), new Observer<Boolean>() {
                @Override
                public void onChanged(@Nullable Boolean isOpen) {
                    if (marketAdapter!=null) {
                        marketAdapter.setIsOpen(isOpen);
                    }
                }
            });

        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_page_market, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.marketRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        if (!isViewShown) {
            subscribeToChanges();
        }

        DividerItemDecoration horizontalDecoration = new DividerItemDecoration(recyclerView.getContext(), VERTICAL);

        updateMarket();

        return view;
    }

    private void updateMarket()
    {
        Log.d("","");
        marketsViewModel.instrumentGroup.observe(getActivity(), instrumentGroups -> {

            InstrumentGroup instrumentGroup = instrumentGroups.get(position + 1);
            keys = new ArrayList<>(instrumentGroup.instruments.keySet());

            InstrumentGroup instrument = instrumentGroups.get(instrID);

            if (instrument == null) {
                Log.w(TAG, "instrument: null" );
                return;
            }

            Log.i(TAG, "instrumentGroup: " + instrument.name + " " + instrument.id + " / " + instrID + "page:" + position );

//            List<Instrument> instrumentsInOrder =  instrument.instruments.get(keys.get(position));

            List<Instrument> instrumentsInOrder =  new ArrayList<>(instrument.instruments.values());
            String filter = marketsViewModel.getSearchFilter();
            String searchFilter = null;
            if (filter!=null) {
                searchFilter = filter.toLowerCase();
            }
            ArrayList<Instrument> instruments = new ArrayList<>();

            for (Instrument item : instrumentsInOrder) {
                if(item!= null && TextUtils.isEmpty(searchFilter) ){
                    instruments.add(item);
                } else if( item!= null && !TextUtils.isEmpty(searchFilter) && item.name.toLowerCase().contains(searchFilter)){
                    instruments.add(item);
                }
            }


            if (marketAdapter == null) {
                marketAdapter = new MarketAdapter(instruments, requireContext(), this);
                marketAdapter.setOnBuyClicked(instrument12 -> {
                    SPUtils.getInstance(getContext()).saveStringPreferences("callback","market");
                    Intent intent = new Intent(getActivity(), BuySellActivity.class);
                    intent.putExtra(Constants.INSTRUMENT, instrument12);
                    intent.putExtra(Constants.ACCOUNT, tradesVM.getActiveAccount());
                    intent.putExtra(Constants.BUY_SELL_ASTATE, BuySellActivity.ActivityState.BUY);
                    startActivityForResult(intent,101);
                });
                marketAdapter.setOnSellClicked(instrument1 -> {
                    SPUtils.getInstance(getContext()).saveStringPreferences("callback","market");
                    Intent intent = new Intent(view.getContext(), BuySellActivity.class);
                    intent.putExtra(Constants.INSTRUMENT, instrument1);
                    intent.putExtra(Constants.ACCOUNT, tradesVM.getActiveAccount());
                    intent.putExtra(Constants.BUY_SELL_ASTATE, BuySellActivity.ActivityState.SELL);
                    startActivityForResult(intent,101);
                });
                marketAdapter.setOnItemClicked(instrument13 -> {
                    SPUtils.getInstance(getContext()).saveStringPreferences("callback","market");
                    Intent intent = new Intent(view.getContext(), BuySellActivity.class);
                    intent.putExtra(Constants.INSTRUMENT, instrument13);
                    intent.putExtra(Constants.ACCOUNT, tradesVM.getActiveAccount());
                    intent.putExtra(Constants.BUY_SELL_ASTATE, BuySellActivity.ActivityState.NONE);
                    startActivityForResult(intent,101);
                });
                recyclerView.setAdapter(marketAdapter);
            }
            else
            {
                marketAdapter.updateList(instruments);
                marketAdapter.notifyDataSetChanged();
            }


        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getView() != null) {
            isViewShown = true;
             subscribeToChanges();
        } else {
            isViewShown = false;
        }
    }

    private void subscribeToChanges() {
        position = 0;
        if (getArguments() != null) {
            position = getArguments().getInt(KEY_POSITION);
        }

        instrID = 0;
        if (getArguments() != null) {
            instrID = getArguments().getInt(KEY_INSTR_ID);
        }
        marketsViewModel.pageChanged(instrID);

    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        Log.d("","");
//        Intent intent = new Intent(getContext(), MainActivity.class);
//        intent.putExtra("isNewInstance", false);
//        startActivity(intent);
//        getActivity().finish();
//    }

    @Override
    public void onNullLiveData()
    {
//        Log.d("","");
//        if(!isLoading)
//        {
//            marketsViewModel.restartSocket();
//            isLoading = true;
//            addDelay();
//        }
//
//    }
//
//    private void addDelay()
//    {
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                isLoading = false;
//            }
//        },10000);
    }
}
