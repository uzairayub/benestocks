package com.fxvalley.exchanger.benestocks.sockets;

/**
 * Created by aloupas on 3/18/2017.
 */

public interface PriceSocketListener {
    void onPriceSocketStateChanged(SocketUtilities.ClientStatus clientStatus);
    void onCategoriesFecthed();
    void onLastPriceFetched();
}
