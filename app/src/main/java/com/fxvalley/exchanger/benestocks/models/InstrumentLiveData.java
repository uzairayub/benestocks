package com.fxvalley.exchanger.benestocks.models;

import net.cocooncreations.template.GeneralUtilities;

import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by aloupas on 3/8/2017.
 */

public class InstrumentLiveData implements Serializable {

    private final SimpleDateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
    private final SimpleDateFormat fromFormat2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
    private final SimpleDateFormat fromFormat3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);

    private final SimpleDateFormat toFormat = new SimpleDateFormat("HH:mm:ss", Locale.US);

    public Double bid;
    public Double ask;
    public String dT;
    public Boolean qC;
    public Double high;
    public Double low;
    public String bidMain;
    public String bidPip;
    public String bidPoint;
    public String askMain;
    public String askPip;
    public String askPoint;
    public Double sIP;
    public Double dPB;
    public Double dPA;
    public Double opn;
    public Double chP;
    public Double chg;
    public Double bI;
    public Double sI;

    public InstrumentLiveData(JSONObject jsonObject) {

        bid = jsonObject.optDouble("Bid");
        ask = jsonObject.optDouble("Ask");
        try {
            dT = toFormat.format(fromFormat.parse(jsonObject.optString("DT")));
        } catch (ParseException e) {
            try {
                dT = toFormat.format(fromFormat2.parse(jsonObject.optString("DT")));
            } catch (ParseException e1) {
                try {
                    dT = toFormat.format(fromFormat3.parse(jsonObject.optString("DT")));
                } catch (ParseException e2) {
                    e2.printStackTrace();
                    dT = "";
                }
            }
        }
        dT = applyTimeZone(dT);
        qC = jsonObject.optBoolean("QC");
        high = jsonObject.optDouble("High");
        low = jsonObject.optDouble("Low");
        bidMain = jsonObject.optString("BidMain");
        bidPip = jsonObject.optString("BidPip");
        bidPoint = jsonObject.optString("BidPoint");
        askMain = jsonObject.optString("AskMain");
        askPip = jsonObject.optString("AskPip");
        askPoint = jsonObject.optString("AskPoint");
        sIP = jsonObject.optDouble("SIP");
        dPB = jsonObject.optDouble("DPB");
        dPA = jsonObject.optDouble("DPA");
        opn = jsonObject.optDouble("Opn");
        chP = jsonObject.optDouble("ChP");
        chg = jsonObject.optDouble("Chg");
        bI = jsonObject.optDouble("BI");
        sI = jsonObject.optDouble("SI");

//        Log.d("af", toString());
    }

    private String applyTimeZone(String dT) {
        Date date = null;
        try {
            date = toFormat.parse(dT);
        } catch (ParseException e) {
            e.printStackTrace();
            return dT;
        }
        int offset = TimeZone.getDefault().getRawOffset() + TimeZone.getDefault().getDSTSavings();
        long now = date.getTime() + offset;
        return toFormat.format(new Date(now));
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InstrumentLiveData that = (InstrumentLiveData) o;

        if (fromFormat != null ? !fromFormat.equals(that.fromFormat) : that.fromFormat != null)
            return false;
        if (fromFormat2 != null ? !fromFormat2.equals(that.fromFormat2) : that.fromFormat2 != null)
            return false;
        if (fromFormat3 != null ? !fromFormat3.equals(that.fromFormat3) : that.fromFormat3 != null)
            return false;
        if (toFormat != null ? !toFormat.equals(that.toFormat) : that.toFormat != null)
            return false;
        if (bid != null ? !bid.equals(that.bid) : that.bid != null) return false;
        if (ask != null ? !ask.equals(that.ask) : that.ask != null) return false;
        if (dT != null ? !dT.equals(that.dT) : that.dT != null) return false;
        if (qC != null ? !qC.equals(that.qC) : that.qC != null) return false;
        if (high != null ? !high.equals(that.high) : that.high != null) return false;
        if (low != null ? !low.equals(that.low) : that.low != null) return false;
        if (bidMain != null ? !bidMain.equals(that.bidMain) : that.bidMain != null) return false;
        if (bidPip != null ? !bidPip.equals(that.bidPip) : that.bidPip != null) return false;
        if (bidPoint != null ? !bidPoint.equals(that.bidPoint) : that.bidPoint != null)
            return false;
        if (askMain != null ? !askMain.equals(that.askMain) : that.askMain != null) return false;
        if (askPip != null ? !askPip.equals(that.askPip) : that.askPip != null) return false;
        if (askPoint != null ? !askPoint.equals(that.askPoint) : that.askPoint != null)
            return false;
        if (sIP != null ? !sIP.equals(that.sIP) : that.sIP != null) return false;
        if (dPB != null ? !dPB.equals(that.dPB) : that.dPB != null) return false;
        if (dPA != null ? !dPA.equals(that.dPA) : that.dPA != null) return false;
        if (opn != null ? !opn.equals(that.opn) : that.opn != null) return false;
        if (chP != null ? !chP.equals(that.chP) : that.chP != null) return false;
        if (chg != null ? !chg.equals(that.chg) : that.chg != null) return false;
        if (bI != null ? !bI.equals(that.bI) : that.bI != null) return false;
        return sI != null ? sI.equals(that.sI) : that.sI == null;
    }

    @Override
    public int hashCode() {
        int result = fromFormat != null ? fromFormat.hashCode() : 0;
        result = 31 * result + (fromFormat2 != null ? fromFormat2.hashCode() : 0);
        result = 31 * result + (fromFormat3 != null ? fromFormat3.hashCode() : 0);
        result = 31 * result + (toFormat != null ? toFormat.hashCode() : 0);
        result = 31 * result + (bid != null ? bid.hashCode() : 0);
        result = 31 * result + (ask != null ? ask.hashCode() : 0);
        result = 31 * result + (dT != null ? dT.hashCode() : 0);
        result = 31 * result + (qC != null ? qC.hashCode() : 0);
        result = 31 * result + (high != null ? high.hashCode() : 0);
        result = 31 * result + (low != null ? low.hashCode() : 0);
        result = 31 * result + (bidMain != null ? bidMain.hashCode() : 0);
        result = 31 * result + (bidPip != null ? bidPip.hashCode() : 0);
        result = 31 * result + (bidPoint != null ? bidPoint.hashCode() : 0);
        result = 31 * result + (askMain != null ? askMain.hashCode() : 0);
        result = 31 * result + (askPip != null ? askPip.hashCode() : 0);
        result = 31 * result + (askPoint != null ? askPoint.hashCode() : 0);
        result = 31 * result + (sIP != null ? sIP.hashCode() : 0);
        result = 31 * result + (dPB != null ? dPB.hashCode() : 0);
        result = 31 * result + (dPA != null ? dPA.hashCode() : 0);
        result = 31 * result + (opn != null ? opn.hashCode() : 0);
        result = 31 * result + (chP != null ? chP.hashCode() : 0);
        result = 31 * result + (chg != null ? chg.hashCode() : 0);
        result = 31 * result + (bI != null ? bI.hashCode() : 0);
        result = 31 * result + (sI != null ? sI.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return GeneralUtilities.toString(this);
    }


}
