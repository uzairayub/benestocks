package com.fxvalley.exchanger.benestocks.utilities.validation;

public interface ITextValidator {
    void check();
}
