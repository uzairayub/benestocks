package com.fxvalley.exchanger.benestocks.database.source;

import android.content.Context;
import android.content.SharedPreferences;

import com.fxvalley.exchanger.benestocks.MyApplication;
import com.fxvalley.exchanger.benestocks.models.Trade;
import com.google.gson.Gson;

public class TradesPreferences {
    private static final String PREF_NAME = "PREFS_NAME";
    private static final String OBJECT_NAME = "MY_OBJECT";

    private SharedPreferences getSharedPreferences() {
        return MyApplication.getAppContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public void writeTrades(Trade trade) {
        SharedPreferences.Editor prefsEditor = getSharedPreferences().edit();
        Gson gson = new Gson();
        String json = gson.toJson(trade);
        prefsEditor.putString(OBJECT_NAME, json);
        prefsEditor.apply();
    }

    public Trade getTrade() {
        Gson gson = new Gson();
        String json = getSharedPreferences().getString(OBJECT_NAME, "");
        return gson.fromJson(json, Trade.class);
    }

}
