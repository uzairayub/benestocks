package com.fxvalley.exchanger.benestocks.models;

import net.cocooncreations.template.GeneralUtilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by aloupas on 3/8/2017.
 */

public class Instrument implements Serializable{

    public final String fixExecutionName;
    public Integer id;
    public String name;
    public String fixName;
    public Integer contractMultiplier;
    public Integer factor;
    public Integer product;
    public Integer precision;
    public Integer spreadMultiplier;
    public Integer point;
    public Integer baseId;
    public Base base;
    public Integer quoteId;
    public Quote quote;
//    public Integer contractId;
//    public Contract contract;
    public Boolean enabled;
    public Integer profitCondition;
    public Integer lossCondition;
    public Integer instrumentCategoryId;
    public Integer order;

    public InstrumentLiveData instrumentLiveData;

    public Instrument(JSONObject jsonObject){
        id = jsonObject.optInt("Id");
        name = jsonObject.optString("Name");
        fixName = jsonObject.optString("FixName");
        fixExecutionName = jsonObject.optString("FixExecutionName");
        contractMultiplier = jsonObject.optInt("ContractMultiplier");
        factor  = jsonObject.optInt("Factor");
        product = jsonObject.optInt("Product");
        precision = jsonObject.optInt("Precision");
        spreadMultiplier = jsonObject.optInt("SpreadMultiplier");
        point = jsonObject.optInt("Point");
        baseId = jsonObject.optInt("BaseId");
        try {
            base = new Base(jsonObject.getJSONObject("Base"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        quoteId = jsonObject.optInt("QuoteId");
        try {
            quote = new Quote(jsonObject.getJSONObject("Quote"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        contractId = jsonObject.optInt("ContractId");
//        try {
//            contract = new Contract(jsonObject.getJSONObject("Contract"));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
        enabled = jsonObject.optBoolean("Enabled");
        profitCondition = jsonObject.optInt("ProfitCondition");
        lossCondition = jsonObject.optInt("LossCondition");
        instrumentCategoryId = jsonObject.optInt("InstrumentCategoryId");
        order = jsonObject.optInt("Order");

//        instrumentLiveData = new InstrumentLiveData(jsonObject);



    }

    @Override
    public String toString() {
        return GeneralUtilities.toString(this);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Instrument that = (Instrument) o;

        if (fixExecutionName != null ? !fixExecutionName.equals(that.fixExecutionName) : that.fixExecutionName != null)
            return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (fixName != null ? !fixName.equals(that.fixName) : that.fixName != null) return false;
        if (contractMultiplier != null ? !contractMultiplier.equals(that.contractMultiplier) : that.contractMultiplier != null)
            return false;
        if (factor != null ? !factor.equals(that.factor) : that.factor != null) return false;
        if (product != null ? !product.equals(that.product) : that.product != null) return false;
        if (precision != null ? !precision.equals(that.precision) : that.precision != null)
            return false;
        if (spreadMultiplier != null ? !spreadMultiplier.equals(that.spreadMultiplier) : that.spreadMultiplier != null)
            return false;
        if (point != null ? !point.equals(that.point) : that.point != null) return false;
        if (baseId != null ? !baseId.equals(that.baseId) : that.baseId != null) return false;
        if (base != null ? !base.equals(that.base) : that.base != null) return false;
        if (quoteId != null ? !quoteId.equals(that.quoteId) : that.quoteId != null) return false;
        if (quote != null ? !quote.equals(that.quote) : that.quote != null) return false;
        if (enabled != null ? !enabled.equals(that.enabled) : that.enabled != null) return false;
        if (profitCondition != null ? !profitCondition.equals(that.profitCondition) : that.profitCondition != null)
            return false;
        if (lossCondition != null ? !lossCondition.equals(that.lossCondition) : that.lossCondition != null)
            return false;
        if (instrumentCategoryId != null ? !instrumentCategoryId.equals(that.instrumentCategoryId) : that.instrumentCategoryId != null)
            return false;
        if (order != null ? !order.equals(that.order) : that.order != null) return false;
        return instrumentLiveData != null ? instrumentLiveData.equals(that.instrumentLiveData) : that.instrumentLiveData == null;
    }

    @Override
    public int hashCode() {
        int result = fixExecutionName != null ? fixExecutionName.hashCode() : 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (fixName != null ? fixName.hashCode() : 0);
        result = 31 * result + (contractMultiplier != null ? contractMultiplier.hashCode() : 0);
        result = 31 * result + (factor != null ? factor.hashCode() : 0);
        result = 31 * result + (product != null ? product.hashCode() : 0);
        result = 31 * result + (precision != null ? precision.hashCode() : 0);
        result = 31 * result + (spreadMultiplier != null ? spreadMultiplier.hashCode() : 0);
        result = 31 * result + (point != null ? point.hashCode() : 0);
        result = 31 * result + (baseId != null ? baseId.hashCode() : 0);
        result = 31 * result + (base != null ? base.hashCode() : 0);
        result = 31 * result + (quoteId != null ? quoteId.hashCode() : 0);
        result = 31 * result + (quote != null ? quote.hashCode() : 0);
        result = 31 * result + (enabled != null ? enabled.hashCode() : 0);
        result = 31 * result + (profitCondition != null ? profitCondition.hashCode() : 0);
        result = 31 * result + (lossCondition != null ? lossCondition.hashCode() : 0);
        result = 31 * result + (instrumentCategoryId != null ? instrumentCategoryId.hashCode() : 0);
        result = 31 * result + (order != null ? order.hashCode() : 0);
        result = 31 * result + (instrumentLiveData != null ? instrumentLiveData.hashCode() : 0);
        return result;
    }
}
