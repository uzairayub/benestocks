package com.fxvalley.exchanger.benestocks.utilities;

public interface IView {
    void showMessage(String message);
}
