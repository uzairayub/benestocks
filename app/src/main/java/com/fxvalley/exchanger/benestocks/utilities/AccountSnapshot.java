package com.fxvalley.exchanger.benestocks.utilities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.fxvalley.exchanger.benestocks.data.TradeRepository;
import com.fxvalley.exchanger.benestocks.models.Instrument;
import com.fxvalley.exchanger.benestocks.models.InstrumentGroup;
import com.fxvalley.exchanger.benestocks.models.InstrumentLiveData;
import com.fxvalley.exchanger.benestocks.models.Trade;
import com.fxvalley.exchanger.benestocks.sockets.PriceSessionSocket;
import com.fxvalley.exchanger.benestocks.sockets.SocketUtilities;
import com.fxvalley.exchanger.benestocks.sockets.TradeSessionSocket;

import java.util.Collections;
import java.util.HashSet;

/**
 * Created by user on 25/07/2017.
 */

public class AccountSnapshot {
    private static final String TAG = "AccountSnapshot" ;

    // Name
    // I
/*    E (CURRENT ACCOUNT BALANCE->C)  C+CPL+ total from open trades
    UC (->UM)
    OPL (->P&L)
    SO (->SL)*/

    private TradeSessionSocket tradeSessionSocket;
    private PriceSessionSocket priceSessionSocket;
    private TopHederViewAdapter topHeaderViewAdapter;

    private volatile boolean isPriceSocketReady = false;
    private volatile boolean openTradesReceived = false;
    private String openTrades[];
    private HashSet<String> openTradesSet = new HashSet<>();

    private Context mContext;

    private Handler handler;
    private BroadcastReceiver ticksUpdatedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            onTicksUpdated(context, intent);
        }
    };

    private BroadcastReceiver openTradesReceivedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG,"Receive message openTradesReceivedReceiver");
            onOpenTradesReceived(context, intent);
        }
    };

    private void onTicksUpdated(Context context, Intent intent) {

        if (intent.hasExtra(Constants.INSTRUMENT))
            try {
                Instrument instrument = (Instrument) intent.getSerializableExtra(Constants.INSTRUMENT);
                if (openTradesSet.contains(instrument.name)) {
                    updateStatusValues(instrument);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

    }

    private void onOpenTradesReceived(Context context, Intent intent) {
        Log.i(TAG,"Call onOpenTradesReceived");
        openTrades = intent.getStringArrayExtra(Constants.OPEN_TRADES);
        double calculatedPL = intent.getDoubleExtra(Constants.CALCULATED_PL, -1);
        openTradesSet.clear();

        Collections.addAll(openTradesSet, openTrades);

        if (tradeSessionSocket != null
                && tradeSessionSocket.clientStatus == SocketUtilities.ClientStatus.CONNECTED) {
            if (topHeaderViewAdapter!= null) {
                topHeaderViewAdapter.updateTopHeaderValues(tradeSessionSocket.accounts.get(tradeSessionSocket.activeAccount), calculatedPL);
            }

        }

        if (tradeSessionSocket == null) {
            Log.e(TAG, "Error : tradeSessionSocket is null");
        } else if(tradeSessionSocket.clientStatus != SocketUtilities.ClientStatus.CONNECTED) {
            Log.e(TAG,"Client status not connected");
        } else {
            Log.i(TAG,"onOpenTrades received OK ");
        }

        if (this.isPriceSocketReady && priceSessionSocket != null
                && priceSessionSocket.clientStatus == SocketUtilities.ClientStatus.CONNECTED) {
            this.priceSessionSocket.subscribeForStatus(openTrades);
            priceSessionSocket.getLastPriceInstrument(openTrades);
        }

        openTradesReceived = true;
    }

    public void onResume(Context context, TradeSessionSocket tradeSessionSocket,
                         PriceSessionSocket priceSessionSocket, TopHederViewAdapter topHeaderViewAdapter) {
        Log.i(TAG,"onResume");

        handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {

                updateTopHeader();

                handler.postDelayed(this, 1000);
            }
        });

        this.tradeSessionSocket = tradeSessionSocket;
        this.priceSessionSocket = priceSessionSocket;
        this.topHeaderViewAdapter = topHeaderViewAdapter;

        this.mContext = context;

        LocalBroadcastManager.getInstance(mContext.getApplicationContext())
                .registerReceiver(openTradesReceivedReceiver, new IntentFilter(Constants.ACCOUNT_SNAPSHOT));
        LocalBroadcastManager.getInstance(mContext.getApplicationContext())
                .registerReceiver(ticksUpdatedReceiver, new IntentFilter(Constants.TICK_UPDATED));

    }

    public void onPause() {

        LocalBroadcastManager.getInstance(mContext.getApplicationContext()).unregisterReceiver(openTradesReceivedReceiver);
        LocalBroadcastManager.getInstance(mContext.getApplicationContext()).unregisterReceiver(ticksUpdatedReceiver);

        if (handler != null)
            handler.removeCallbacksAndMessages(null);

    }

    public void onTradeSessionSocketReady() {
        tradeSessionSocket.getOpen(1, 0, 20).subscribe((result)->{}, (err)->{
            Log.e(TAG, "onTradeSessionSocketReady: getOpen ", err);
        });
    }

    public void onPriceSessionSocketReady() {

        if (openTradesReceived) {
            priceSessionSocket.subscribeForStatus(openTrades);
            priceSessionSocket.getLastPriceInstrument(openTrades);
        }

        Log.d("PriceSocket", "Price Socket Ready");

        isPriceSocketReady = true;
    }

    private void updateStatusValues(Instrument instrument) {

        for (Trade trade : tradeSessionSocket.openPositions) {

            if (trade.instrument.id.equals(instrument.id)) {
                if (instrument.instrumentLiveData != null) {
                    trade.instrument.instrumentLiveData = instrument.instrumentLiveData;
                }

                break;
            }
        }

    }

    private void updateTopHeader() {

        if (isPriceSocketReady && openTradesReceived) {

            Double openTotal = 0.0;

            for (Trade trade : tradeSessionSocket.openPositions) {

                String profitCurrency = trade.instrument.quote.name +
                        tradeSessionSocket.accounts.get(tradeSessionSocket.activeAccount).currency;
                String profitCurrency2 = tradeSessionSocket.accounts.get(tradeSessionSocket.activeAccount).currency
                        + trade.instrument.quote.name;


                if (!openTradesSet.contains(profitCurrency)) {
                    priceSessionSocket.getLastPriceInstrument(new String[]{profitCurrency});
                    priceSessionSocket.subscribe(profitCurrency, String.valueOf(priceSessionSocket.postFix.tAL));
                    openTradesSet.add(profitCurrency);
                }

                if (!openTradesSet.contains(profitCurrency2)) {
                    priceSessionSocket.getLastPriceInstrument(new String[]{profitCurrency2});
                    priceSessionSocket.subscribe(profitCurrency2, String.valueOf(priceSessionSocket.postFix.tAL));
                    openTradesSet.add(profitCurrency2);
                }

                InstrumentLiveData instrumentLiveData = null;

                double rate = 1;

                try {

                    InstrumentLiveData instrumentLiveData1 = priceSessionSocket.instrumentGroups.get(trade.instrument.instrumentCategoryId)
                            .instruments.get(trade.instrument.id).instrumentLiveData;
                    if (instrumentLiveData1 != null) {
                        trade.instrument.instrumentLiveData = instrumentLiveData1;
                    }

                    for (InstrumentGroup instrumentGroup : priceSessionSocket.instrumentGroups.values()) {
                        for (Instrument instrument : instrumentGroup.instruments.values()) {
                            if (instrument.name.equals(profitCurrency)) {
                                if (instrument.instrumentLiveData != null) {
                                    instrumentLiveData = instrument.instrumentLiveData;
                                }
                                rate = trade.type == 1 ? instrumentLiveData.ask : instrumentLiveData.bid;
                                break;
                            }
                            if (instrument.name.equals(profitCurrency2)) {
                                if (instrument.instrumentLiveData != null) {
                                    instrumentLiveData = instrument.instrumentLiveData;
                                }
                                rate = trade.type == 1 ? 1 / instrumentLiveData.ask : 1 / instrumentLiveData.bid;
                                break;
                            }
                        }

                    }

                } catch (NullPointerException nullPointerEx) {
                    nullPointerEx.printStackTrace();
                }

                openTotal += trade.calculatePL(TradeRepository.OPEN, rate);

            }

            if (topHeaderViewAdapter != null && tradeSessionSocket != null && tradeSessionSocket.accounts != null)
                topHeaderViewAdapter.updateTopHeaderValues(tradeSessionSocket.accounts.get(tradeSessionSocket.activeAccount), openTotal);
        }

    }

}
