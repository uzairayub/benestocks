package com.fxvalley.exchanger.benestocks.fragments;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.fxvalley.exchanger.benestocks.R;
import com.fxvalley.exchanger.benestocks.markets.MarketFragment;
import com.fxvalley.exchanger.benestocks.models.EventData;
import com.fxvalley.exchanger.benestocks.utilities.Constants;

import net.cocooncreations.template.network.CocoonStringRequest;
import net.cocooncreations.template.network.RequestFragment;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;



/**
 * A simple {@link Fragment} subclass.
 */
public class CalendarFragment extends RequestFragment implements SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = "CalendarFragment";

    private CalendarFragment.OnCalendarFragmentFragmentInteractionListener mListener;
    private ArrayList<EventData> eventsList;
    private RecyclerView calendarRecyclerView;
    private EventsAdapter eventsAdapter;
    private SwipeRefreshLayout calendarRefresher;
    private int todayIndex = 0;
    private Calendar myCalendar;

//    uzair
    private TextView tvToday;
    private LinearLayoutManager layoutManager;

    public CalendarFragment() {
        // Required empty public constructor
    }

    public static CalendarFragment newInstance() {
        CalendarFragment fragment = new CalendarFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myCalendar = Calendar.getInstance();
        eventsList = new ArrayList<EventData>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_calendar, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        calendarRecyclerView = (RecyclerView) view.findViewById(R.id.calendarRecyclerView);
        calendarRefresher = view.findViewById(R.id.calendarRefresher);
        calendarRefresher.setOnRefreshListener(this);
        layoutManager = new LinearLayoutManager(getContext());
        calendarRecyclerView.setLayoutManager(layoutManager);

        calendarRecyclerView.setAdapter(eventsAdapter= new EventsAdapter());

        tvToday = view.findViewById(R.id.tvToday);
        setToday();
    }

    private void setToday()
    {
        SimpleDateFormat sd=new SimpleDateFormat("EEEE, dd MMM");
        String today = sd.format(myCalendar.getTime());
        tvToday.setText(today);

        tvToday.setOnClickListener(view -> {

            if(calendarRecyclerView != null)
            {
                int scrollPosition = todayIndex;
                if(layoutManager.findFirstCompletelyVisibleItemPosition() < todayIndex)
                {
                    scrollPosition = todayIndex + layoutManager.findLastVisibleItemPosition();
                }
                calendarRecyclerView.smoothScrollToPosition(scrollPosition);
            }
        });
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MarketFragment.OnMarketFragmentInteractionListener) {
            mListener = (OnCalendarFragmentFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getUpdatedData();
    }

    private void getUpdatedData()
    {
        calendarRefresher.setRefreshing(true);
        Calendar instance = Calendar.getInstance();
        int day = instance.get(Calendar.DAY_OF_MONTH);
        int year = instance.get(Calendar.YEAR);
//        String url = Constants.EVENTS_URL + "?" +  String.format("%s.%s", day, year);
        CocoonStringRequest.with(Constants.EVENTS_URL, this).callAsync();
    }
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public <T> void onResponse(T response, String requestcode, NetworkResponse networkResponse) {

        calendarRefresher.setRefreshing(false);
        String reply = (String) response;

        Log.d(TAG, reply);

        DocumentBuilderFactory documentBuilder = DocumentBuilderFactory.newInstance();
        try {

            DocumentBuilder documentBuilder1 = documentBuilder.newDocumentBuilder();

            InputStream stream = new ByteArrayInputStream(reply.getBytes("UTF-8"));

            Document parse = documentBuilder1.parse(stream);

            NodeList item = parse.getElementsByTagName("event");

            eventsList.clear();

            for (int i = item.getLength() -1; i >= 0; i--) {
                eventsList.add(new EventData(item.item(i)));

                SimpleDateFormat sd=new SimpleDateFormat("dd");
                String today = sd.format(myCalendar.getTime());
                String[] dateParts = eventsList.get(eventsList.size()-1).date.split("-");

                if((today.equalsIgnoreCase(dateParts[1])))
                {
                    todayIndex = i;
                }
//                eventsList.get(i).toString();
            }

            Collections.reverse(eventsList);
            eventsAdapter.notifyDataSetChanged();
            moveToPosition();


        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }


    }

    private void moveToPosition()
    {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                tvToday.callOnClick();
            }
        },10);

    }

    @Override
    public void onError(VolleyError volleyError) {

    }

    @Override
    public void onRefresh()
    {
        getUpdatedData();
    }


    public interface OnCalendarFragmentFragmentInteractionListener {
    }

    public class EventsAdapter extends RecyclerView.Adapter<EventsViewHolder> {

        @Override
        public EventsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new EventsViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.events_row, parent, false));
        }

        @Override
        public void onBindViewHolder(EventsViewHolder holder, int position) {

            holder.country.setText(eventsList.get(position).country);
            holder.title.setText(eventsList.get(position).title);
            holder.time.setText(eventsList.get(position).time);
            holder.impact.setText(eventsList.get(position).getImpactString());
            holder.forecast.setText(eventsList.get(position).forecast);
            holder.previous.setText(eventsList.get(position).previous);

            String[] dateParts = (eventsList.get(position).date).split("-");
            String monthName = getMonthShortName(Integer.parseInt(dateParts[0]));
            holder.date.setText(monthName+" "+dateParts[1]+", "+dateParts[2]);
        }


        public String getMonthShortName(int monthNumber) {
            String monthName = "";

            if (monthNumber >= 0 && monthNumber < 12)
                try {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.MONTH, (monthNumber-1));

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM");
                    //simpleDateFormat.setCalendar(calendar);
                    monthName = simpleDateFormat.format(calendar.getTime());
                } catch (Exception e) {
                    if (e != null)
                        e.printStackTrace();
                }
            return monthName;
        }

        @Override
        public int getItemCount() {
            return eventsList.size();
        }
    }

    public class EventsViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public TextView country;
        public TextView date;
        public TextView time;
        public TextView impact;
        public TextView forecast;
        public TextView previous;

        public EventsViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.eventsTitle);
            country = (TextView) itemView.findViewById(R.id.eventsCountry);
            date = (TextView) itemView.findViewById(R.id.eventsDate);
            time = (TextView) itemView.findViewById(R.id.eventsTime);
            impact = (TextView) itemView.findViewById(R.id.eventsImpact);
            forecast = (TextView) itemView.findViewById(R.id.eventsForecast);
            previous = (TextView) itemView.findViewById(R.id.eventsPrevious);


        }
    }

}
