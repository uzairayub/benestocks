package com.fxvalley.exchanger.benestocks.utilities;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SPUtils {


    private static SPUtils instance = null;
    private Context mContext;
    private final String STRING_PREFERENCES = "StringPreferences";
    private final String BOOLEAN_PREFERENCES = "BooleanPreferences";
    private final String INTEGER_PREFERENCES = "IntegerPreferences";


    public static SPUtils getInstance(Context context) {
        if (instance == null) {
            instance = new SPUtils(context);
        }
        return instance;
    }

    public SPUtils(Context context) {
        this.mContext = context;
    }


    public float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    public void saveStringPreferences(String key, String value) {
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(STRING_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void saveIntegerPreferences(String key, int value) {
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(INTEGER_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public void saveBooleanPreferences(String key, boolean value) {
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(BOOLEAN_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public String getStringPreferences(String key) {
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(STRING_PREFERENCES, Context.MODE_PRIVATE);
        return sharedpreferences.getString(key, "");
    }

    public boolean getBooleanPreferences(String key) {
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(BOOLEAN_PREFERENCES, Context.MODE_PRIVATE);
        return sharedpreferences.getBoolean(key, false);
    }

    public int getIntegerPreferences(String key) {
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(INTEGER_PREFERENCES, Context.MODE_PRIVATE);
        return sharedpreferences.getInt(key, 0);
    }

    public <T> List getListPreferences(String key, Class<T> clazz) {
        try {
            Gson gson = new Gson();
            String value = getStringPreferences(key);
            if (!value.equals("")) {
                Type type = com.google.gson.internal.$Gson$Types.newParameterizedTypeWithOwner(null, ArrayList.class, clazz);
                return gson.<List<T>>fromJson(value, type);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void clearAllSharedPreferences() {
        SharedPreferences stringSharedpreferences = mContext.getSharedPreferences(STRING_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences integerSharedpreferences = mContext.getSharedPreferences(INTEGER_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences booleanSharedpreferences = mContext.getSharedPreferences(BOOLEAN_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = stringSharedpreferences.edit();
        editor.clear().apply();
        SharedPreferences.Editor editor1 = integerSharedpreferences.edit();
        editor1.clear().apply();
        SharedPreferences.Editor editor2 = booleanSharedpreferences.edit();
        editor2.clear().apply();
    }


}
