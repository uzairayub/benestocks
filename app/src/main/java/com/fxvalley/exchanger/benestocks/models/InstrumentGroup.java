package com.fxvalley.exchanger.benestocks.models;

import net.cocooncreations.template.GeneralUtilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by aloupas on 3/8/2017.
 */

public class InstrumentGroup {

    public Integer id;
    public String indices;
    public String name;
    public Integer order;
    public Map<Integer, Instrument> instruments  = new LinkedHashMap<>();
    public Boolean enabled;

    public InstrumentGroup(JSONObject jsonObject) {

        id = jsonObject.optInt("Id");
        indices = jsonObject.optString("Indices");
        order = jsonObject.optInt("Order");
        name = jsonObject.optString("Name");
        try {
            JSONArray instrumentsobject = jsonObject.getJSONArray("Instruments");
            instruments.clear();

            for (int i = 0; i < instrumentsobject.length(); i++) {
                Instrument instrument = new Instrument(instrumentsobject.getJSONObject(i));
                instruments.put(instrument.id, instrument);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        enabled = jsonObject.optBoolean("Enabled");

    }

    @Override
    public String toString() {
        return GeneralUtilities.toString(this);
    }

}
