package com.fxvalley.exchanger.benestocks.utilities;

/**
 * Created by aloupas on 3/9/2017.
 */

public interface CategoriesListener {
    void onCategoriesFetched();
}
