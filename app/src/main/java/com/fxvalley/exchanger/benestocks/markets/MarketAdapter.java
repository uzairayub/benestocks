package com.fxvalley.exchanger.benestocks.markets;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fxvalley.exchanger.benestocks.R;
import com.fxvalley.exchanger.benestocks.models.Instrument;

import java.text.DecimalFormat;
import java.util.List;


public class MarketAdapter extends RecyclerView.Adapter<MarketViewHolder> {

    private static final String TAG = "MarketAdapter";
    private DecimalFormat decimalFormat = new DecimalFormat("0.00");
    private List<Instrument> instrumentList;
    private Context context;

    OnBuyClicked onBuyClicked;
    OnSellClicked onSellClicked;
    OnItemClicked onItemClicked;
    private Boolean isOpen = true;

    private MarketCallback callback;

    public MarketAdapter(List<Instrument> instrumentList, Context context,MarketCallback callback) {
       this.instrumentList = instrumentList;
        this.context = context;
        this.callback = callback;
    }

    @Override
    public MarketViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MarketViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_market_test, parent, false));
    }

    @Override
    public void onBindViewHolder(MarketViewHolder holder, int position) {
        Instrument instrument = this.instrumentList.get(position);
//        if (!search.getText().toString().isEmpty() && !this.oldList.name.toLowerCase().contains(search.getText().toString().toLowerCase())) {
//            ViewGroup.LayoutParams layoutParams = holder.itemView.getLayoutParams();
//            layoutParams.height = 0;
//            holder.itemView.setLayoutParams(layoutParams);
//            return;
//        }
        holder.currencyPair.setText(instrument.name);

        if (instrument.instrumentLiveData == null) {

            callback.onNullLiveData();
            Log.w(TAG, "onBindViewHolder:  instrument[" + position +"] .instrumentLiveData == null !");
            String placeholer = "-";
            holder.updatedAt.setText(placeholer);
            holder.marketRowOpen.setText(placeholer);
            holder.marketRowHigh.setText(placeholer);
            holder.marketRowLow.setText(placeholer);
            holder.marketRowClose.setText(placeholer);
            holder.marketRowClose.setTextColor(Color.WHITE);
            holder.marketRowSellValue.setText(placeholer);
            holder.marketRowSellValueTwoDigits.setText(placeholer);
            holder.marketRowSellValueLastDigit.setText("");
            holder.marketRowBuyValue.setText(placeholer);
            holder.marketRowBuyValueTwoDigits.setText(placeholer);
            holder.marketRowBuyValueLastDigit.setText("");
            holder.marketRowSpread.setText(placeholer);

            holder.marketRowButtonBuy.setOnClickListener(null);
            holder.marketRowButtonSell.setOnClickListener(null);

            holder.itemView.setOnClickListener(null);

            holder.marketRowButtonBuy.setBackgroundResource(R.drawable.market_button_blue_background);
            holder.marketRowButtonSell.setBackgroundResource(R.drawable.market_button_blue_background);

        } else {
            holder.updatedAt.setText("U:" + instrument.instrumentLiveData.dT);
            holder.marketRowOpen.setText("O:" + String.valueOf(instrument.instrumentLiveData.opn));
            holder.marketRowHigh.setText("H:" + String.valueOf(instrument.instrumentLiveData.high));
            holder.marketRowLow.setText("L:" + String.valueOf(instrument.instrumentLiveData.low));
            try {
                holder.marketRowClose.setText(String.valueOf("C:" + decimalFormat.format(instrument.instrumentLiveData.chP) + "%"));
                if (instrument.instrumentLiveData.chP > 0) {
                    holder.marketRowClose.setTextColor(context.getResources().getColor(R.color.button_green));
                } else if (instrument.instrumentLiveData.chP < 0) {
                    holder.marketRowClose.setTextColor(context.getResources().getColor(R.color.button_red));
                } else {
                    holder.marketRowClose.setTextColor(Color.WHITE);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                holder.marketRowClose.setText(String.valueOf("C:" + "-"));
                holder.marketRowClose.setTextColor(Color.WHITE);
            }

            holder.marketRowSellValue.setText(String.valueOf(instrument.instrumentLiveData.bidMain));
            holder.marketRowSellValueTwoDigits.setText(String.valueOf(instrument.instrumentLiveData.bidPip));
            holder.marketRowSellValueLastDigit.setText(String.valueOf(instrument.instrumentLiveData.bidPoint));

            holder.marketRowBuyValue.setText(String.valueOf(instrument.instrumentLiveData.askMain));
            holder.marketRowBuyValueTwoDigits.setText(String.valueOf(instrument.instrumentLiveData.askPip));
            holder.marketRowBuyValueLastDigit.setText(String.valueOf(instrument.instrumentLiveData.askPoint));
            holder.marketRowSpread.setText(String.valueOf(instrument.instrumentLiveData.sIP));

            if (isOpen) {
                if (instrument.instrumentLiveData.dPA > 0) {
                    holder.marketRowButtonBuy.setBackgroundResource(R.drawable.market_button_green_background);
                } else if (instrument.instrumentLiveData.dPA < 0) {
                    holder.marketRowButtonBuy.setBackgroundResource(R.drawable.market_button_red_background);
                } else {
                    holder.marketRowButtonBuy.setBackgroundResource(R.drawable.market_button_blue_background);
                }
            } else {
                holder.marketRowButtonBuy.setBackgroundResource(R.drawable.market_button_orange_background);
            }

            if (isOpen) {
                if (instrument.instrumentLiveData.dPB > 0) {
                    holder.marketRowButtonSell.setBackgroundResource(R.drawable.market_button_green_background);
                } else if (instrument.instrumentLiveData.dPB < 0) {
                    holder.marketRowButtonSell.setBackgroundResource(R.drawable.market_button_red_background);
                } else {
                    holder.marketRowButtonSell.setBackgroundResource(R.drawable.market_button_blue_background);
                }
            } else {
                holder.marketRowButtonSell.setBackgroundResource(R.drawable.market_button_orange_background);
            }
                holder.marketRowButtonBuy.setOnClickListener(view -> {
                    if (onBuyClicked != null) {
                        onBuyClicked.onBuyClicked(instrument);
                    }
                });
                holder.marketRowButtonSell.setOnClickListener(view -> {
                    if (onSellClicked != null) {
                        onSellClicked.onSellClicked(instrument);
                    }
                });
                holder.getRoot().setOnClickListener(view -> {
                    if (onItemClicked != null) {
                        onItemClicked.onItemClicked(instrument);
                    }
                });
        }


    }

    @Override
    public int getItemCount() {
        try {
            return instrumentList.size();
        } catch (NullPointerException ex) {
            return 0;
        }
    }

    public void setOnBuyClicked(OnBuyClicked onBuyClicked) {
        this.onBuyClicked = onBuyClicked;
    }

    public void setOnSellClicked(OnSellClicked onSellClicked) {
        this.onSellClicked = onSellClicked;
    }

    public void setOnItemClicked(OnItemClicked onItemClicked) {
        this.onItemClicked = onItemClicked;
    }

    public void updateList(List<Instrument> newList) {
        //todo diffutil
//        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new InstrumentDiffCallback(this.instrumentList, newList));
//        diffResult.dispatchUpdatesTo(this);
        this.instrumentList = newList;
        notifyDataSetChanged();
    }

    public void setIsOpen(Boolean isOpen) {
        this.isOpen = isOpen;
        notifyDataSetChanged();
    }


    public interface OnBuyClicked{
        void onBuyClicked(Instrument instrument);
    }
    public interface OnSellClicked{
        void onSellClicked(Instrument instrument);
    }
    public interface OnItemClicked{
        void onItemClicked(Instrument instrument);
    }
    public interface MarketCallback
    {
        void onNullLiveData();
    }

    private static class InstrumentDiffCallback extends DiffUtil.Callback {

        private final List<Instrument> oldList;
        private final List<Instrument> newList;

        public InstrumentDiffCallback(List<Instrument> oldList, List<Instrument> newList) {
            this.oldList = oldList;
            this.newList = newList;
        }

        @Override
        public int getOldListSize() {
            return oldList.size();
        }

        @Override
        public int getNewListSize() {
            return newList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            Instrument oldInstrument = oldList.get(oldItemPosition);
            Instrument newInstrument = newList.get(newItemPosition);
            return oldInstrument.id.equals( newInstrument.id);
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            Instrument oldInstrument = oldList.get(oldItemPosition);
            Instrument newInstrument = newList.get(newItemPosition);
            return oldInstrument.equals(newInstrument);
        }
    }
}