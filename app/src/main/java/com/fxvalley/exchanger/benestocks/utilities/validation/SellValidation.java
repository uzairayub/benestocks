package com.fxvalley.exchanger.benestocks.utilities.validation;

public class SellValidation implements IValidation {
    private String stopLostError = "";
    private String stopProfitError = "";

    @Override
    public boolean isStopLostValid(double stopLost, double marketPriceValue) {
        boolean stopLostAboveZero = stopLost > 0;
        if(!stopLostAboveZero){
            stopLostError = "Stop lost should be above zero";
        }
        boolean stopLostHigherThanMarket = stopLost > marketPriceValue;
        if(!stopLostHigherThanMarket){
            stopLostError = "Stop lost should be higher than market price";
        }

        if(!stopLostAboveZero && !stopLostHigherThanMarket){
            stopLostError = "Stop lost should be higher than market price and above zero";
        }
        return stopLostAboveZero && stopLostHigherThanMarket;
    }

    @Override
    public boolean isStopProfitValid(double stopProfit, double marketPriceValue) {
        boolean stopProfitAboveZero = stopProfit > 0;
        boolean stopProfitLessThanMarket = stopProfit < marketPriceValue;

        if(!stopProfitAboveZero){
            stopProfitError = "Stop profit should be above zero";
        }
        if(!stopProfitLessThanMarket){
            stopProfitError = "Stop profit should be less than market price";
        }

        if(!stopProfitAboveZero && !stopProfitLessThanMarket){
            stopProfitError = "Stop profit should be less than market price and above zero";
        }
        return stopProfitAboveZero && stopProfitLessThanMarket;
    }

    @Override
    public CharSequence getStopLostError() {
        return stopLostError;
    }

    @Override
    public CharSequence getStopProfitError() {
        return stopProfitError;
    }
}
