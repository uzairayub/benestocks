package com.fxvalley.exchanger.benestocks.models;

import android.content.Context;
import android.content.SharedPreferences;

import com.fxvalley.exchanger.benestocks.utilities.Constants;

import net.cocooncreations.template.GeneralUtilities;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by aloupas on 3/4/2017.
 */

public class User {

    public String firstName;
    public String lastName;
    public String country;
    public String email;
    public String phone;
    public String username;
    public String token;

    public JSONObject toJSON() {

        JSONObject userObject = new JSONObject();

        try {
            userObject.put("firstName", firstName);
            userObject.put("lastName", lastName);
            userObject.put("country", country);
            userObject.put("email", email);
            userObject.put("phone", phone);
            userObject.put("userName", username);
            userObject.put("refresh_token", token);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return userObject;
    }

    public User(JSONObject jsonObject) throws JSONException {

        firstName = jsonObject.optString("firstName");
        lastName = jsonObject.optString("lastName");
        country = jsonObject.optString("country");
        email = jsonObject.optString("email");
        phone = jsonObject.optString("phone");
        username = jsonObject.optString("userName");
        token = jsonObject.getString("refresh_token");

    }

    @Override
    public String toString() {

        return GeneralUtilities.toString(this);

    }

    public void save(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString("user", toJSON().toString()).apply();


    }

    public static User getUser(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE);
        User user = null;
        try {
             user = new User(new JSONObject(sharedPreferences.getString("user", "")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return user;
    }
}
