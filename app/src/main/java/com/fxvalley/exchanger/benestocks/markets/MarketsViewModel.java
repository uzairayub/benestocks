package com.fxvalley.exchanger.benestocks.markets;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.fxvalley.exchanger.benestocks.models.InstrumentGroup;
import com.fxvalley.exchanger.benestocks.sockets.PriceSessionSocket;

import java.util.Map;

public class MarketsViewModel extends ViewModel {

    private static final String TAG = "MarketsViewModel";

    private PriceSessionSocket priceSocket;

    public MutableLiveData<Map<Integer, InstrumentGroup>> instrumentGroup = new MutableLiveData<>();
    private String filterText;

    public MarketsViewModel(PriceSessionSocket priceSocket) {
        this.priceSocket = priceSocket;
    }

    public void start() {
        Log.i(TAG, "start: ");
        getInstrumentGroups();
    }

    private void getInstrumentGroups() {
        priceSocket.getInstrumentGroups()
                .subscribe(integerInstrumentGroupMap -> {
                            Log.i(TAG, "getInstrumentGroups: ");
                            instrumentGroup.postValue(integerInstrumentGroupMap);
                        },
                        throwable -> {
                            Log.e(TAG, "getInstrumentGroups: ", throwable);
                        }
                );
    }

    public void restartSocket()
    {
        priceSocket.unsubscribeAll();
        priceSocket.getInstrumentGroups()
                .subscribe(integerInstrumentGroupMap -> {
                            Log.i(TAG, "getInstrumentGroups: ");
                            instrumentGroup.postValue(integerInstrumentGroupMap);
                        },
                        throwable -> {
                            Log.e(TAG, "getInstrumentGroups: ", throwable);
                        }
                );
    }


    public void pause() {
        priceSocket.unsubscribeAll();
    }

    public void pageChanged(int instrID) {
        priceSocket.getLastPrice(instrID);
        priceSocket.subscribeTo(instrID);
    }

    public void getUpdatedTicks() {
        priceSocket.getInstrumentsNow()
               // .delay(1, TimeUnit.SECONDS)
                .subscribe(integerInstrumentGroupMap -> {
            Log.i(TAG, "getUpdatedTicks: ");
            instrumentGroup.postValue(integerInstrumentGroupMap);
        });
    }

    public void filterResults(String s) {
        filterText = s;
        getInstrumentGroups();
    }

    public String getSearchFilter() {
        return filterText;
    }
}
