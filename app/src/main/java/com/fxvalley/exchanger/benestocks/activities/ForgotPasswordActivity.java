package com.fxvalley.exchanger.benestocks.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.fxvalley.exchanger.benestocks.R;
import com.fxvalley.exchanger.benestocks.utilities.Constants;
import com.fxvalley.exchanger.benestocks.utilities.FadeOnTouchListener;

import net.cocooncreations.template.network.CocoonJsonRequest;
import net.cocooncreations.template.network.RequestActivity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by aloupas on 2/1/2017.
 */
public class ForgotPasswordActivity extends RequestActivity {

    private EditText forgotPasswordEdittext;
    private View resetButton;

    private final String PATH = "auth/reset";
    private MaterialDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.forgot_password_toolbar);
        toolbar.setTitle(R.string.reset_password);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        forgotPasswordEdittext = (EditText) findViewById(R.id.forgotPasswordEdittext);
        resetButton = findViewById(R.id.resetButton);
        resetButton.setOnTouchListener(new FadeOnTouchListener() {
            @Override
            public void onClick(View view, MotionEvent event) {
                CocoonJsonRequest.with(Constants.REST_API_URL + PATH, ForgotPasswordActivity.this).jsonObject(getSignUpJson()).callAsync();
                progressDialog.show();
            }
        });


        progressDialog = new MaterialDialog.Builder(this)
                .title(R.string.please_wait)
                .content(R.string.sending_request)
                .progress(true, 0).build();


    }

    public JSONObject getSignUpJson() {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("email", forgotPasswordEdittext.getText().toString());

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public <T> void onResponse(T response, String requestcode, NetworkResponse networkResponse) {

        if (requestcode.equals(Constants.REST_API_URL + PATH + Request.Method.GET)) {

            if (networkResponse.statusCode == 200) {

                new MaterialDialog.Builder(this)
                        .title(R.string.success)
                        .content(R.string.you_will_receive_email)
                        .positiveText(R.string.ok).onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                    }
                }).show();
            }
        }
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    @Override
    public void onError(VolleyError volleyError) {

        volleyError.printStackTrace();

        if (progressDialog != null)
            progressDialog.dismiss();

    }
}
