package com.fxvalley.exchanger.benestocks.database.datasource;


import android.database.Cursor;

import com.fxvalley.exchanger.benestocks.database.models.BaseModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by aloupas on 2/5/2017.
 */

public abstract class BaseDatasource<T extends BaseModel> {

    private Class<T> modelClass;

    BaseDatasource(String table, Class<T> modelClass) {
        this.modelClass = modelClass;
    }

    T getModelByColumnValues(HashMap<String, String> columnAndValues) {
        try {

            Cursor cursor = null;

            return T.from(cursor, modelClass);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    T getModelByColumnValue(String column, String value) {
        try {

            Cursor cursor = null;

            return T.from(cursor, modelClass);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    List<T> getModelsByColumnValue(String column, String value) {

        return new ArrayList<T>();
    }

    List<T> getModelsByColumnValue(HashMap<String, String> columnAndValues) {

        return new ArrayList<T>();
    }

    List<T> getAllModels() {

        List<T> returnedList = new ArrayList<T>();
        try {
            Cursor cursor = null;

            return returnedList;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    long saveModel() {

        return -1;
    }

    long updateModel() {

        return -1;
    }

    void saveModels(List<T> models) {

    }

    void updateModels(List<T> models) {

    }

    boolean deleteModel() {

        return false;
    }

    boolean deleteModels() {

        return false;
    }


}
