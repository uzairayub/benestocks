package com.fxvalley.exchanger.benestocks.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.fxvalley.exchanger.benestocks.R;
import com.fxvalley.exchanger.benestocks.models.User;
import com.fxvalley.exchanger.benestocks.utilities.Constants;

import net.cocooncreations.template.network.CocoonStringRequest;
import net.cocooncreations.template.network.RequestActivity;
import net.cocooncreations.template.network.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends RequestActivity {

    private static final String TAG = "LoginActivity";
    private final String PATH = "token";

    private View joinView, forgotPassword, login;
    private EditText username, password;
    private MaterialDialog progressDialog;

    private final String LOGIN_URL = Constants.REST_API_URL + PATH;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        joinView = findViewById(R.id.joinButton);
        forgotPassword = findViewById(R.id.forgot_password);
        ImageView hook = findViewById(R.id.imageView);
        hook.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
//                username.setText("mobiledeveloper");
//                password.setText("XM5aO23JNBOV");

                username.setText("AndroidDev1");
                password.setText("Qwert123");

//                username.setText("demo1");
//                password.setText("Demo1234");
                return false;
            }
        });
        login = findViewById(R.id.loginButton);
        username = (EditText) findViewById(R.id.usernameEdittext);
        password = (EditText) findViewById(R.id.passwordEdittext);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");

                HashMap<String, String> params = new HashMap<String, String>();
                params.put("grant_type", "password");
                params.put("username", username.getText().toString());
                params.put("password", password.getText().toString());


                CocoonStringRequest.with(LOGIN_URL, LoginActivity.this)
                        .method(Request.Method.POST)
                        .headers(headers)
                        .params(params)
                        .callAsync();

                progressDialog.show();

            }
        });

        joinView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), SignUpActivity.class);
                startActivity(intent);
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        progressDialog = new MaterialDialog.Builder(this)
                .title(R.string.please_wait)
                .content(R.string.attempting_to_login)
                .progress(true, 0).build();



    }

    private void gotoMainActivity() {

        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.putExtra("isNewInstance", true);
        intent.putExtra("restart", false);
        startActivity(intent);
        finish();

        Toast.makeText(LoginActivity.this, getString(R.string.succesfully_logged_in), Toast.LENGTH_SHORT).show();
    }

    @Override
    public <T> void onResponse(T response, String requestcode, NetworkResponse networkResponse) {

        if (requestcode.equals(Request.Method.POST + LOGIN_URL)) {

            if (networkResponse.statusCode == 200) {
                try {
                    JSONObject jsonObject = new JSONObject((String) response);
                    User user = new User(jsonObject);
                    user.save(LoginActivity.this);
                    Log.d(TAG, user.toString());
                    gotoMainActivity();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else{
                Toast.makeText(LoginActivity.this, R.string.error_attempting_to_login, Toast.LENGTH_LONG).show();
            }
        }

    if(progressDialog!=null)
            progressDialog.dismiss();

}

    @Override
    public void onError(VolleyError volleyError) {

        volleyError.printStackTrace();

        if (progressDialog != null)
            progressDialog.dismiss();

        showDialog();
    }

    public JSONObject getSignUpJson() {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("username", username.getText().toString());
            jsonObject.put("password", password.getText().toString());
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        return jsonObject;
    }

    public void login() {

        StringRequest jsonObjRequest = new StringRequest(Request.Method.POST,
                Constants.REST_API_URL + PATH,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("TEST", response);

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("volley", "Error: " + error.getMessage());
                error.printStackTrace();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("grant_type", "password");
                params.put("username", "demo3");
                params.put("password", "Demo1234");
                return params;
            }

        };

        VolleySingleton.getInstance(this).addRequest(jsonObjRequest);
    }


    void showDialog ()
    {
        final Dialog dialog = new Dialog(LoginActivity.this,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);


        ImageButton btnClose = dialog.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        TextView tvOk = dialog.findViewById(R.id.tvOk);
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }
}
