package com.fxvalley.exchanger.benestocks.charts;

import com.fxvalley.exchanger.benestocks.models.ChartData;
import com.fxvalley.exchanger.benestocks.utilities.ChartSelector;
import com.github.mikephil.charting.charts.CandleStickChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;
import java.util.List;

public class TimeValueFormatter implements IAxisValueFormatter {

    private List<ChartData> chartData;
    private CandleStickChart chart;
    private ChartSelector.ChartSelectorState chartSelectorState;

    public TimeValueFormatter(List<ChartData> chartData, CandleStickChart chart, ChartSelector.ChartSelectorState chartSelectorState) {
        this.chartData = new ArrayList<>(chartData);
        this.chart = chart;
        this.chartSelectorState = chartSelectorState;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        if ((int) value >= chartData.size()) {
            return "";
        }
            return chartData.get((int) value).dayDateTime;
    }
}