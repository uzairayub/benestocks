package com.fxvalley.exchanger.benestocks.data.model;

import com.google.gson.annotations.SerializedName;

public class PingMessage {
    @SerializedName("Token")
    public String token;
    @SerializedName("Host")
    public String host;

    public PingMessage(String token, String host) {
        this.token = token;
        this.host = host;
    }
}