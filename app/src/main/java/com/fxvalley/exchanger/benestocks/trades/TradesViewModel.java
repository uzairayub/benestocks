package com.fxvalley.exchanger.benestocks.trades;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.PagedList;
import android.util.SparseArray;

import com.fxvalley.exchanger.benestocks.data.TradeDataFactory;
import com.fxvalley.exchanger.benestocks.data.TradeDataSource;
import com.fxvalley.exchanger.benestocks.data.TradeRepository;
import com.fxvalley.exchanger.benestocks.data.TradeRepositoryImpl;
import com.fxvalley.exchanger.benestocks.data.model.DataLoadState;
import com.fxvalley.exchanger.benestocks.data.model.PagingResult;
import com.fxvalley.exchanger.benestocks.models.Account;
import com.fxvalley.exchanger.benestocks.models.Trade;
import com.fxvalley.exchanger.benestocks.sockets.TradeSessionSocket;

import static com.fxvalley.exchanger.benestocks.data.TradeRepository.CANCELLED;
import static com.fxvalley.exchanger.benestocks.data.TradeRepository.CLOSED;
import static com.fxvalley.exchanger.benestocks.data.TradeRepository.OPEN;
import static com.fxvalley.exchanger.benestocks.data.TradeRepository.PENDING;


public class TradesViewModel extends ViewModel {

    private TradeRepository repository;
    public MutableLiveData<Boolean> tradesOpen = new MutableLiveData<>();
    private TradeSessionSocket socket;

    public TradesViewModel(TradeSessionSocket socket) {
        this.socket = socket;
        SparseArray<TradeDataFactory> dataSourceSparseArray = new SparseArray<>();
        dataSourceSparseArray.put(OPEN, new TradeDataFactory(new TradeDataSource(socket, OPEN)));
        dataSourceSparseArray.put(CLOSED, new TradeDataFactory(new TradeDataSource(socket, CLOSED)));
        dataSourceSparseArray.put(PENDING, new TradeDataFactory(new TradeDataSource(socket, PENDING)));
        dataSourceSparseArray.put(CANCELLED, new TradeDataFactory(new TradeDataSource(socket, CANCELLED)));
        repository = new TradeRepositoryImpl(dataSourceSparseArray);
        socket.subscribeToTradeOpen().subscribe((isOpen)->{
            tradesOpen.postValue(isOpen);
        });
    }



    public LiveData<PagedList<Trade>> getTrades(int type) {
        return repository.getTrades(type);
    }

    public LiveData<DataLoadState> dataLoadStatus() {
        return repository.getDataLoadStatus();
    }

    public LiveData<PagingResult> getPageInfo() {
        return repository.getPageInfo();
    }

    public Account getActiveAccount() {
        return socket.getActiveAccount();
    }
}
