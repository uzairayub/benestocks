package com.fxvalley.exchanger.benestocks.database.datasource;

import com.fxvalley.exchanger.benestocks.database.models.Instrument;

/**
 * Created by aloupas on 2/5/2017.
 */

public class InstrumentDatasource extends BaseDatasource<Instrument>{

    public InstrumentDatasource(String table, Class<Instrument> modelClass) {
        super(table, modelClass);


    }
}
