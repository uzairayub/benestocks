package com.github.mikephil.charting.components;

/** enum that indicates the position of the LimitLine label */
public enum LabelPosition {
    LEFT_TOP, LEFT_BOTTOM, RIGHT_TOP, RIGHT_BOTTOM
}
